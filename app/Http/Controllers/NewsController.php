<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use DB;
use Carbon;
use Illuminate\Http\RedirectResponse;

class NewsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {    $News = DB::table('noticias')->get();

        return view('News',compact('News'));
        
    }
    public function NewsInserir()
    {   
         return view('NewsInserir');
    }
    public function NewsInsert(Request $request)
    {

        
        
        $image = $request->file('file');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/img/news');

            $image->move($destinationPath, $name);
           
            

        $titulo = $request->get('titulo');
        $texto = $request->get('texto');
        $slug = strtr($titulo, "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ ", "aaaaeeiooouucAAAAEEIOOOUUC-");
        $data = Carbon\Carbon::now();

        DB::table('noticias')
                    ->insert(['titulo' => $titulo,'imagem' => $name, 'texto' => $texto,'slug' => $slug, 'data' => $data]);
    
        return redirect('News');
        
    
    }
    public function NewsDeletar($id)
    {
        $News = DB::table('noticias')
        ->where('id', '=', $id)->delete();
        return redirect('News');
    }
    public function NewsEditar($id)
    {
      $News = DB::table('noticias')
        ->where('id','=',$id)
        ->first();

        return view('NewsEditar',compact('News'));
        
    }
    public function NewsEdit(Request $request,$id)
    {
        $titulo = $request->get('titulo');
        $texto = $request->get('texto');
        
        DB::table('noticias')
        ->where('id','=',$id)
        ->update(['titulo' => $titulo, 'texto' => $texto ]);

        return redirect('News');
        
    }
}