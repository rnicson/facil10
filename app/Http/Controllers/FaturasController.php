<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class FaturasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     


public function  abertas(Request $request){
   
       $user = auth()->user()->id;
       $servicos = DB::select('select * from pedidos_servicos where id_user = :user order BY id asc', ['user' => $user]);

     return view('servicos', compact('servicos'));
}
public function  fechadas(Request $request){
   
       $atual = date('m');


     return view('faturas');
}
public function  creditos(Request $request){
   
       $atual = date('m');


     return view('creditos-conta');
}
}