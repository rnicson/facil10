<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class DadosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     


public function  index(Request $request){
   
       $atual = date('m');


     return view('dados');
}

            public function update(Request $request)
        {
            $id = auth()->user()->id;
            
            DB::table('users')
                    ->where('id', $id)
                    ->update(['name' => $request->input('nome'),
                             'sobrenome' => $request->input('sobrenome'),
                             'sexo' => $request->input('sexo'),
                             'nascimento' => $request->input('nascimento'),
                             'cargo' => $request->input('cargo'),
                             'email' => $request->input('email'),
                             'escolaridade' => $request->input('escolaridade'),
                             'civil' => $request->input('civil'),
                             'telresidencial' => $request->input('telresidencial'),
                             'telcelular' => $request->input('telcelular'),
                             'telrecado' => $request->input('telrecado'),
                             'rua' => $request->input('rua'),
                             'numero' => $request->input('numero'),
                             'bairro' => $request->input('bairro'),
                             'estado' => $request->input('estado')]);
                             
                  
    if ($request->file('image')) {           
                             
    $image = $request->file('image');

    $input['image'] = $id.'.'.$image->getClientOriginalExtension();

    $destinationPath = public_path('/images/usuarios/'.$id);

    $image->move($destinationPath, $input['image']);
    
     DB::table('users')
                    ->where('id', $id)
                    ->update(['image' => $input['image']]);
    }
                             
    
        return redirect('dados');

        }
     
}
