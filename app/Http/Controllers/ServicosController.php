<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


 class ServicosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function servico()
        {
         return view('dominio');
    }
    public function insert(Request $request)
       {
                $user = auth()->user()->id;
                $dominio = $request->input('dominio');
                $servico = $request->input('servico');
                $data = date("Y-m-d");
                $valor = '0';
                $null = $data;
                
                DB::table('pedidos_servicos')
                        ->insert(['id_servico' => $servico,
                                 'id_user' => $user,
                                 'valor' => $valor,
                                 'observacao' => $dominio,
                                 'status' => 'Pendente',
                                 'data_criado' => $data,
                                 'data_expirado' => $null]);
                                 
            return redirect('servicos');
    
            }
}