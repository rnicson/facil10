<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class DivulgarController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     


public function  index(Request $request){
    
         $noticia = DB::select("select * from noticias ORDER BY id DESC");
         $cloud = DB::select("select * from produtos_afiliados WHERE tipoprod = 6 ORDER BY idprod ASC");
         $revenda = DB::select("select * from produtos_afiliados WHERE tipoprod = 5 ORDER BY idprod ASC");
         $hospedagem = DB::select("select * from produtos_afiliados WHERE tipoprod = 4 ORDER BY idprod ASC");
         $dedicado = DB::select("select * from produtos_afiliados WHERE tipoprod = 3 ORDER BY idprod ASC");
         $infrafisica = DB::select("select * from produtos_afiliados WHERE tipoprod = 2 ORDER BY idprod ASC");
          $infrajuridica = DB::select("select * from produtos_afiliados WHERE tipoprod = 1 ORDER BY idprod ASC");
     
   
       $atual = date('m');


     return view('divulgar', compact('noticia','infrafisica','infrajuridica','dedicado','hospedagem','revenda','cloud'));
}
}