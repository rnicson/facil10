<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\RedirectResponse;

class HomeController extends Controller
{
    
        public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
     $noticia = DB::select("select * from noticias ORDER BY id DESC");
     return view('welcome', compact('noticia'));
}
public function noticia($id)
    {
     $noticia = DB::select("select * from noticias where id = $id");
     
     return view('noticia', compact('noticia'));
}

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function pontuar($id)
    {
          $user = DB::select("select * from users where id = $id");


     return  new RedirectResponse("https://www.facil10.com.br");
}
public function servico()
    {
     return view('dominio');
}

public function insert(Request $request)
   {
            $user = auth()->user()->id;
            $dominio = $request->input('dominio');
            $servico = $request->input('servico');
            $data = date("Y-m-d");
            $valor = '0';
            $null = $data;
            
            DB::table('pedidos_servicos')
                    ->insert(['id_servico' => $servico,
                             'id_user' => $user,
                             'valor' => $valor,
                             'observacao' => $dominio,
                             'status' => 'Pendente',
                             'data_criado' => $data,
                             'data_expirado' => $null]);
                             
        return redirect('servicos');

        }
}
