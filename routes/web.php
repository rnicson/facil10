<?php


Route::get('/', 'PaginaController@inicio');

Route::get('/quemsomos', function () {
    return view('quemsomos');
});

Route::get('/curso/informatica-basica', function () {
    return view('infobasica');
});

Route::get('/curso/web-developer', function () {
    return view('developerweb');
});

Route::get('/infraestrutura', function () {
    return view('infraestrutura');
});

Route::get('/site', function () {
    return view('site');
});

Route::get('/sitemobi', function () {
    return view('sitemobi');
});

Route::get('/artegrafica', function () {
    return view('artegrafica');
});
Route::get('/artemobi', function () {
    return view('artemobi');
});

Route::get('/sistemas', function () {
    return view('sistemas');
});

Route::get('/seo', function () {
    return view('seo');
});

Route::get('/redessociais', function () {
    return view('redessociais');
});

Route::get('/dedicado', function () {
    return view('dedicado');
});

Route::get('/cloud', function () {
    return view('cloud');
});

Route::get('/revenda', function () {
    return view('revenda');
});

Route::get('/hospedagem', function () {
    return view('hospedagem');
});

Route::get('/dedicadom', function () {
    return view('dedicadom');
});

Route::get('/cloudm', function () {
    return view('cloudm');
});

Route::get('/revendam', function () {
    return view('revendam');
});

Route::get('/hospedagemm', function () {
    return view('hospedagemm');
});

Route::get('/emailmarketing', function () {
    return view('email');
});

Route::get('/emailm', function () {
    return view('emailm');
});

Route::get('/smsmarketing', function () {
    return view('smsmarketing');
});

Route::get('/smsm', function () {
    return view('smsm');
});


Route::get('/eccomerce', function () {
    return view('eccomerce');
});

Route::get('/suporteti', function () {
    return view('suporteti');
});

Route::get('/cloudcorporativo', function () {
    return view('cloudcorporativo');
});

Route::get('/segurancainformacao', function () {
    return view('segurancainformacao');
});

Route::get('/suporteti/{indicador}', 'HomeController@suporteti');
Route::get('/cloudcorporativo/{indicador}', 'HomeController@cloudcorporativo');
Route::get('/segurancainformacao/{indicador}', 'HomeController@segurancaoinformacao');
Route::get('/seo/{indicador}', 'HomeController@seo');
Route::get('/emailmarketing/{indicador}', 'HomeController@emailmarketing');
Route::get('/redessociais/{indicador}', 'HomeController@redessociais');
Route::get('/dedicado/{indicador}', 'HomeController@dedicado');
Route::get('/cloud/{indicador}', 'HomeController@cloud');
Route::get('/revenda/{indicador}', 'HomeController@revenda');
Route::get('/hospedagem/{indicador}', 'HomeController@hospedagem');
Route::get('/site/{indicador}', 'HomeController@site');
Route::get('/eccomerce/{indicador}', 'HomeController@eccomerce');
Route::get('/artegrafica/{indicador}', 'HomeController@artegrafica');
Route::get('/sistemas/{indicador}', 'HomeController@sistemas');

Route::get('/blog', function () {
    return view('blog');
});

Route::get('/eccomerce', function () {
    return view('eccomerce');
});

Route::get('/contato', function () {
    return view('contato');
});
Route::get('/infra-desk', function () {
    return view('infra-desk');
});
Route::get('/shop', function () {
    return view('shop');
});
Route::get('/indicado/{idref}', 'HomeController@pontuar')->name('home');
Route::get('/dominio', 'ServicosController@servico')->name('home');
Route::post('/insertdomain', 'ServicosController@insert');

Route::post('/enviar', function(Illuminate\Http\Request $request){
	var_dump($request->all());
});





Auth::routes();

Route::get('/criar-chamados', 'ChamadosController@criar')->name('home');
Route::get('/chamados-abertos', 'ChamadosController@abertos')->name('home');
Route::get('/chamados-fechados', 'ChamadosController@fechados')->name('home');
Route::get('/dados', 'DadosController@index')->name('editar');
Route::post('/dados', 'DadosController@update')->name('editar');
Route::get('/pagamentos', 'PagamentosController@index')->name('editar');
Route::post('/pagamentos', 'PagamentosController@update')->name('editar');
Route::get('/convites', 'ConvitesController@index')->name('editar');
Route::get('/divulgar', 'DivulgarController@index')->name('editar');
Route::get('/sites', 'SitesController@index')->name('editar');
Route::get('/creditos-conta', 'FaturasController@creditos')->name('editar');
Route::get('/servicos', 'FaturasController@abertas')->name('editar');
Route::get('/faturas', 'FaturasController@fechadas')->name('editar');
Route::get('/funciona', 'FuncionaController@index')->name('editar');
Route::get('/noticia/{id}/{slug}', 'PaginaController@noticiano')->name('noticiano');
Route::get('/noticia/{id}/{slug}/{indicador}', 'HomeController@noticia');
Route::get('/{indicador}', 'PaginaController@indicador');
Route::get('/News', 'NewsController@index')->name('home');
Route::get('/NewsInserir', 'NewsController@NewsInserir')->name('home');
Route::post('/NewsInserir', 'NewsController@NewsInsert')->name('home');
Route::get('/editar/{id}', 'NewsController@NewsEditar')->name('home');
Route::get('/News', 'NewsController@index')->name('home');
Route::get('/NewsInserir', 'NewsController@NewsInserir')->name('home');
Route::post('/NewsInserir', 'NewsController@NewsInsert')->name('home');
Route::get('/deletar/{id}', 'NewsController@NewsDeletar')->name('home');
Route::get('/editar/{id}', 'NewsController@NewsEditar')->name('home');
Route::post('/editar/{id}', 'NewsController@NewsEdit')->name('home');
