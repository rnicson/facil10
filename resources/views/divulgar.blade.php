@extends('layout')
 
@section('title', 'Divulgue e Lucre')

@section('content')
<div class="container">
    <center><h1>Divulgação</h1>
    <h5>Escolha e Clique no que deseja divulgar,copie o link e comece agora mesmo a receber lucros.</h5></br></br>
    <div class="alert alert-secondary" role="alert">
  Informações
</div>
<div class="alert alert-success" role="alert">
  Notícias
</div>
    <h6><a class="btn btn-success" id="noticiasb">Tecnologia</a></h6>
        <div id="noticiasdiv" class="container">
 @foreach($noticia as $noticia)
  <div class="container">
    <div class="col-3">
     <img src="/img/news/{{$noticia->imagem}}" class="img-fluid" alt="Noticia">
    </div>
    <div class="col-3">
     {{$noticia->titulo}}
    </div>
  </div>
  
  <div class="row">
    <div class="col">
      <input type="text" value="https://facil10.com.br/divulgar/{{auth()->user()->id}}/noticia/{{$noticia->id}}" size="32" readonly>
    </div>
  </div>
  </br>
  @endforeach
</div>
</br>
<div class="alert alert-secondary" role="alert">
  Produtos
</div>
<div class="alert alert-success" role="alert">
  Infraestrutura
</div>
    <h6><a class="btn btn-success" id="infraestruturapessoafisicab">Infraestrutura Pessoa Física</a></h6>
    <div id="infraestruturapessoafisicadiv" class="container">
         @foreach($infrafisica as $infrafisica)
  <div class="container">
    <div class="col-3">
      <b>{{$infrafisica->nameprod}}</b>
    </div>
    <div class="col-3">
     R${{$infrafisica->valorprod}}
    </div>
  </div>
  <div class="row">
    <div class="col">
      <input type="text" value="https://facil10.com.br/divulgar/{{auth()->user()->id}}/produto/{{$infrafisica->idprod}}" size="32" readonly>
    </div>
  </div>
  <HR>
  @endforeach
</div>

</br>
    <h6><a class="btn btn-success" id="infraestruturapessoajuridicab">Infraestrutura Pessoa Jurídica</a></h6>
    <div id="infraestruturapessoajuridicadiv" class="container">
        @foreach($infrajuridica as $infrajuridica)
  <div class="container">
    <div class="col-3">
      <b>{{$infrajuridica->nameprod}}</b>
    </div>
    <div class="col-3">
     R${{$infrajuridica->valorprod}}
    </div>
  </div>
 
  <div class="row">
    <div class="col">
      <input type="text" value="https://facil10.com.br/divulgar/{{auth()->user()->id}}/produto/{{$infrajuridica->idprod}}" size="32" readonly>
    </div>
  </div>
  <HR>
  @endforeach
</div>

</br>
<div class="alert alert-success" role="alert">
  Servidores
</div>
    <h6><a class="btn btn-success" id="dedicadob">Servidor Dedicado</a></h6>
    <div id="dedicadodiv" class="container">
        @foreach($dedicado as $dedicado)
  <div class="container">
    <div class="col-3">
       <b>{{$dedicado->nameprod}}</b>
    </div>
    <div class="col-3">
     R${{$dedicado->valorprod}}
    </div>
  </div>
  <div class="row">
    <div class="col">
      <input type="text" value="https://facil10.com.br/divulgar/{{auth()->user()->id}}" size="32" readonly>
    </div>
  </div>
  @endforeach
</div>

</br>
    <h6><a class="btn btn-success" id="hospedagemb">Hospedagem de Site</a></h6>
    <div id="hospedagemdiv" class="container">
         @foreach($hospedagem as $hospedagem)
  <div class="container">
    <div class="col-3">
      <b>{{$hospedagem->nameprod}}</b>
    </div>
    <div class="col-3">
      R${{$hospedagem->valorprod}}
    </div>
  </div>
  <div class="row">
    <div class="col">
      <input type="text" value="https://facil10.com.br/divulgar/{{auth()->user()->id}}" size="32" readonly>
    </div>
  </div>
  @endforeach
</div>

</br>
    <h6><a class="btn btn-success" id="revendab">Revenda de Site</a></h6>
    <div id="revendadiv" class="container">
        @foreach($revenda as $revenda)
  <div class="container">
    <div class="col-3">
      <b>{{$revenda->nameprod}}</b>
    </div>
    <div class="col-3">
      R${{$revenda->valorprod}}
    </div>
  </div>
  <div class="row">
    <div class="col">
      <input type="text" value="https://facil10.com.br/divulgar/{{auth()->user()->id}}" size="32" readonly>
    </div>
  </div>
  @endforeach
</div>

</br>
    <h6><a class="btn btn-success" id="cloudb">Cloud Computing</a></h6>
    <div id="clouddiv" class="container">
        @foreach($cloud as $cloud)
  <div class="container">
    <div class="col-3">
       <b>{{$cloud->nameprod}}</b>
    </div>
    <div class="col-3">
     R${{$cloud->valorprod}}
    </div>
  </div>
  <div class="row">
    <div class="col">
      <input type="text" value="https://facil10.com.br/divulgar/{{auth()->user()->id}}" size="32" readonly>
    </div>
  </div>
  @endforeach
</div>

</br>
<div class="alert alert-success" role="alert">
  Desenvolvimento
</div>
    <h6><a class="btn btn-success" id="siteb">Site</a></h6>
    <div id="sitediv" class="container">
  <div class="container">
    <div class="col-3">
      imagem
    </div>
    <div class="col-3">
     titulo
    </div>
  </div>
  <div class="row">
    <div class="col">
      <input type="text" value="https://facil10.com.br/divulgar/{{auth()->user()->id}}" size="32" readonly>
    </div>
  </div>
</div>

</br>
    <h6><a class="btn btn-success" id="lojavirtualb">Loja Virtual</a></h6>
    <div id="lojavirtualdiv" class="container">
  <div class="container">
    <div class="col-3">
      imagem
    </div>
    <div class="col-3">
     titulo
    </div>
  </div>
  <div class="row">
    <div class="col">
      <input type="text" value="https://facil10.com.br/divulgar/{{auth()->user()->id}}" size="32" readonly>
    </div>
  </div>
</div>

</br>
    <h6><a class="btn btn-success" id="artesgraficasb">Artes Gráficas</a></h6>
    <div id="artesgraficasdiv" class="container">
  <div class="container">
    <div class="col-3">
      imagem
    </div>
    <div class="col-3">
     titulo
    </div>
  </div>
  <div class="row">
    <div class="col">
      <input type="text" value="https://facil10.com.br/divulgar/{{auth()->user()->id}}" size="32" readonly>
    </div>
  </div>
</div>

</br>
    <h6><a class="btn btn-success" id="sistemasb">Sistemas</a></h6>
    <div id="sistemasdiv" class="container">
  <div class="container">
    <div class="col-3">
      imagem
    </div>
    <div class="col-3">
     titulo
    </div>
  </div>
  <div class="row">
    <div class="col">
      <input type="text" value="https://facil10.com.br/divulgar/{{auth()->user()->id}}" size="32" readonly>
    </div>
  </div>
</div>

</br>
<div class="alert alert-success" role="alert">
  Marketing
</div>
    <h6><a class="btn btn-success" id="seob">SEO</a></h6>
    <div id="seodiv" class="container">
  <div class="container">
    <div class="col-3">
      imagem
    </div>
    <div class="col-3">
     titulo
    </div>
  </div>
  <div class="row">
    <div class="col">
      <input type="text" value="https://facil10.com.br/divulgar/{{auth()->user()->id}}" size="32" readonly>
    </div>
  </div>
</div>

</br>
    <h6><a class="btn btn-success" id="emailmarketingb">Email Marketing</a></h6>
    <div id="emailmarketingdiv" class="container">
  <div class="container">
    <div class="col-3">
      imagem
    </div>
    <div class="col-3">
     titulo
    </div>
  </div>
  <div class="row">
    <div class="col">
      <input type="text" value="https://facil10.com.br/divulgar/{{auth()->user()->id}}" size="32" readonly>
    </div>
  </div>
</div>

</br>
    <h6><a class="btn btn-success" id="redessociaisb">Redes Sociais</a></h6>
    <div id="redessociaisdiv" class="container">
  <div class="container">
    <div class="col-3">
      imagem
    </div>
    <div class="col-3">
     titulo
    </div>
  </div>
  <div class="row">
    <div class="col">
      <input type="text" value="https://facil10.com.br/divulgar/{{auth()->user()->id}}" size="32" readonly>
    </div>
  </div>
</div>
    </center>
</div>
@stop