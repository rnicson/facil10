@extends('layout')

@section('title', 'UNIDAS - MEU PERFIL')

@section('content_header')
<div class="container" style="text-align:center">
    @empty(auth()->user()->image)
    @if(auth()->user()->sexo == 'Masculino')
        <img class="img-circle" width="200px" src="/images/usuarios/homem.jpeg">
    @else
         <img class="img-circle" width="200px" src="/images/usuarios/mulher.jpeg">
         @endif
   @else
           <img class="img-circle" width="200px" src="/images/usuarios/{{auth()->user()->id}}/{{auth()->user()->image}}">
    @endempty 
    </div>
    @stop

@section('content')
@if(session('sucess'))
<div class="alert alert-sucess">
{{session('sucess')}}
</div>
@endif
@if(session('error'))
<div class="alert alert-danger">
{{session('error')}}
</div>
@endif
<div class="container">
    <form method="POST" action="/dados" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group row">
  <label for="example-email-input" class="col-2 col-form-label">Nome</label>
  <div class="col-10">
    <input class="form-control" name="nome" required="required" title="Digite conforme ao exemplo: Richard Nicson" type="text" value="{{auth()->user()->name}}" id="nome">
  </div>
</div>
 <div class="form-group row">
  <label for="example-email-input" class="col-2 col-form-label">Sobrenome</label>
  <div class="col-10">
    <input class="form-control" type="text" required="required" title="Digite conforme ao exemplo: Costa de Lima" value="{{auth()->user()->sobrenome}}" name="sobrenome" id="sobrenome">
  </div>
</div>

 <div class="form-group row">
    <label for="example-tel-input">Sexo</label>
    <select class="form-control" name="sexo" id="sexo">
        @if(auth()->user()->sexo == 'Masculino')
      <option value="Masculino" selected>Masculino</option>
      <option value="Feminino">Feminino</option>
      @else
      <option value="Masculino">Masculino</option>
      <option value="Feminino" selected>Feminino</option>
      @endif
    </select>
  </div>

<div class="form-group row">
  <label for="example-email-input"  class="col-2 col-form-label">Data de Nascimento</label>
  <div class="col-10">
    <input class="form-control" type="date" required="required" value="{{auth()->user()->nascimento}}" name="nascimento" id="nascimento">
  </div>
</div>
 <div class="form-group row">
  <label for="example-email-input" class="col-2 col-form-label">Cargo</label>
  <div class="col-10">
    <input class="form-control" type="text" required="required" title="Digite conforme ao exemplo: Analista de T.I" value="{{auth()->user()->cargo}}" name="cargo" id="cargo">
  </div>
</div>
<div class="form-group row">
  <label for="example-email-input" class="col-2 col-form-label">Email</label>
  <div class="col-10">
    <input class="form-control" type="email" required="required" title="Digite conforme ao exemplo: meuemail@unidas.org.br" value="{{auth()->user()->email}}" name="email" id="email" readonly>
  </div>
</div>
 <div class="form-group row">
  <label for="example-email-input" class="col-2 col-form-label">Escolaridade</label>
  <div class="col-10">
    <input class="form-control" type="text" required="required" value="{{auth()->user()->escolaridade}}" title="Digite conforme ao exemplo: Ensino Superior Completo" name="escolaridade" id="escolaridade">
  </div>
</div>
 <div class="form-group row">
  <label for="example-email-input" class="col-2 col-form-label">Estado Civil</label>
  <div class="col-10">
         <select class="form-control" selected="auth()->user()->estado" name="civil" id="civil">
      <option>{{auth()->user()->civil}}</option>
      <option>Solteiro(a)</option>
      <option>Casado(a)</option>
      <option>Divorciado(a)</option>
    </select>
  </div>
</div>
 <div class="form-group row">
  <label for="example-email-input" class="col-2 col-form-label">Telefone Residêncial</label>
  <div class="col-10">
    <input class="form-control" type="tel" pattern="\([0-9]{2})[0-9]{4,6}-[0-9]{3,4}$" title="Digite conforme ao exemplo: (11)1111-1111"  required="required" value="{{auth()->user()->telresidencial}}" name="telresidencial" id="telresidencial">
  </div>
</div>
 <div class="form-group row">
  <label for="example-email-input" class="col-2 col-form-label">Telefone Celular</label>
  <div class="col-10">
    <input class="form-control" type="tel" pattern="\([0-9]{2}) [0-9]{4,6}-[0-9]{3,4}$" title="Digite conforme ao exemplo: (11)11111-1111" required="required" value="{{auth()->user()->telcelular}}" name="telcelular" id="telcelular">
  </div>
</div>
 <div class="form-group row">
  <label for="example-email-input" class="col-2 col-form-label">Telefone Recado</label>
  <div class="col-10">
    <input class="form-control" type="tel" pattern="\([0-9]{2}) [0-9]{4,6}-[0-9]{3,4}$" title="Digite conforme ao exemplo: (11)11111-1111" required="required" value="{{auth()->user()->telrecado}}" name="telrecado" id="telrecado">
  </div>
</div>
<div class="form-group row">
  <label for="example-url-input" class="col-2 col-form-label">Rua</label>
  <div class="col-10">
    <input class="form-control" type="text" required="required" value="{{auth()->user()->rua}}" title="Digite conforme ao exemplo: Alameda Santos" name="rua" id="rua">
  </div>
</div>
<div class="form-group row">
  <label for="example-tel-input" class="col-2 col-form-label">Número</label>
  <div class="col-10">
    <input class="form-control" type="number" required="required" value="{{auth()->user()->numero}}" name="numero" title="Digite conforme ao exemplo: 200" id="numero">
  </div>
</div>
<div class="form-group row">
  <label for="example-tel-input" class="col-2 col-form-label">Bairro</label>
  <div class="col-10">
    <input class="form-control" type="text" required="required" value="{{auth()->user()->bairro}}" title="Digite conforme ao exemplo: Vila Santos" name="bairro"  id="bairro">
  </div>
</div>
<div class="form-group row">
    <label for="example-tel-input">Estado</label>
    <select class="form-control" selected="auth()->user()->estado" name="estado" id="estado">
      <option>{{auth()->user()->estado}}</option>
      <option>AC</option>
      <option>AL</option>
      <option>AM</option>
      <option>AP</option>
      <option>BA</option>
      <option>CE</option>
      <option>DF</option>
      <option>ES</option>
      <option>GO</option>
      <option>MA</option>
      <option>MT</option>
      <option>MS</option>
      <option>MG</option>
      <option>PA</option>
      <option>PB</option>
      <option>PE</option>
      <option>PR</option>
      <option>PI</option>
      <option>RJ</option>
      <option>RN</option>
      <option>RS</option>
      <option>RO</option>
      <option>RR</option>
      <option>SC</option>
      <option>SP</option>
      <option>SE</option>
      <option>TO</option>
    </select>
  </div>
  <div class="form-group row">
    <label for="exampleFormControlFile1">Foto</label>
    <input type="file" class="form-control-file" name="image" id="image">
  </div> 
<center><button type="submit" class="btn btn-success">Enviar</button></center>
</form></div>
@stop