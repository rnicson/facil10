@extends('layout')
 
@section('title', 'CURSO - INFORMÁTICA BÁSICA')
 
@section('content')
<br>
@guest
<a href="https://www.facil10.com.br/login" class="btn btn-outline-success btn-block">Quero Contratar</a>
@else
<a href="https://www.moip.com.br/PagamentoMoIP.do?id_carteira={{auth()->user()->id_carteira}}&valor=4999&nome=Curso Informática Básica&pagador_email={{auth()->user()->email}}&pagador_nome={{auth()->user()->name}}" class="btn btn-outline-success btn-block">Quero Contratar</a>
@endguest
	<br>
	<p style="text-align: justify;">No mundo de hoje, ter o conhecimento b&aacute;sico de como utilizar um computador &eacute; fundamental. A informatiza&ccedil;&atilde;o est&aacute; presente em praticamente todos os setores da vida moderna. Com este curso de <strong>inform&aacute;tica</strong> b&aacute;sica voc&ecirc; ter&aacute; acesso aos <span>principais conhecimentos</span> da inform&aacute;tica, como:</p>
<ul>
<li style="text-align: justify;">Compreender a parte f&iacute;sica do computador (pe&ccedil;as e placas);</li>
<li style="text-align: justify;">Ligar, desligar, entradas e sa&iacute;das de conex&otilde;es;</li>
<li style="text-align: justify;">Como utilizar a &Aacute;rea de Trabalho;</li>
<li style="text-align: justify;">Como navegar, pesquisar, baixar arquivos na Internet;</li>
<li style="text-align: justify;">Como utilizar os programas b&aacute;sicos do <strong>Windows</strong>;</li>
<li style="text-align: justify;">E muito mais...</li>
</ul>
<p style="text-align: justify;">Este curso de inform&aacute;tica &eacute; ideal para todos que desejam aprender corretamente como utilizar o computador e tamb&eacute;m para aqueles que desejam aprimorar ou testar seus conhecimentos de Inform&aacute;tica B&aacute;sica.</p>
<p style="text-align: justify;"><strong><span>&Eacute; um curso essencial para o curr&iacute;culo em qualquer &aacute;rea de atua&ccedil;&atilde;o.</span></strong></p>
<p><strong>Com o seguinte conte&uacute;do program&aacute;tico:</strong></p>
<p>Introdu&ccedil;&atilde;o <br />Aula 1: Ligar e Desligar e Assuntos B&aacute;sicos<br />Aula 2: Aplicativos e Internet<br />Aula 3: Pesquisar na Internet e Download<br />Aula 4: Editores de Texto<br />Aula 5: Planilhas de C&aacute;lculo<br />Aula 6: Gerador de Apresenta&ccedil;&otilde;es<br />Aula 7: Complementos<br /> Aprendendo um pouco sobre celulares Android<br />Estrutura do Computador &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <br />Informa&ccedil;&otilde;es sobre o mundo da Inform&aacute;tica &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; <br />Bibliografia/Links Recomendados</p>
<table class="curso-info">
<tbody>
<tr>
<th>FICHA DE INFORMA&Ccedil;&Otilde;ES </th>
</tr>
<tr>
<td><span><strong>Embasamento Legal: </strong>nossos cursos t&ecirc;m base legal constitu&iacute;da pelo Decreto Presidencial n&ordm; 5.154 e nossa metodologia segue as normas do MEC atrav&eacute;s da Resolu&ccedil;&atilde;o CNE n&ordm; 04/99.</span></td>
</tr>
<tr>
<td><span><strong>Pr&eacute;-requisitos:</strong> n&atilde;o h&aacute; pr&eacute;-requisitos para esse curso, sugere-se ter Ensino M&eacute;dio completo (n&atilde;o obrigat&oacute;rio).</span></td>
</tr>
<tr>
<td><span><strong>Carga Hor&aacute;ria do Certificado:</strong> 40 horas</span></td>
</tr>
<tr>
<td style="text-align: justify;"><span><strong>Objetivos:</strong> curso livre para Qualifica&ccedil;&atilde;o Profissional, onde o aluno aprender&aacute; as principais no&ccedil;&otilde;es e conhecimentos de inform&aacute;tica b&aacute;sica, como organizar a &aacute;rea de trabalho, navegar e pesquisar na internet, fazer downloads, enviar emails, utilizar editores de textos, criar planilhas, slides, instalar e utilizar programas e equipamentos.<br /></span></td>
</tr>
<tr>
<td>
<p><span><strong>Vantagens do Certificado:</strong> </span></p>
<p><span>* Atualizar seu Curr&iacute;culo, aumentando suas chances para conquistar um bom emprego;</span><br /><span>* Aumentar suas chances de promo&ccedil;&atilde;o no emprego (atual);</span><br /><span>* Completar horas em atividades Extracurriculares (geralmente exigidas em Faculdades);</span><br /><span>* Progress&atilde;o Funcional para Servidores P&uacute;blicos;</span><br /><span>* Pr&eacute;-Requisito para concursos e cursos.</span></p>
</td>
</tr>
</tbody>
</table>
	<br>
	@guest
<a href="https://www.facil10.com.br/login" class="btn btn-outline-success btn-block">Quero Contratar</a>
@else
<a href="https://www.moip.com.br/PagamentoMoIP.do?id_carteira={{auth()->user()->id_carteira}}&valor=4999&nome=Curso Informática Básica&pagador_email={{auth()->user()->email}}&pagador_nome={{auth()->user()->name}}" class="btn btn-outline-success btn-block">Quero Contratar</a>
@endguest
@stop