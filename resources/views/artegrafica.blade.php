@extends('layout')
 
@section('title', 'ARTE GRÁFICA')
 
@section('content')
<div class="container">
<div class="clear-fix pad-top-40 ta-l" style="box-sizing: border-box; clear: both; font-family: &quot;Open Sans&quot;; font-size: 13px; padding-top: 40px !important;">
	<h2 class="fw-700 w-95" style="box-sizing: border-box; line-height: 1.1; color: inherit; margin-top: 18px; margin-bottom: 25.6406px; font-size: 1.9em; width: 1083px;">
		A um pre&ccedil;o acess&iacute;vel &eacute; poss&iacute;vel desenvolver sua arte com os designers da Fácil10, especializados em cria&ccedil;&atilde;o de materias gr&aacute;ficos.</h2>
	<p class="ta-l fs-1-4" style="box-sizing: border-box; margin: 0px 0px 9px; font-size: 1.4em !important;">
		Se voc&ecirc; ainda n&atilde;o tem o layout para imprimir seus materiais gr&aacute;ficos, poder&aacute; contar com a equipe de cria&ccedil;&atilde;o da Fácil10, composta por designers dedicados &agrave; cria&ccedil;&atilde;o e ajustes de layouts para impress&atilde;o de produtos gr&aacute;ficos.</p>
</div>
<div class="clear-fix pad-top-40 ta-c precos-prazos" style="box-sizing: border-box; clear: both; text-align: center; font-family: &quot;Open Sans&quot;; font-size: 13px; padding-top: 40px !important;">
	<div class="lined-heading x2" style="box-sizing: border-box;">
		<h2 style="box-sizing: border-box; font-weight: 500; line-height: 1.1; color: inherit; margin: 18px auto 0px; font-size: 1.9em; z-index: 9; position: relative; width: 684px; padding-bottom: 50px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">
			PRE&Ccedil;OS E&nbsp;<span style="box-sizing: border-box; font-weight: 700;">PRAZOS</span></h2>
	</div>
	<p class="ta-l fs-1-4" style="box-sizing: border-box; margin: 0px 0px 9px; font-size: 1.4em !important; text-align: left;">
		O pre&ccedil;o de cada servi&ccedil;o ser&aacute; automaticamente adicionado ao total da sua compra.&nbsp;<span style="box-sizing: border-box; font-weight: 700; color: rgb(0, 0, 0);">Os prazos para produ&ccedil;&atilde;o da arte come&ccedil;am a contar a partir da aprova&ccedil;&atilde;o do briefing</span>, enviado por voc&ecirc;. Ap&oacute;s finalizar a compra, voc&ecirc; precisar&aacute; enviar o briefing contendo todas as informa&ccedil;&otilde;es necess&aacute;rias para a produ&ccedil;&atilde;o do seu material, al&eacute;m dos arquivos de base como logotipos, fotos e textos.&nbsp;<span style="box-sizing: border-box; font-weight: 700; color: rgb(0, 0, 0);">Confira abaixo os valores e prazos para cria&ccedil;&atilde;o da arte para cada categoria:</span></p>
		
		
		
		<div class="container">
  <div class="row">
    <div class="col">
     <p class="clear-fix-2 ta-l w-95 fs-1-4" style="box-sizing: border-box; margin: 0px 0px 9px; width: 1083px; text-align: left; clear: both; font-size: 1.4em !important; padding-top: 50px !important;">
		<span style="box-sizing: border-box; font-weight: 700; color: rgb(0, 0, 0);">R$ 60,00 - At&eacute; 03 dias &uacute;teis</span>&nbsp;de cria&ccedil;&atilde;o para as seguintes categorias:</p>
	<ul class="ta-l w-88 fs-1-4 pad-0 ls-0" style="box-sizing: border-box; margin-top: 0px; margin-bottom: 9px; width: 1003.19px; list-style: none; text-align: left; padding-right: 0px !important; padding-left: 0px !important; font-size: 1.4em !important;">
		<li style="box-sizing: border-box;">
			&nbsp;Cart&atilde;o de Visita</li>
		<li style="box-sizing: border-box;">
			&nbsp;Mini Cart&otilde;es</li>
		<li style="box-sizing: border-box;">
			&nbsp;Cart&atilde;o Duplo</li>
	</ul>
    </div>
  </div>
</div>
		
		
	@guest	
		<a href="https://link.pagar.me/lSJeRbp7OTU" style="box-sizing: border-box; color: rgb(0, 180, 181); text-decoration-line: none; background-color: transparent; transition: all 0.3s ease 0s;" target="_blank"><button class="primary-btn planos-btn mt-20" style="border-radius: 50px; margin: 20px 0px 0px; font-family: inherit; font-size: inherit; line-height: 42px; overflow: visible; text-transform: uppercase; outline: none; background: rgb(0, 180, 181); padding-left: 40px; padding-right: 40px; border-width: 1px; border-style: solid; border-color: transparent; color: rgb(255, 255, 255); transition: all 0.3s ease 0s; cursor: pointer; position: relative; box-shadow: none; width: 269.984px;">CONTRATAR</button></a>
		
		@else
		<a href="https://link.pagar.me/lSJeRbp7OTU" style="box-sizing: border-box; color: rgb(0, 180, 181); text-decoration-line: none; background-color: transparent; transition: all 0.3s ease 0s;" target="_blank"><button class="primary-btn planos-btn mt-20" style="border-radius: 50px; margin: 20px 0px 0px; font-family: inherit; font-size: inherit; line-height: 42px; overflow: visible; text-transform: uppercase; outline: none; background: rgb(0, 180, 181); padding-left: 40px; padding-right: 40px; border-width: 1px; border-style: solid; border-color: transparent; color: rgb(255, 255, 255); transition: all 0.3s ease 0s; cursor: pointer; position: relative; box-shadow: none; width: 269.984px;">CONTRATAR</button></a>
		@endguest
		
		
		
		
		
		<div class="container">
  <div class="row">
    <div class="col">
    <p class="clear-fix-2 ta-l w-95 fs-1-4" style="box-sizing: border-box; margin: 0px 0px 9px; width: 1083px; text-align: left; clear: both; font-size: 1.4em !important; padding-top: 50px !important;">
		<span style="box-sizing: border-box; font-weight: 700; color: rgb(0, 0, 0);">R$ 75,00 - At&eacute; 03 dias &uacute;teis</span>&nbsp;de cria&ccedil;&atilde;o para as seguintes categorias:</p>
	<div class="col-md-4 pad-0" style="box-sizing: border-box; position: relative; min-height: 1px; padding: 0px !important; float: left; width: 380px;">
		<ul class="ta-l fs-1-4 pad-0 ls-0" style="box-sizing: border-box; margin: 15px 15px 15px 0px; list-style: none; text-align: left; padding-right: 0px !important; padding-left: 0px !important; font-size: 1.4em !important;">
			<li style="box-sizing: border-box;">
				&nbsp;Adesivo Papel / Vinil</li>
			<li style="box-sizing: border-box;">
				&nbsp;Adesivo Eletrost&aacute;tico</li>
			<li style="box-sizing: border-box;">
				&nbsp;Adesivo Parachoque</li>
			<li style="box-sizing: border-box;">
				&nbsp;Adesivo Perfurado</li>
			<li style="box-sizing: border-box;">
				&nbsp;Adesivos CD/DVD</li>
			<li style="box-sizing: border-box;">
				&nbsp;Atestado M&eacute;dico</li>
			<li style="box-sizing: border-box;">
				&nbsp;Bloco de Anota&ccedil;&atilde;o</li>
			<li style="box-sizing: border-box;">
				&nbsp;Bolacha de Chopp</li>
			<li style="box-sizing: border-box;">
				&nbsp;Calend&aacute;rio de Bolso</li>
			<li style="box-sizing: border-box;">
				&nbsp;Calend&aacute;rio de Mesa</li>
		</ul>
	</div>
	<div class="col-md-4 pad-0" style="box-sizing: border-box; position: relative; min-height: 1px; padding: 0px !important; float: left; width: 380px;">
		<ul class="ta-l fs-1-4 pad-0 ls-0" style="box-sizing: border-box; margin: 15px 15px 15px 0px; list-style: none; text-align: left; padding-right: 0px !important; padding-left: 0px !important; font-size: 1.4em !important;">
			<li style="box-sizing: border-box;">
				&nbsp;Calend&aacute;rio de Parede</li>
			<li style="box-sizing: border-box;">
				&nbsp;Calend&aacute;rio Folhinhas</li>
			<li style="box-sizing: border-box;">
				&nbsp;Cart&atilde;o Postal</li>
			<li style="box-sizing: border-box;">
				&nbsp;Flyer</li>
			<li style="box-sizing: border-box;">
				&nbsp;Folders</li>
			<li style="box-sizing: border-box;">
				&nbsp;Folheto</li>
			<li style="box-sizing: border-box;">
				&nbsp;Im&atilde; de Geladeira</li>
			<li style="box-sizing: border-box;">
				&nbsp;Jogos Americanos</li>
			<li style="box-sizing: border-box;">
				&nbsp;Marcadores de P&aacute;ginas</li>
			<li style="box-sizing: border-box;">
				&nbsp;N&atilde;o perturbe</li>
		</ul>
	</div>
	<div class="col-md-4 pad-0" style="box-sizing: border-box; position: relative; min-height: 1px; padding: 0px !important; float: left; width: 380px;">
		<ul class="ta-l fs-1-4 pad-0 ls-0" style="box-sizing: border-box; margin: 15px 15px 15px 0px; list-style: none; text-align: left; padding-right: 0px !important; padding-left: 0px !important; font-size: 1.4em !important;">
			<li style="box-sizing: border-box;">
				&nbsp;Panfleto</li>
			<li style="box-sizing: border-box;">
				&nbsp;Papel Bandeja</li>
			<li style="box-sizing: border-box;">
				&nbsp;Papel Timbrado</li>
			<li style="box-sizing: border-box;">
				&nbsp;Porta Copos</li>
			<li style="box-sizing: border-box;">
				&nbsp;Receitu&aacute;rio</li>
			<li style="box-sizing: border-box;">
				&nbsp;R&eacute;guas de Papel</li>
			<li style="box-sizing: border-box;">
				&nbsp;R&oacute;tulos</li>
			<li style="box-sizing: border-box;">
				&nbsp;Solapas / Cartelas</li>
			<li style="box-sizing: border-box;">
				&nbsp;Tags</li>
			<li style="box-sizing: border-box;">
				&nbsp;Ventarolas</li>
		</ul>
	</div>
    </div>
  </div>
</div>
		
		
		
	@guest	
		<a href="https://link.pagar.me/lHkK4p7u6U" style="box-sizing: border-box; color: rgb(0, 180, 181); text-decoration-line: none; background-color: transparent; transition: all 0.3s ease 0s;" target="_blank"><button class="primary-btn planos-btn mt-20" style="border-radius: 50px; margin: 20px 0px 0px; font-family: inherit; font-size: inherit; line-height: 42px; overflow: visible; text-transform: uppercase; outline: none; background: rgb(0, 180, 181); padding-left: 40px; padding-right: 40px; border-width: 1px; border-style: solid; border-color: transparent; color: rgb(255, 255, 255); transition: all 0.3s ease 0s; cursor: pointer; position: relative; box-shadow: none; width: 269.984px;">CONTRATAR</button></a>
		
		@else
		<a href="https://link.pagar.me/lHkK4p7u6U" style="box-sizing: border-box; color: rgb(0, 180, 181); text-decoration-line: none; background-color: transparent; transition: all 0.3s ease 0s;" target="_blank"><button class="primary-btn planos-btn mt-20" style="border-radius: 50px; margin: 20px 0px 0px; font-family: inherit; font-size: inherit; line-height: 42px; overflow: visible; text-transform: uppercase; outline: none; background: rgb(0, 180, 181); padding-left: 40px; padding-right: 40px; border-width: 1px; border-style: solid; border-color: transparent; color: rgb(255, 255, 255); transition: all 0.3s ease 0s; cursor: pointer; position: relative; box-shadow: none; width: 269.984px;">CONTRATAR</button></a>
		@endguest
		
		
		
		
		
		<div class="container">
  <div class="row">
    <div class="col">
	<p class="clear-fix-2 ta-l w-95 fs-1-4" style="box-sizing: border-box; margin: 0px 0px 9px; width: 1083px; text-align: left; clear: both; font-size: 1.4em !important; padding-top: 50px !important;">
		<span style="box-sizing: border-box; font-weight: 700; color: rgb(0, 0, 0);">R$ 95,00 - At&eacute; 05 dias &uacute;teis</span>&nbsp;de cria&ccedil;&atilde;o para as seguintes categorias:</p>
	<div class="col-md-4 pad-0" style="box-sizing: border-box; position: relative; min-height: 1px; padding: 0px !important; float: left; width: 380px;">
		<ul class="ta-l fs-1-4 pad-0 ls-0" style="box-sizing: border-box; margin: 15px 15px 15px 0px; list-style: none; text-align: left; padding-right: 0px !important; padding-left: 0px !important; font-size: 1.4em !important;">
			<li style="box-sizing: border-box;">
				&nbsp;Agenda</li>
			<li style="box-sizing: border-box;">
				&nbsp;Banner ou Faixa</li>
			<li style="box-sizing: border-box;">
				&nbsp;Caderno</li>
			<li style="box-sizing: border-box;">
				&nbsp;Cartazez</li>
			<li style="box-sizing: border-box;">
				&nbsp;Convites</li>
		</ul>
	</div>
	<div class="col-md-4 pad-0" style="box-sizing: border-box; position: relative; min-height: 1px; padding: 0px !important; float: left; width: 380px;">
		<ul class="ta-l fs-1-4 pad-0 ls-0" style="box-sizing: border-box; margin: 15px 15px 15px 0px; list-style: none; text-align: left; padding-right: 0px !important; padding-left: 0px !important; font-size: 1.4em !important;">
			<li style="box-sizing: border-box;">
				&nbsp;Encarte CD/DVD</li>
			<li style="box-sizing: border-box;">
				&nbsp;Envelope</li>
			<li style="box-sizing: border-box;">
				&nbsp;Envelope CD/DVD</li>
			<li style="box-sizing: border-box;">
				&nbsp;Pasta</li>
			<li style="box-sizing: border-box;">
				&nbsp;Sacolas de Papel</li>
		</ul>
    </div>
  </div>
</div>
		
		
		
	@guest	
		<a href="https://link.pagar.me/lByPwTXdpI" style="box-sizing: border-box; color: rgb(0, 180, 181); text-decoration-line: none; background-color: transparent; transition: all 0.3s ease 0s;" target="_blank"><button class="primary-btn planos-btn mt-20" style="border-radius: 50px; margin: 20px 0px 0px; font-family: inherit; font-size: inherit; line-height: 42px; overflow: visible; text-transform: uppercase; outline: none; background: rgb(0, 180, 181); padding-left: 40px; padding-right: 40px; border-width: 1px; border-style: solid; border-color: transparent; color: rgb(255, 255, 255); transition: all 0.3s ease 0s; cursor: pointer; position: relative; box-shadow: none; width: 269.984px;">CONTRATAR</button></a>
		
		@else
		<a href="https://link.pagar.me/lByPwTXdpI" style="box-sizing: border-box; color: rgb(0, 180, 181); text-decoration-line: none; background-color: transparent; transition: all 0.3s ease 0s;" target="_blank"><button class="primary-btn planos-btn mt-20" style="border-radius: 50px; margin: 20px 0px 0px; font-family: inherit; font-size: inherit; line-height: 42px; overflow: visible; text-transform: uppercase; outline: none; background: rgb(0, 180, 181); padding-left: 40px; padding-right: 40px; border-width: 1px; border-style: solid; border-color: transparent; color: rgb(255, 255, 255); transition: all 0.3s ease 0s; cursor: pointer; position: relative; box-shadow: none; width: 269.984px;">CONTRATAR</button></a>
		@endguest
		
		
		
		
		
		
		
			<div class="container">
  <div class="row">
    <div class="col">
	<p class="clear-fix-2 ta-l w-95 fs-1-4" style="box-sizing: border-box; margin: 0px 0px 9px; width: 1083px; text-align: left; clear: both; font-size: 1.4em !important; padding-top: 50px !important;">
		<span style="box-sizing: border-box; font-weight: 700; color: rgb(0, 0, 0);">R$ 150,00 - At&eacute; 05 dias &uacute;teis</span>&nbsp;de cria&ccedil;&atilde;o para as seguintes categorias:</p>
	<div class="col-md-4 pad-0" style="box-sizing: border-box; position: relative; min-height: 1px; padding: 0px !important; float: left; width: 380px;">
		<ul class="ta-l fs-1-4 pad-0 ls-0" style="box-sizing: border-box; margin: 15px 15px 15px 0px; list-style: none; text-align: left; padding-right: 0px !important; padding-left: 0px !important; font-size: 1.4em !important;">
			<li style="box-sizing: border-box;">
				&nbsp;Card&aacute;pio</li>
			<li style="box-sizing: border-box;">
				&nbsp;Encarte de Mercado</li>
		</ul>
	</div>
    </div>
  </div>
</div>

	@guest	
		<a href="https://link.pagar.me/lBkYcamda8" style="box-sizing: border-box; color: rgb(0, 180, 181); text-decoration-line: none; background-color: transparent; transition: all 0.3s ease 0s;" target="_blank"><button class="primary-btn planos-btn mt-20" style="border-radius: 50px; margin: 20px 0px 0px; font-family: inherit; font-size: inherit; line-height: 42px; overflow: visible; text-transform: uppercase; outline: none; background: rgb(0, 180, 181); padding-left: 40px; padding-right: 40px; border-width: 1px; border-style: solid; border-color: transparent; color: rgb(255, 255, 255); transition: all 0.3s ease 0s; cursor: pointer; position: relative; box-shadow: none; width: 269.984px;">CONTRATAR</button></a>
		
		@else
		<a href="https://link.pagar.me/lBkYcamda8" style="box-sizing: border-box; color: rgb(0, 180, 181); text-decoration-line: none; background-color: transparent; transition: all 0.3s ease 0s;" target="_blank"><button class="primary-btn planos-btn mt-20" style="border-radius: 50px; margin: 20px 0px 0px; font-family: inherit; font-size: inherit; line-height: 42px; overflow: visible; text-transform: uppercase; outline: none; background: rgb(0, 180, 181); padding-left: 40px; padding-right: 40px; border-width: 1px; border-style: solid; border-color: transparent; color: rgb(255, 255, 255); transition: all 0.3s ease 0s; cursor: pointer; position: relative; box-shadow: none; width: 269.984px;">CONTRATAR</button></a>
		@endguest

		<div class="container">
  <div class="row">
    <div class="col">
    <p class="clear-fix-2 ta-l w-95 fs-1-4" style="box-sizing: border-box; margin: 0px 0px 9px; width: 1083px; clear: both; font-family: &quot;Open Sans&quot;; font-size: 1.4em !important; padding-top: 50px !important;">
	<span style="box-sizing: border-box; font-weight: 700; color: rgb(0, 0, 0);">R$ 795,00 - At&eacute; 07 dias &uacute;teis</span>&nbsp;de cria&ccedil;&atilde;o para as seguintes categorias:</p>
<div class="col-md-4 pad-0" style="box-sizing: border-box; position: relative; min-height: 1px; float: left; width: 380px; font-family: &quot;Open Sans&quot;; font-size: 13px; text-align: center; padding: 0px !important;">
	<ul class="ta-l fs-1-4 pad-0 ls-0" style="box-sizing: border-box; margin: 15px 15px 15px 0px; list-style: none; text-align: left; padding-right: 0px !important; padding-left: 0px !important; font-size: 1.4em !important;">
		<li style="box-sizing: border-box;">
			&nbsp;Revista</li>
		<li style="box-sizing: border-box;">
			&nbsp;Jornal</li>
	</ul>
</div>
    </div>
  </div>
</div>

	@guest	
		<a href="https://link.pagar.me/lrJsCpQO6U" style="box-sizing: border-box; color: rgb(0, 180, 181); text-decoration-line: none; background-color: transparent; transition: all 0.3s ease 0s;" target="_blank"><button class="primary-btn planos-btn mt-20" style="border-radius: 50px; margin: 20px 0px 0px; font-family: inherit; font-size: inherit; line-height: 42px; overflow: visible; text-transform: uppercase; outline: none; background: rgb(0, 180, 181); padding-left: 40px; padding-right: 40px; border-width: 1px; border-style: solid; border-color: transparent; color: rgb(255, 255, 255); transition: all 0.3s ease 0s; cursor: pointer; position: relative; box-shadow: none; width: 269.984px;">CONTRATAR</button></a>
		
		@else
		<a href="https://link.pagar.me/lrJsCpQO6U" style="box-sizing: border-box; color: rgb(0, 180, 181); text-decoration-line: none; background-color: transparent; transition: all 0.3s ease 0s;" target="_blank"><button class="primary-btn planos-btn mt-20" style="border-radius: 50px; margin: 20px 0px 0px; font-family: inherit; font-size: inherit; line-height: 42px; overflow: visible; text-transform: uppercase; outline: none; background: rgb(0, 180, 181); padding-left: 40px; padding-right: 40px; border-width: 1px; border-style: solid; border-color: transparent; color: rgb(255, 255, 255); transition: all 0.3s ease 0s; cursor: pointer; position: relative; box-shadow: none; width: 269.984px;">CONTRATAR</button></a>
		@endguest

<div class="clear-fix pad-top-40 ta-c" style="box-sizing: border-box; clear: both; text-align: center; font-family: &quot;Open Sans&quot;; font-size: 13px; padding-top: 40px !important;">
	<div class="lined-heading x2" style="box-sizing: border-box;">
		<h2 style="box-sizing: border-box; font-weight: 500; line-height: 1.1; color: inherit; margin: 18px auto 0px; font-size: 1.9em; z-index: 9; position: relative; width: 684px; padding-bottom: 50px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">
			INSTRU&Ccedil;&Otilde;ES PARA&nbsp;<span style="box-sizing: border-box; font-weight: 700;">O BRIEFING</span></h2>
	</div>
	<p class="ta-l w-95 fs-1-4" style="box-sizing: border-box; margin: 0px 0px 9px; font-size: 1.4em !important; width: 1083px; text-align: left;">
		O briefing deve conter todas as informa&ccedil;&otilde;es necess&aacute;rias para que o designer consiga produzir o seu material. Ao envi&aacute;-lo voc&ecirc; precisa anexar os materiais de base, como logotipos em vetor, arquivos de fontes, textos e fotos. O briefing &eacute; enviado ap&oacute;s finalizar a compra.&nbsp;<span style="box-sizing: border-box; font-weight: 700; color: rgb(0, 0, 0);">Seu briefing deve informar:</span></p>
	<p class="ta-l w-95 fs-1-4 instrucoes" style="box-sizing: border-box; margin: 30px 0px; font-size: 1.4em !important; width: 1083px; text-align: left;">
		&nbsp;<span style="box-sizing: border-box; font-weight: 700; color: rgb(0, 0, 0);">Como o material ser&aacute; utilizado?</span><br style="box-sizing: border-box;" />
		<span style="box-sizing: border-box; font-weight: 700; color: rgb(0, 0, 0);">Ex.:&nbsp;</span>&Eacute; um material institucional; ser&aacute; entregue em uma feira de neg&oacute;cios; ser&aacute; feito somente para um evento; ser&aacute; entregue em visitas de vendas.</p>
	<p class="ta-l w-95 fs-1-4 instrucoes" style="box-sizing: border-box; margin: 30px 0px; font-size: 1.4em !important; width: 1083px; text-align: left;">
		&nbsp;<span style="box-sizing: border-box; font-weight: 700; color: rgb(0, 0, 0);">Como voc&ecirc; quer que o material fique?</span><br style="box-sizing: border-box;" />
		<span style="box-sizing: border-box; font-weight: 700; color: rgb(0, 0, 0);">Ex.:&nbsp;</span>Pensei em colocar a logo na frente e as informa&ccedil;&otilde;es de contato no verso; primeiro mostraremos fotos da empresa, e a lista de produtos vai na segunda parte.</p>
	<p class="ta-l w-95 fs-1-4 instrucoes" style="box-sizing: border-box; margin: 30px 0px; font-size: 1.4em !important; width: 1083px; text-align: left;">
		&nbsp;<span style="box-sizing: border-box; font-weight: 700; color: rgb(0, 0, 0);">Quais os detalhes obrigat&oacute;rios! O que n&atilde;o pode ter?</span><br style="box-sizing: border-box;" />
		<span style="box-sizing: border-box; font-weight: 700; color: rgb(0, 0, 0);">Ex.:&nbsp;</span>Tem que colocar o n&uacute;mero de registro de cada vendedor; n&atilde;o pode usar imagens gen&eacute;ricas; n&atilde;o pode usar a cor vermelha.</p>
	<p class="ta-l w-95 fs-1-4 instrucoes" style="box-sizing: border-box; margin: 30px 0px; font-size: 1.4em !important; width: 1083px; text-align: left;">
		&nbsp;<span style="box-sizing: border-box; font-weight: 700; color: rgb(0, 0, 0);">Que materiais da marca s&atilde;o essenciais?</span><br style="box-sizing: border-box;" />
		<span style="box-sizing: border-box; font-weight: 700; color: rgb(0, 0, 0);">Ex.:&nbsp;</span>Manuais de marca, diretrizes e orienta&ccedil;&otilde;es espec&iacute;ficas para a sua identidade visual.</p>
</div>
<div class="clear-fix pad-top-40 ta-c" style="box-sizing: border-box; clear: both; text-align: center; font-family: &quot;Open Sans&quot;; font-size: 13px; padding-top: 40px !important;">
	<div class="lined-heading x2" style="box-sizing: border-box;">
		<h2 style="box-sizing: border-box; font-weight: 500; line-height: 1.1; color: inherit; margin: 18px auto 0px; font-size: 1.9em; z-index: 9; position: relative; width: 684px; padding-bottom: 50px; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">
			REGRAS PARA&nbsp;<span style="box-sizing: border-box; font-weight: 700;">CRIA&Ccedil;&Atilde;O DE ARTE</span></h2>
	</div>
	<p class="ta-l w-95 fs-1-4 regras" style="box-sizing: border-box; margin: 30px 0px; font-size: 1.4em !important; width: 1083px; text-align: left;">
		&nbsp;<span style="box-sizing: border-box; font-weight: 700; color: rgb(0, 0, 0);">Os prazos para cria&ccedil;&atilde;o da arte</span>&nbsp;come&ccedil;am a contar a partir da aprova&ccedil;&atilde;o do briefing, enviado por voc&ecirc;, contendo todas as informa&ccedil;&otilde;es e materiais (como logotipo, fotos e conte&uacute;do) necess&aacute;rios para a cria&ccedil;&atilde;o.</p>
	<p class="ta-l w-95 fs-1-4 regras" style="box-sizing: border-box; margin: 30px 0px; font-size: 1.4em !important; width: 1083px; text-align: left;">
		&nbsp;<span style="box-sizing: border-box; font-weight: 700; color: rgb(0, 0, 0);">O prazo de produ&ccedil;&atilde;o do material</span>&nbsp;criado pelos designers da Fácil10 come&ccedil;a a contar somente ap&oacute;s sua aprova&ccedil;&atilde;o da arte.</p>
	<p class="ta-l w-95 fs-1-4 regras" style="box-sizing: border-box; margin: 30px 0px; font-size: 1.4em !important; width: 1083px; text-align: left;">
		&nbsp;<span style="box-sizing: border-box; font-weight: 700; color: rgb(0, 0, 0);">O pre&ccedil;o inclui 3 altera&ccedil;&otilde;es gratuitas na arte.</span>&nbsp;Caso voc&ecirc; deseje mais altera&ccedil;&otilde;es, ser&aacute; cobrado o valor de R$ 45,00 por altera&ccedil;&atilde;o solicitada.</p>
	<p class="ta-l w-95 fs-1-4 regras" style="box-sizing: border-box; margin: 30px 0px; font-size: 1.4em !important; width: 1083px; text-align: left;">
		&nbsp;<span style="box-sizing: border-box; font-weight: 700; color: rgb(0, 0, 0);">Cria&ccedil;&atilde;o de Logotipo n&atilde;o incluso.</span>&nbsp;Os valores para cria&ccedil;&atilde;o das artes n&atilde;o est&atilde;o inclusos a cria&ccedil;&atilde;o de logo marcas, caso queira adquirir esse servi&ccedil;o envie um e-mail para nossa equipe solicitando a cria&ccedil;&atilde;o. O valor para desenvolvimento de Logotipo &eacute; de a partir de R$500,00.</p>
	<p class="ta-l w-95 fs-1-4 regras" style="box-sizing: border-box; margin: 30px 0px; font-size: 1.4em !important; width: 1083px; text-align: left;">
		&nbsp;<span style="box-sizing: border-box; font-weight: 700; color: rgb(0, 0, 0);">O envio do briefing dever&aacute; ser feito atrav&eacute;s de sua conta no site.</span>&nbsp;Ap&oacute;s finalizarmos a cria&ccedil;&atilde;o da sua arte, enviaremos um e-mail informando que est&aacute; pronto, ent&atilde;o &eacute; necess&aacute;rio acessar sua conta no site e aprovar. Se necess&aacute;rio enviaremos um e-mail solicitando mais informa&ccedil;&otilde;es para a cria&ccedil;&atilde;o, por&eacute;m &eacute; necess&aacute;rio enviar os detalhes atrav&eacute;s de sua conta no site, n&atilde;o aceitaremos briefings enviados por e-mail.</p>
	<p class="ta-l w-95 fs-1-4 regras" style="box-sizing: border-box; margin: 30px 0px; font-size: 1.4em !important; width: 1083px; text-align: left;">
		&nbsp;<span style="box-sizing: border-box; font-weight: 700; color: rgb(0, 0, 0);">Por gentileza, conferir a arte como um todo.</span>&nbsp;Textos contidos no material, conferir se n&atilde;o h&aacute; nenhum erro ortogr&aacute;fico, averiguar se est&aacute; tudo disposto conforme o solicitado. O material produzido ser&aacute; fiel ao arquivo aprovado pelo cliente. Ap&oacute;s aprova&ccedil;&atilde;o, o mesmo &eacute; enviado para produ&ccedil;&atilde;o. N&atilde;o nos responsabilizaremos por altera&ccedil;&otilde;es ou reparos ap&oacute;s aprova&ccedil;&atilde;o, por favor analise com cuidado.</p>
</div>
<div class="clear-fix pad-top-40 ta-c" style="box-sizing: border-box; clear: both; text-align: center; font-family: &quot;Open Sans&quot;; font-size: 13px; padding-top: 40px !important;">
	<p class="w-95 fs-1-4 ta-c" style="box-sizing: border-box; margin: 0px 0px 9px; width: 1083px; font-size: 1.4em !important;">
		Conhe&ccedil;a nossos produtos e fa&ccedil;a agora mesmo seu pedido!</p>
</div>
<p>
	&nbsp;</p>
	</div>

@stop