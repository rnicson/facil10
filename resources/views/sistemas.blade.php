@extends('layout')
 
@section('title', 'DESENVOLVIMENTO SISTEMAS')
 
@section('content')
<br><br><br>
<h3 style="box-sizing: border-box; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-weight: 400; line-height: 24px; color: rgb(0, 136, 204); margin: 0px 0px 33px; font-size: 1.8em; text-transform: uppercase;">
	PRECISA DE UM SISTEMA DE GEST&Atilde;O PARA O SEU NEG&Oacute;CIO?</h3>
<p style="box-sizing: border-box; margin: 0px 0px 20px; color: rgb(119, 119, 119); line-height: 24px; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;">
	Administrar qualquer tipo de neg&oacute;cio sem um sistema de gest&atilde;o eficiente pode ser uma tarefa complicada. &Eacute; pensando nisso que a FÁCIL10 se coloca &agrave; frente e oferece a voc&ecirc; a cria&ccedil;&atilde;o de um sistema de gest&atilde;o totalmente modelado para a sua empresa.</p>
<p style="box-sizing: border-box; margin: 0px 0px 20px; color: rgb(119, 119, 119); line-height: 24px; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;">
	Veja abaixo como funciona nosso servi&ccedil;o:</p>
	
	
	
	
	<div class="container">
  <div class="row">
    <div class="col-sm">
      	<h4 class="shorter" style="box-sizing: border-box; font-family: inherit; font-weight: 400; line-height: 27px; color: rgb(14, 14, 14); margin: 0px; font-size: 1.4em;">
								Clientes</h4>
								<p class="tall" style="box-sizing: border-box; margin: 0px 0px 20px; line-height: 24px;">
								O cliente percebe que necessita de um sistema de gest&atilde;o adaptado ao pr&oacute;prio neg&oacute;cio.</p>
    </div>
    <div class="col-sm">
      							<h4 class="shorter" style="box-sizing: border-box; font-family: inherit; font-weight: 400; line-height: 27px; color: rgb(14, 14, 14); margin: 0px; font-size: 1.4em;">
								Reuni&atilde;o</h4>
															<p class="tall" style="box-sizing: border-box; margin: 0px 0px 20px; line-height: 24px;">
								Em uma reuni&atilde;o, o cliente passa as informa&ccedil;&otilde;es sobre o que precisa no sistema. Ap&oacute;s essa reuni&atilde;o, enviamos um or&ccedil;amento para a produ&ccedil;&atilde;o desse sistema.</p>
    </div>
    <div class="col-sm">
      							<h4 class="shorter" style="box-sizing: border-box; font-family: inherit; font-weight: 400; line-height: 27px; color: rgb(14, 14, 14); margin: 0px; font-size: 1.4em;">
								Fluxograma/Design</h4>
															<p class="tall" style="box-sizing: border-box; margin: 0px 0px 20px; line-height: 24px;">
								Criamos um fluxograma indicando a forma como ser&aacute; feita a cria&ccedil;&atilde;o do sistema e as datas de entrega de cada item e cria&ccedil;&atilde;o do design do sistema.</p>
    </div>
  </div>
</div>

	<div class="container">
  <div class="row">
    <div class="col-sm">
      	<h4 class="shorter" style="box-sizing: border-box; font-family: inherit; font-weight: 400; line-height: 27px; color: rgb(14, 14, 14); margin: 0px; font-size: 1.4em;">
								Desenvolvimento</h4>
						<p class="tall" style="box-sizing: border-box; margin: 0px 0px 20px; line-height: 24px;">
								Ap&oacute;s o cliente aprovar o design, come&ccedil;amos o desenvolvimento do sistema de gest&atilde;o.</p>
    </div>
    <div class="col-sm">
							<h4 class="shorter" style="box-sizing: border-box; font-family: inherit; font-weight: 400; line-height: 27px; color: rgb(14, 14, 14); margin: 0px; font-size: 1.4em;">
								Testes</h4>
							<p class="tall" style="box-sizing: border-box; margin: 0px 0px 20px; line-height: 24px;">
								Terminada a etapa de desenvolvimento, passamos o sistema para o cliente fazer os testes e fazemos os ajustes necess&aacute;rios.</p>
    </div>
    <div class="col-sm">
							<h4 class="shorter" style="box-sizing: border-box; font-family: inherit; font-weight: 400; line-height: 27px; color: rgb(14, 14, 14); margin: 0px; font-size: 1.4em;">
								Disponibiliza&ccedil;&atilde;o</h4>
							<p class="tall" style="box-sizing: border-box; margin: 0px 0px 20px; line-height: 24px;">
								Pronto! Um sistema de gest&atilde;o espec&iacute;fico para o seu neg&oacute;cio est&aacute; pronto para ser utilizado.</p>
    </div>
  </div>
</div>
</br></br>

			</div>
		</div>
	</div>
</div>
	<p>
		<a class="botaoHome abreFormulario botaoCriacaoSites" data-formulario="Página Criação de Sites" href="https://api.whatsapp.com/send?phone=5511945040347&text=Encontrei%20o%20site%20da%20F%C3%A1cil10%2Ce%20tenho%20interesse%20em%20seus%20servi%C3%A7os." style="margin: 0px; padding: 0px 25px; font-weight: 450; transition: all 600ms ease 0s; text-decoration-line: none; color: rgb(255, 255, 255); border-radius: 25px; line-height: 50px; border: 1px solid rgb(255, 51, 51); background: #7F00FF; width:100%; display: inline-block; font-size: 1.2em; text-transform: uppercase; letter-spacing: 1px; white-space: nowrap; font-family: Ubuntu, sans-serif; text-align: center;">SOLICITE UM OR&Ccedil;AMENTO!</a></p>
<p>
	&nbsp;</p>

@stop