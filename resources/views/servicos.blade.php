@extends('layout')
 
@section('title')
 
@section('content')
    <center><h1>Meus Serviços</h1></center>
    <div class="row" style="background-color: #ccc;">
  <div class="col-md-2"><B>Serviço</B></div>
  <div class="col-md-2"><B>Valor</B></div>
  <div class="col-md-2"><B>Data</B></div>
  <div class="col-md-2"><B>Status</B></div>
  <div class="col-md-4"><B>Observação</B></div>
</div>

<br>
<div class="row" style="background-color: #fff;">
@foreach($servicos as $servicos)
  <div class="col-md-2">{{$servicos->id_servico}}</div>
  <div class="col-md-2">{{$servicos->valor}}</div>
  <div class="col-md-2">{{ date( 'd/m/Y' , strtotime($servicos->data_criado))}}</div>
  <div class="col-md-2">{{$servicos->status}}</div>
  <div class="col-md-4">{{$servicos->observacao}}</div>
      <hr>
@endforeach
</div>
@stop