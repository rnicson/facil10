@extends('layout')
 
@section('title', 'SEO')
 
@section('content')
<section id="sp-section-3" style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 14px;">
	<div class="container" style="box-sizing: border-box; padding-right: 15px; padding-left: 15px; margin-right: auto; margin-left: auto; width: 1170px;">
		<div class="row" style="box-sizing: border-box; margin-right: -15px; margin-left: -15px;">
			<div class="col-xs-12 col-sm-12 col-md-12" id="sp-pagebuilder" style="box-sizing: border-box; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; float: left; width: 1170px;">
				<div class="sp-column " style="box-sizing: border-box;">
					<div class="sp-module " style="box-sizing: border-box; margin-top: 0px;">
						<div class="sp-module-content" style="box-sizing: border-box;">
							<div class="custom" style="box-sizing: border-box;">
								<div class="col-md-12 hidden-sm hidden-xs" style="box-sizing: border-box; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; float: left; width: 1140px;">
									<h1 class="h1title1" style="box-sizing: border-box; margin: 10px 0px 80px; font-size: 40px; font-family: &quot;Open Sans Condensed&quot;, sans-serif; font-weight: 300; line-height: 49px; color: rgb(92, 91, 95); text-transform: uppercase; text-align: center;">
										<span style="box-sizing: border-box; font-weight: 700;">OTIMIZA&Ccedil;&Atilde;O DE SITES SUA EMPRESA 24 HORAS&nbsp;<span style="box-sizing: border-box; color: green;">NA PRIMEIRA P&Aacute;GINA DO GOOGLE.</span></span><br style="box-sizing: border-box;" />
										<span style="box-sizing: border-box; color: green;">SEM PAGAR POR CLIQUES.</span></h1>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="sp-position-2" style="box-sizing: border-box; background: rgb(241, 242, 244); padding: 30px 0px; color: rgb(51, 51, 51); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 14px;">
	<div class="container" style="box-sizing: border-box; padding-right: 15px; padding-left: 15px; margin-right: auto; margin-left: auto; width: 1170px;">
		<div class="row" style="box-sizing: border-box; margin-right: -15px; margin-left: -15px;">
			<div class="col-xs-12 col-sm-12 col-md-12" id="sp-position2" style="box-sizing: border-box; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; float: left; width: 1170px;">
				<div class="sp-column " style="box-sizing: border-box;">
					<div class="sp-module " style="box-sizing: border-box; margin-top: 0px;">
						<div class="sp-module-content" style="box-sizing: border-box;">
							<div class="custom" style="box-sizing: border-box;">
								<div class="col-md-12 col-sm-12 col-xs-12" style="box-sizing: border-box; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; float: left; width: 1140px;">
									<div class="col-md-12 col-sm-12 col-xs-12" style="box-sizing: border-box; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; float: left; width: 1110px;">
										<img alt="" class="setaimg" src="https://www.g5seo.com.br/otimizacao-de-sites/images/seta.png" style="box-sizing: border-box; border: 0px; vertical-align: middle; display: block; max-width: 100%; height: auto; position: relative; top: -31px; margin-left: auto; margin-right: auto;" /></div>
									<div class="col-md-12 hidden-sm hidden-xs" style="box-sizing: border-box; position: relative; min-height: 1px; padding: 0px; float: left; width: 1110px;">
										<h1 class="h1title3" style="box-sizing: border-box; margin: 0px 0px 60px; font-size: 30px; font-family: &quot;Open Sans Condensed&quot;, sans-serif; line-height: 35px; color: rgb(0, 149, 67); text-transform: uppercase;">
											VANTAGENS QUE UM SITE OTIMIZADO OFERECE:</h1>
									</div>
								</div>
								<div class="col-md-12 hidden-sm hidden-xs" style="box-sizing: border-box; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; float: left; width: 1140px;">
									<div class="col-md-12 hidden-sm hidden-xs box12vantagens" style="box-sizing: border-box; position: relative; min-height: 1px; padding: 0px; float: left; width: 1110px;">
										<div class="col-md-6 box6vantagens" style="box-sizing: border-box; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; float: left; width: 555px; padding-bottom: 30px;">
											<div class="col-md-3 widthbox boxcheck" style="box-sizing: border-box; position: relative; min-height: 1px; padding: 0px; float: left; width: 120px; max-width: 120px;">
												<img alt="" class="checkico" src="https://www.g5seo.com.br/otimizacao-de-sites/images/check.png" style="box-sizing: border-box; border: 0px; vertical-align: middle; display: block; max-width: 100%; height: auto; padding-top: 15px;" /></div>
											<div class="col-md-9 boxtextvan" style="box-sizing: border-box; position: relative; min-height: 1px; padding: 0px; float: left; width: 393.75px;">
												<p class="pvantagem" style="box-sizing: border-box; margin: 0px 0px 2px; color: rgb(92, 91, 95);">
													<span class="titulovantagem" style="box-sizing: border-box; font-size: 23px; text-transform: uppercase; font-weight: 700; font-family: &quot;open sans condensed&quot;; line-height: 50px;">APARE&Ccedil;A QUANDO MAIS PRECISAM</span><br style="box-sizing: border-box;" />
													Apare&ccedil;a para seus potenciais clientes no momento exato em que eles buscam e possuem real interesse em seu produto ou servi&ccedil;o.</p>
											</div>
										</div>
										<div class="col-md-6 box6vantagens" style="box-sizing: border-box; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; float: left; width: 555px; padding-bottom: 30px;">
											<div class="col-md-3 widthbox boxcheck" style="box-sizing: border-box; position: relative; min-height: 1px; padding: 0px; float: left; width: 120px; max-width: 120px;">
												<img alt="" class="checkico" src="https://www.g5seo.com.br/otimizacao-de-sites/images/check.png" style="box-sizing: border-box; border: 0px; vertical-align: middle; display: block; max-width: 100%; height: auto; padding-top: 15px;" /></div>
											<div class="col-md-9 boxtextvan" style="box-sizing: border-box; position: relative; min-height: 1px; padding: 0px; float: left; width: 393.75px;">
												<p class="pvantagem" style="box-sizing: border-box; margin: 0px 0px 2px; color: rgb(92, 91, 95);">
													<span class="titulovantagem" style="box-sizing: border-box; font-size: 23px; text-transform: uppercase; font-weight: 700; font-family: &quot;open sans condensed&quot;; line-height: 50px;">DESTAQUE-SE 24H POR DIA</span><br style="box-sizing: border-box;" />
													Diferente dos Links Patrocinados, o site permanece na 1&ordf; p&aacute;gina do Google 24h por dia. Aumentando a possibilidade de vendas de produtos ou servi&ccedil;os</p>
											</div>
										</div>
									</div>
									<div class="col-md-12 hidden-sm hidden-xs box12vantagens" style="box-sizing: border-box; position: relative; min-height: 1px; padding: 0px; float: left; width: 1110px;">
										<div class="col-md-6 box6vantagens" style="box-sizing: border-box; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; float: left; width: 555px; padding-bottom: 30px;">
											<div class="col-md-3 widthbox boxcheck" style="box-sizing: border-box; position: relative; min-height: 1px; padding: 0px; float: left; width: 120px; max-width: 120px;">
												<img alt="" class="checkico" src="https://www.g5seo.com.br/otimizacao-de-sites/images/check.png" style="box-sizing: border-box; border: 0px; vertical-align: middle; display: block; max-width: 100%; height: auto; padding-top: 15px;" /></div>
											<div class="col-md-9 boxtextvan" style="box-sizing: border-box; position: relative; min-height: 1px; padding: 0px; float: left; width: 393.75px;">
												<p class="pvantagem" style="box-sizing: border-box; margin: 0px 0px 2px; color: rgb(92, 91, 95);">
													<span class="titulovantagem" style="box-sizing: border-box; font-size: 23px; text-transform: uppercase; font-weight: 700; font-family: &quot;open sans condensed&quot;; line-height: 50px;">AMPLIE O FLUXO DE CLIENTES</span><br style="box-sizing: border-box;" />
													Atrav&eacute;s da otimiza&ccedil;&atilde;o &eacute; poss&iacute;vel expandir o fluxo de clientes que visitam o site, atraindo novas vendas e fortalecendo a marca no mercado.</p>
											</div>
										</div>
										<div class="col-md-6 box6vantagens" style="box-sizing: border-box; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; float: left; width: 555px; padding-bottom: 30px;">
											<div class="col-md-3 widthbox boxcheck" style="box-sizing: border-box; position: relative; min-height: 1px; padding: 0px; float: left; width: 120px; max-width: 120px;">
												<img alt="" class="checkico" src="https://www.g5seo.com.br/otimizacao-de-sites/images/check.png" style="box-sizing: border-box; border: 0px; vertical-align: middle; display: block; max-width: 100%; height: auto; padding-top: 15px;" /></div>
											<div class="col-md-9 boxtextvan" style="box-sizing: border-box; position: relative; min-height: 1px; padding: 0px; float: left; width: 393.75px;">
												<p class="pvantagem" style="box-sizing: border-box; margin: 0px 0px 2px; color: rgb(92, 91, 95);">
													<span class="titulovantagem" style="box-sizing: border-box; font-size: 23px; text-transform: uppercase; font-weight: 700; font-family: &quot;open sans condensed&quot;; line-height: 50px;">AUMENTE O LUCRO</span><br style="box-sizing: border-box;" />
													Com o aumento do fluxo de clientes que acessam o site, h&aacute; um aumento significativo na possibilidade de novas vendas ou na pr&oacute;pria divulga&ccedil;&atilde;o do neg&oacute;cio em si.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop