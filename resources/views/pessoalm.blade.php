@extends('layout')
 
@section('title', 'INFRAESTRUTURA PESSOAL')
 
@section('content')
<div class="container">
<center><h1>Precisa de Suporte Técnico para Pessoa Física?</h1></center>
<div class="row">
    <div class="col-md-12">
        Conheça os serviços da Fácil10,trabalhamos diariamente solucionando os problemas de nossos clientes seja Pessoa Física ou Jurídica.
    </div>
    </div>
    </div>
    <hr>
    <div class="container">
  <div class="row">
    <div class="col">
      <img src="https://facil10.com.br/img/foto.jpg" width="200px">
    </div>
    <div class="col" style="margin-top:30px;">
       Em curto prazo de tempo após confirmação de pagamento do chamado um de nossos técnicos entrarão em contato para lhe atender.
    </div>
  </div>
</div>
<hr>
<center>
    <h2>Planos Remotos</h2>
<div class"container">
<div class="row" style="background-color:#aaa;">
  <div class="col-md-3"><b>1 Chamado</b></div>
</div>
<div class="row" style="background-color:#fff;">
  <div class="col-md-3">Email</div>
  <div class="col-md-3">Office</div>
  <div class="col-md-3">Programas</div>
  <div class="col-md-3">Configurações</div>
  <div class="col-md-3">Vírus</div>
</div>
<hr>
<div class="row" style="background-color:#fff; color:#ff0000;">
  <div class="col-md-3">R$10,00</div>
</div>
<hr>
@guest
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://facil10.com.br/login"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@else
<div class="row" style="background-color:#fff;">
 <div class="col-md-3"><a href="https://www.moip.com.br/PagamentoMoIP.do?id_carteira={{auth()->user()->id_carteira}}&valor=1000&nome=1 Chamado Remoto Pessoal&pagador_email={{auth()->user()->email}}&pagador_nome={{auth()->user()->name}}"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@endguest

</br>

<div class="row" style="background-color:#aaa;">
  <div class="col-md-3"><b>5 Chamados</b></div>
</div>
<div class="row" style="background-color:#fff;">
  <div class="col-md-3">Email</div>
  <div class="col-md-3">Office</div>
  <div class="col-md-3">Programas</div>
  <div class="col-md-3">Configurações</div>
  <div class="col-md-3">Vírus</div>
</div>
<hr>
<div class="row" style="background-color:#fff; color:#ff0000;">
  <div class="col-md-3">R$40,00</div>
</div>
<hr>
@guest
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://facil10.com.br/login"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@else
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://www.moip.com.br/PagamentoMoIP.do?id_carteira={{auth()->user()->id_carteira}}&valor=4000&nome=5 Chamado Remoto Pessoal&pagador_email={{auth()->user()->email}}&pagador_nome={{auth()->user()->name}}"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@endguest

</br>

<div class="row" style="background-color:#aaa;">
  <div class="col-md-3"><b>10 Chamados</b></div>
</div>
<div class="row" style="background-color:#fff;">
  <div class="col-md-3">Email</div>
  <div class="col-md-3">Office</div>
  <div class="col-md-3">Programas</div>
  <div class="col-md-3">Configurações</div>
  <div class="col-md-3">Vírus</div>
</div>
<hr>
<div class="row" style="background-color:#fff; color:#ff0000;">
  <div class="col-md-3">R$70,00</div>
</div>
<hr>
@guest
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://facil10.com.br/login"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@else
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://www.moip.com.br/PagamentoMoIP.do?id_carteira={{auth()->user()->id_carteira}}&valor=7000&nome=10 Chamado Remoto Pessoal&pagador_email={{auth()->user()->email}}&pagador_nome={{auth()->user()->name}}"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@endguest

</br>

<div class="row" style="background-color:#aaa;">
  <div class="col-md-3"><b>30 Chamados</b></div>
</div>
<div class="row" style="background-color:#fff;">
  <div class="col-md-3">Email</div>
  <div class="col-md-3">Office</div>
  <div class="col-md-3">Programas</div>
  <div class="col-md-3">Configurações</div>
  <div class="col-md-3">Vírus</div>
</div>
<hr>
<div class="row" style="background-color:#fff; color:#ff0000;">
  <div class="col-md-3">R$180,00</div>
</div>
<hr>
@guest
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://facil10.com.br/login"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@else
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://www.moip.com.br/PagamentoMoIP.do?id_carteira={{auth()->user()->id_carteira}}&valor=18000&nome=30 Chamado Remoto Pessoal&pagador_email={{auth()->user()->email}}&pagador_nome={{auth()->user()->name}}"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@endguest
</div>
</center>
<hr>
<center>
    <h2>Planos Presenciais</h2>
<div class"container">
<div class="row" style="background-color:#aaa;">
  <div class="col-md-3"><b>1 Chamado</b></div>
</div>
<div class="row" style="background-color:#fff;">
  <div class="col-md-3">Email</div>
  <div class="col-md-3">Office</div>
  <div class="col-md-3">Programas</div>
  <div class="col-md-3">Configurações</div>
  <div class="col-md-3">Vírus</div>
</div>
<hr>
<div class="row" style="background-color:#fff; color:#ff0000;">
  <div class="col-md-3">R$60,00</div>
</div>
<hr>
@guest
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://facil10.com.br/login"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@else
<div class="row" style="background-color:#fff;">
 <div class="col-md-3"><a href="https://www.moip.com.br/PagamentoMoIP.do?id_carteira={{auth()->user()->id_carteira}}&valor=6000&nome=1 Chamado Presencial Pessoal&pagador_email={{auth()->user()->email}}&pagador_nome={{auth()->user()->name}}"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@endguest

</br>

<div class="row" style="background-color:#aaa;">
  <div class="col-md-3"><b>5 Chamados</b></div>
</div>
<div class="row" style="background-color:#fff;">
  <div class="col-md-3">Email</div>
  <div class="col-md-3">Office</div>
  <div class="col-md-3">Programas</div>
  <div class="col-md-3">Configurações</div>
  <div class="col-md-3">Vírus</div>
</div>
<hr>
<div class="row" style="background-color:#fff; color:#ff0000;">
  <div class="col-md-3">R$250,00</div>
</div>
<hr>
@guest
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://facil10.com.br/login"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@else
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://www.moip.com.br/PagamentoMoIP.do?id_carteira={{auth()->user()->id_carteira}}&valor=25000&nome=5 Chamado Presencial Pessoal&pagador_email={{auth()->user()->email}}&pagador_nome={{auth()->user()->name}}"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@endguest

</br>

<div class="row" style="background-color:#aaa;">
  <div class="col-md-3"><b>10 Chamados</b></div>
</div>
<div class="row" style="background-color:#fff;">
  <div class="col-md-3">Email</div>
  <div class="col-md-3">Office</div>
  <div class="col-md-3">Programas</div>
  <div class="col-md-3">Configurações</div>
  <div class="col-md-3">Vírus</div>
</div>
<hr>
<div class="row" style="background-color:#fff; color:#ff0000;">
  <div class="col-md-3">R$400,00</div>
</div>
<hr>
@guest
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://facil10.com.br/login"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@else
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://www.moip.com.br/PagamentoMoIP.do?id_carteira={{auth()->user()->id_carteira}}&valor=40000&nome=10 Chamado Presencial Pessoal&pagador_email={{auth()->user()->email}}&pagador_nome={{auth()->user()->name}}"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@endguest

</br>

<div class="row" style="background-color:#aaa;">
  <div class="col-md-3"><b>30 Chamados</b></div>
</div>
<div class="row" style="background-color:#fff;">
  <div class="col-md-3">Email</div>
  <div class="col-md-3">Office</div>
  <div class="col-md-3">Programas</div>
  <div class="col-md-3">Configurações</div>
  <div class="col-md-3">Vírus</div>
</div>
<hr>
<div class="row" style="background-color:#fff; color:#ff0000;">
  <div class="col-md-3">R$1.000,00</div>
</div>
<hr>
@guest
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://facil10.com.br/login"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@else
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://www.moip.com.br/PagamentoMoIP.do?id_carteira={{auth()->user()->id_carteira}}&valor=100000&nome=30 Chamado Presencial Pessoal&pagador_email={{auth()->user()->email}}&pagador_nome={{auth()->user()->name}}"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@endguest
</div>
</center>
@stop