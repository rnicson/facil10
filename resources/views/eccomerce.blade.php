@extends('layout')
 
@section('title', 'LOJA VIRTUAL')
 
@section('content')
<p>
	&nbsp;</p>
<section class="mine-options" style="box-sizing: border-box; padding-top: 50px; color: rgb(74, 74, 74); font-family: Montserrat; font-size: 16px;">
	<div class="mine-container" style="box-sizing: border-box; width: 1170px; max-width: 1170px; margin: 0px auto; padding-right: 15px; padding-left: 15px;">
		<div class="mine-options-desktop" style="box-sizing: border-box;">
			<div class="mine-options-content tab-content" style="box-sizing: border-box; margin-bottom: 50px; text-align: center;">
				<div class="mine-options-tab tab-pane active" id="home" role="tabpanel" style="box-sizing: border-box; margin-top: 50px; overflow: hidden;">
					<div class="mine-option-item col-md-3 col-sm-3" style="box-sizing: border-box; position: relative; min-height: 1px; padding-left: 15px; padding-right: 15px; float: left; width: 285px; margin-bottom: 40px;">
						<img alt="" class="mine-options-tab-img" src="https://www.minestore.com.br/assets/images/icon/ic-crie-loja.svg" style="box-sizing: border-box; border: 0px; vertical-align: middle; max-width: 100%; height: 80px; width: 80px; display: block; margin: 0px auto;" />
						<h3 class="mine-options-tab-title" style="box-sizing: border-box; font-family: Montserrat, sans-serif; line-height: 1.5em; color: rgb(238, 39, 120); margin: 30px 0px 5px; font-size: 16px; -webkit-font-smoothing: antialiased; padding: 0px; min-height: 48px;">
							Crie sua loja virtual<br style="box-sizing: border-box;" /></h3>
						<p class="mine-options-tab-text" style="box-sizing: border-box; margin: 0px; -webkit-font-smoothing: antialiased; font-family: Montserrat, sans-serif; padding: 0px;">
							N&oacute;s tamb&eacute;m acreditamos no seu sonho! Crie sua loja e paga uma pequena taxa mensal.</p>
					</div>
					<div class="mine-option-item col-md-3 col-sm-3" style="box-sizing: border-box; position: relative; min-height: 1px; padding-left: 15px; padding-right: 15px; float: left; width: 285px; margin-bottom: 40px;">
						<img alt="" class="mine-options-tab-img" src="https://www.minestore.com.br/assets/images/icon/ic-mensalidade.svg" style="box-sizing: border-box; border: 0px; vertical-align: middle; max-width: 100%; height: 80px; width: 80px; display: block; margin: 0px auto;" />
						<h3 class="mine-options-tab-title" style="box-sizing: border-box; font-family: Montserrat, sans-serif; line-height: 1.5em; color: rgb(238, 39, 120); margin: 30px 0px 5px; font-size: 16px; -webkit-font-smoothing: antialiased; padding: 0px; min-height: 48px;">
							Hospedagem Gratuita</h3>
						<p class="mine-options-tab-text" style="box-sizing: border-box; margin: 0px; -webkit-font-smoothing: antialiased; font-family: Montserrat, sans-serif; padding: 0px;">
							Nossa loja virtual não precisa se preocupar com Hospedagem,pois nós oferecemos gratuitamente.</p>
					</div>
					<div class="mine-option-item col-md-3 col-sm-3" style="box-sizing: border-box; position: relative; min-height: 1px; padding-left: 15px; padding-right: 15px; float: left; width: 285px; margin-bottom: 40px;">
						<img alt="" class="mine-options-tab-img" src="https://www.minestore.com.br/assets/images/icon/ic-limitacoes.svg" style="box-sizing: border-box; border: 0px; vertical-align: middle; max-width: 100%; height: 80px; width: 80px; display: block; margin: 0px auto;" />
						<h3 class="mine-options-tab-title" style="box-sizing: border-box; font-family: Montserrat, sans-serif; line-height: 1.5em; color: rgb(238, 39, 120); margin: 30px 0px 5px; font-size: 16px; -webkit-font-smoothing: antialiased; padding: 0px; min-height: 48px;">
							Sem limita&ccedil;&otilde;es</h3>
						<p class="mine-options-tab-text" style="box-sizing: border-box; margin: 0px; -webkit-font-smoothing: antialiased; font-family: Montserrat, sans-serif; padding: 0px;">
							Seja livre! Aqui na Fácil10 n&atilde;o h&aacute; limites. Sem limite de produtos, fotos ou acessos.</p>
					</div>
					<div class="mine-option-item col-md-3 col-sm-3" style="box-sizing: border-box; position: relative; min-height: 1px; padding-left: 15px; padding-right: 15px; float: left; width: 285px; margin-bottom: 40px;">
						<img alt="" class="mine-options-tab-img" src="https://www.minestore.com.br/assets/images/icon/ic-programar.svg" style="box-sizing: border-box; border: 0px; vertical-align: middle; max-width: 100%; height: 80px; width: 80px; display: block; margin: 0px auto;" />
						<h3 class="mine-options-tab-title" style="box-sizing: border-box; font-family: Montserrat, sans-serif; line-height: 1.5em; color: rgb(238, 39, 120); margin: 30px 0px 5px; font-size: 16px; -webkit-font-smoothing: antialiased; padding: 0px; min-height: 48px;">
							N&atilde;o precisa saber<br style="box-sizing: border-box;" />
							programar</h3>
						<p class="mine-options-tab-text" style="box-sizing: border-box; margin: 0px; -webkit-font-smoothing: antialiased; font-family: Montserrat, sans-serif; padding: 0px;">
							Possu&iacute;mos temas prontos, lindos e gratuitos para o layout da sua loja virtual! &Eacute; s&oacute; escolher, personalizar ou customizar.</p>
					</div>
				</div>
			</div>
			<div class="mine-options-main" style="box-sizing: border-box;">
				<div class="mine-options-content" style="box-sizing: border-box; margin-bottom: 50px; text-align: center;">
					<h2 class="mine-section-title no-padding" style="box-sizing: border-box; font-family: Montserrat, sans-serif; line-height: 1.5em; color: inherit; margin: 0px auto; font-size: 24px; -webkit-font-smoothing: antialiased; letter-spacing: 1px; padding: 0px;">
						N&atilde;o importa o tamanho do seu neg&oacute;cio,</h2>
					<h2 class="mine-section-title" style="box-sizing: border-box; font-family: Montserrat, sans-serif; line-height: 1.5em; color: inherit; margin: 0px auto; font-size: 24px; -webkit-font-smoothing: antialiased; letter-spacing: 1px; padding: 0px;">
						n&oacute;s estamos prontos para atender voc&ecirc;.</h2>
					<p class="mine-section-text" style="box-sizing: border-box; margin: 0px; -webkit-font-smoothing: antialiased; font-family: Montserrat, sans-serif; line-height: 1.5em; padding: 0px;">
						Comece hoje mesmo a construir seu sonho, inicie com nosso plano hoje mesmo<br style="box-sizing: border-box;" />
						e voc&ecirc; s&oacute; paga um pequeno valor mensal sem nada mais incluso!</p>
					<br style="box-sizing: border-box;" />
					@guest
					<a class="mine-btn mine-btn-outline-purple" href="https://api.whatsapp.com/send?phone=5511945040347&text=Encontrei%20o%20site%20da%20F%C3%A1cil10%2Ce%20tenho%20interesse%20em%20seus%20servi%C3%A7os." style="box-sizing: border-box; background-color: transparent; color: rgb(134, 0, 156); text-decoration-line: none; -webkit-font-smoothing: antialiased; font-family: Montserrat, sans-serif; padding: 14px 20px; border-radius: 5px; position: relative; line-height: 1; cursor: pointer; font-size: 14px; font-weight: 600; border: 1px solid rgb(134, 0, 156); text-transform: lowercase; display: inline-block; transition: color 0s ease 0s, border-color 0s ease 0s, all 0.3s ease 0s;">conhe&ccedil;a nossos planos.</a>
					@else
					<a class="mine-btn mine-btn-outline-purple" href="https://api.whatsapp.com/send?phone=5511945040347&text=Encontrei%20o%20site%20da%20F%C3%A1cil10%2Ce%20tenho%20interesse%20em%20seus%20servi%C3%A7os." style="box-sizing: border-box; background-color: transparent; color: rgb(134, 0, 156); text-decoration-line: none; -webkit-font-smoothing: antialiased; font-family: Montserrat, sans-serif; padding: 14px 20px; border-radius: 5px; position: relative; line-height: 1; cursor: pointer; font-size: 14px; font-weight: 600; border: 1px solid rgb(134, 0, 156); text-transform: lowercase; display: inline-block; transition: color 0s ease 0s, border-color 0s ease 0s, all 0.3s ease 0s;">conhe&ccedil;a nossos planos.</a>
					@endguest</div>
			</div>
		</div>
	</div>
</section>
@stop