@extends('layout')
 
@section('title', 'HOSPEDAGEM DE SITE')
 
@section('content')
<div class="container">
<center><h1>Hospedagem de Site</h1></center>
    </div>
<center>
<div class"container">
<div class="row" style="background-color:#aaa;">
  <div class="col-md-4"><b>Host Basic</b></div>
  <div class="col-md-4"><b>Host Master</b></div>
  <div class="col-md-4"><b>Host Super</b></div>
</div>
<div class="row" style="background-color:#fff;">
  <div class="col-md-4">Construtor de Site Gratuito</br>Armazenamento: 4 GB</br>
Memória: 128mb</br>
Tráfego:Ilimitado</div>
  <div class="col-md-4">Construtor de Site Gratuito</br>Armazenamento: 30 GB</br>
Memória: 256mb</br>
Tráfego: Ilimitado</div>
  <div class="col-md-4">Construtor de Site Gratuito</br>Armazenamento: 50 GB</br>
Memória: 512mb</br>
Tráfego:Ilimitado</div>
</div>
<hr>
<div class="row" style="background-color:#fff; color:#ff0000;">
  <div class="col-md-4">R$99,84/Ano</div>
  <div class="col-md-4">R$24,90/Mês</div>
  <div class="col-md-4">R$32,90/Mês</div>
</div>
<hr>
@guest
<div class="row" style="background-color:#fff;">
    <div class="col-md-4"><a href="https://link.pagar.me/lH1HJb4dp8"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-4"><a href="https://link.pagar.me/lBkfrWbNuaI"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-4"><a href="https://link.pagar.me/lS1lVZVuaI"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@else
<div class="row" style="background-color:#fff;">
  <div class="col-md-4"><a href="https://link.pagar.me/lH1HJb4dp8"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-4"><a href="https://link.pagar.me/lBkfrWbNuaI"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-4"><a href="https://link.pagar.me/lS1lVZVuaI"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@endguest
</div>
</center>
<hr>
<center>
<div class"container">
<div class="row" style="background-color:#aaa;">
  <div class="col-md-4"><b>Host Silver</b></div>
  <div class="col-md-4"><b>Host Gold</b></div>
  <div class="col-md-4"><b>Host Diamond</b></div>
</div>
<div class="row" style="background-color:#fff;">
    <div class="col-md-4">Domínio 1 Ano Grátis</br>Construtor de Site Gratuito</br>Armazenamento: 65 GB</br>
Memória: 768mb</br>
Tráfego:Ilimitado</div>
  <div class="col-md-4">Domínio 1 Ano Grátis</br>Construtor de Site Gratuito</br>Armazenamento: 80 GB</br>
Memória: 1GB</br>
Tráfego:Ilimitado</div>
  <div class="col-md-4">Domínio 1 Ano Grátis</br>Construtor de Site Gratuito</br>Armazenamento: 95 GB</br>
Memória: 1,2GB</br>
Tráfego:Ilimitado</div>
</div>
<hr>
<div class="row" style="background-color:#fff; color:#ff0000;">
    <div class="col-md-4">R$47,90/Mês</div>
  <div class="col-md-4">R$55,00/Mês</div>
  <div class="col-md-4">R$69,00/Mês</div>
</div>
<hr>
@guest
<div class="row" style="background-color:#fff;">
  <div class="col-md-4"><a href="https://link.pagar.me/lSkGiBZEdpI"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-4"><a href="https://link.pagar.me/lH1NvWVdaL"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-4"><a href="https://link.pagar.me/lrJxadWEda8"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@else
<div class="row" style="background-color:#fff;">
    <div class="col-md-4"><a href="https://link.pagar.me/lSkGiBZEdpI"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-4"><a href="https://link.pagar.me/lH1NvWVdaL"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-4"><a href="https://link.pagar.me/lrJxadWEda8"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@endguest
</div>
</center>
@stop