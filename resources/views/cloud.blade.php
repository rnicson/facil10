@extends('layout')
 
@section('title', 'CLOUD COMPUTING')
 
@section('content')
<div class="container">
<center><h1>Cloud Computing</h1></center>
    </div>
<center>
<div class"container">
<div class="row" style="background-color:#aaa;">
  <div class="col-md-3"><b>Cloud 1</b></div>
  <div class="col-md-3"><b>Cloud 2</b></div>
  <div class="col-md-3"><b>Cloud 3</b></div>
  <div class="col-md-3"><b>Cloud 4</b></div>
</div>
<div class="row" style="background-color:#fff;">
  <div class="col-md-3">Memória RAM	              1 GB</br>
Armazenamento	              40 GB</br>
Limite de transferência      1 TB</br>
Endereços de IP	               2 IPs
</div>
  <div class="col-md-3">Memória RAM	              2 GB</br>
Armazenamento	               80 GB</br>
Limite de transferência      2 TB</br>
Endereços de IP	               2 IPs</div>
  <div class="col-md-3">Memória RAM	              4 GB</br>
Armazenamento	               120 GB</br>
Limite de transferência      3 TB</br>
Endereços de IP	               2 IPs</div>
  <div class="col-md-3">Memória RAM	              8 GB</br>
Armazenamento	               200 GB</br>
Limite de transferência      3 TB</br>
Endereços de IP	               2 IPs</div>
</div>
<hr>
<div class="row" style="background-color:#fff; color:#ff0000;">
  <div class="col-md-3">R$51,99/Mês</div>
  <div class="col-md-3">R$103,99/Mês</div>
  <div class="col-md-3">R$206,99/Mês</div>
  <div class="col-md-3">R$413,99/Mês</div>
</div>
<hr>
@guest
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://link.pagar.me/lBJgzy4OaL"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-3"><a href="https://link.pagar.me/lBJzNkEuaU"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-3"><a href="https://link.pagar.me/lr1eP1EO6U"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-3"><a href="https://link.pagar.me/lSkWmty4daL"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@else
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://link.pagar.me/lBJgzy4OaL"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-3"><a href="https://link.pagar.me/lBJzNkEuaU"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-3"><a href="https://link.pagar.me/lr1eP1EO6U"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-3"><a href="https://link.pagar.me/lSkWmty4daL"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>

@endguest
</div>
</center>
@stop