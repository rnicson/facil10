@extends('layout')
 
@section('title', 'INFRAESTRUTURA')
 
@section('content')
<div style="box-sizing: border-box; font-size: 16px; font-family: lato, sans-serif; color: rgb(60, 60, 60); line-height: 16px; padding: 0px 0px 20px; margin: 0px; width: 1263px; border-bottom: 1px solid rgb(153, 153, 153);">
	<div class="wrapper center" style="box-sizing: border-box; line-height: 16px; padding: 0px; margin: 0px auto; text-align: center; max-width: 100%; width: 970px;">
		<span class="fs30 lh32 fw200" style="box-sizing: border-box; font-size: 30px; color: inherit; line-height: 34px; padding: 0px; margin: 0px; text-align: inherit;">F&aacute;cil 10: especialistas na conce&ccedil;&atilde;o e gest&atilde;o de datacenters</span></div>
</div>
<div style="box-sizing: border-box; font-size: 16px; font-family: lato, sans-serif; color: rgb(60, 60, 60); line-height: 16px; padding: 0px; margin: 0px; width: 1263px;">
	&nbsp;</div>
<p>
	<a name="datacentre" style="box-sizing: border-box; color: rgb(0, 104, 177); text-decoration-line: underline; font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 14px; line-height: 20px; padding: 0px; margin: 0px; cursor: pointer; outline: 0px; border-width: 0px; border-style: initial; border-color: initial;"></a></p>
<div class="wrapper" style="box-sizing: border-box; color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 16px; line-height: 16px; padding: 0px; margin: 0px auto; width: 970px; max-width: 100%;">
	<p class="titleLight" style="box-sizing: border-box; margin: 0px; font-size: 24px; color: rgb(255, 255, 255); line-height: 20px; padding: 10px 30px; display: table-cell; background: none 0px 0px repeat scroll rgb(68, 68, 68); width: auto;">
		Os nossos datacenters</p>
	<p style="box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem;">
		&nbsp;</p>
	<p style="box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem;">
		<span style="box-sizing: border-box;">Trabalhamos com datacenters no Brasil e Estados Unidos com estrutura de ponta nos sistemas operacionais Linux e Windows</span></p>
	<p style="box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem;">
		<span style="box-sizing: border-box; font-weight: bolder; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;">E tamb&eacute;m solu&ccedil;&otilde;es em Datacenters como:&nbsp;</span><br style="box-sizing: border-box;" />
		<span style="box-sizing: border-box; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;">&bull;&nbsp;Colocation,</span><br style="box-sizing: border-box; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;" />
		<span style="box-sizing: border-box; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;">&bull;&nbsp;Colocation Gerenciado,&nbsp;</span><br style="box-sizing: border-box; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;" />
		<span style="box-sizing: border-box; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;">&bull;&nbsp;Servidores Dedicados&nbsp;</span><br style="box-sizing: border-box; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;" />
		<span style="box-sizing: border-box; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;">&bull;&nbsp;Cloud Computing.</span><br style="box-sizing: border-box; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;" />
		<br style="box-sizing: border-box; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;" />
		<span style="box-sizing: border-box; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;">Com experi&ecirc;ncia profissional e cont&iacute;nuo aprimoramento t&eacute;cnico, o corpo de analistas esta a disposi&ccedil;&atilde;o para implementar solu&ccedil;&otilde;es e resolver problemas com alta efici&ecirc;ncia e qualidade.</span><br style="box-sizing: border-box; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;" />
		<br style="box-sizing: border-box; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;" />
		<span style="box-sizing: border-box; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;">Conhe&ccedil;a nossa &aacute;rea de clientes e nossos depoimentos recebidos que foram resultados de um esfor&ccedil;o cont&iacute;nuo de toda nossa equipe!</span><br style="box-sizing: border-box; line-height: 14px; padding: 0px; margin: 0px; clear: both;" />
		&nbsp;</p>
	<div style="box-sizing: border-box; line-height: 16px; padding: 0px; margin: 0px; width: 970px;">
		<p class="legend italic center lgrey" qtlid="1171366" style="box-sizing: border-box; margin: 9.6875px 0px 0px; font-size: 14px; color: rgb(153, 153, 153); line-height: 20px; padding: 0px; text-align: center; font-style: italic;">
			Rede Mundial de Datacenters</p>
	</div>
</div>
<div class="wrapper" style="box-sizing: border-box; color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 16px; line-height: 16px; padding: 0px; margin: 0px auto; width: 970px; max-width: 100%;">
	<p class="titleLight" style="box-sizing: border-box; margin: 0px; font-size: 24px; color: rgb(255, 255, 255); line-height: 20px; padding: 10px 30px; display: table-cell; background: none 0px 0px repeat scroll rgb(68, 68, 68); width: auto;">
		M&aacute;xima seguran&ccedil;a</p>
	<br style="box-sizing: border-box; line-height: 14px; padding: 0px; margin: 0px; clear: both;" />
	<br style="box-sizing: border-box; line-height: 14px; padding: 0px; margin: 0px; clear: both;" />
	<div style="box-sizing: border-box; line-height: 16px; padding: 0px; margin: 0px; display: table; width: 970px;">
		<div class="table-cell half padded" style="box-sizing: border-box; line-height: 16px; padding: 19.3906px; margin: 0px; display: table-cell; width: 475px; vertical-align: middle; text-align: center;">
			<ul style="box-sizing: border-box; margin: 0px 0px 5px 10px; line-height: 16px; padding-right: 0px; padding-left: 0px;">
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					A F&aacute;cil 10 &eacute; a &uacute;nica entidade respons&aacute;vel pelos seus datacenters. S&oacute; os trabalhadores acreditados podem aceder &agrave;s instala&ccedil;&otilde;es e aos servidores;</li>
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					Acesso controlado por cart&otilde;es de identifica&ccedil;&atilde;o RFID, v&iacute;deo-vigil&acirc;ncia e equipas de seguran&ccedil;a onsite 24x7;</li>
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					Salas equipadas com sistemas de dete&ccedil;&atilde;o de fumos;</li>
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					Pessoal t&eacute;cnico presente 24x7<a class="btn fw600" href="https://www.ovh.pt/aproposito/seguranca.xml" qtlid_title="816385" style="box-sizing: border-box; color: rgb(255, 255, 255); text-decoration-line: none; background-color: transparent; display: inline-block; text-align: inherit; white-space: nowrap; vertical-align: middle; user-select: none; border: 0px; padding: 10px; font-size: 14px; line-height: 30px; border-radius: 7px; transition: box-shadow 0.35s ease-in-out 0s, all 0s ease-in-out 0s, all 0s ease-in-out 0s, all 0s ease-in-out 0s; cursor: pointer; margin: 0px; outline: 0px; height: 30px; box-shadow: rgba(0, 0, 0, 0) 0px 0px 0px 0px inset;" title="Saber mais sobre a segurança OVH"><span class="bold white" qtlid="816385" style="box-sizing: border-box; line-height: 30px; padding: 0px; margin: 0px; text-align: inherit; display: inline !important;">&ccedil;a&nbsp;</span></a></li>
			</ul>
		</div>
		<div class="table-cell half padded" style="box-sizing: border-box; line-height: 16px; padding: 19.3906px; margin: 0px; display: table-cell; width: 475px; vertical-align: middle; text-align: center;">
			<a href="https://www.ovh.pt/images/about-us/zones-securisees.jpg" rel="lightbox[gallery]" style="box-sizing: border-box; color: rgb(0, 104, 177); text-decoration-line: none; background-color: transparent; font-size: 14px; line-height: 20px; padding: 0px; margin: 0px; cursor: pointer; outline: 0px; border: 0px;" title=""><img src="https://www.ovh.pt/images/about-us/zones-securisees.jpg" style="box-sizing: border-box; vertical-align: middle; border: 0px; font-size: 12px; color: rgb(60, 60, 60); line-height: 16px; padding: 0px; margin: 0px; max-width: 100%; height: auto; width: 436.219px;" /></a></div>
	</div>
</div>
<p>
	<br style="box-sizing: border-box; color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 16px; line-height: 14px; padding: 0px; margin: 0px; clear: both;" />
	<span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 16px;">&nbsp;&nbsp;</span><a name="infra" style="box-sizing: border-box; color: rgb(0, 104, 177); text-decoration-line: underline; font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 14px; line-height: 20px; padding: 0px; margin: 0px; cursor: pointer; outline: 0px; border-width: 0px; border-style: initial; border-color: initial;"></a></p>
<div class="wrapper" style="box-sizing: border-box; color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 16px; line-height: 16px; padding: 0px; margin: 0px auto; width: 970px; max-width: 100%;">
	<p class="titleLight" style="box-sizing: border-box; margin: 0px; font-size: 24px; color: rgb(255, 255, 255); line-height: 20px; padding: 10px 30px; display: table-cell; background: none 0px 0px repeat scroll rgb(68, 68, 68); width: auto;">
		Infraestruturas de alta disponibilidade</p>
	<br style="box-sizing: border-box; line-height: 14px; padding: 0px; margin: 0px; clear: both;" />
	<br style="box-sizing: border-box; line-height: 14px; padding: 0px; margin: 0px; clear: both;" />
	<div style="box-sizing: border-box; line-height: 16px; padding: 0px; margin: 0px; display: table; width: 970px;">
		<div class="table-cell half padded" style="box-sizing: border-box; line-height: 16px; padding: 19.3906px; margin: 0px; display: table-cell; width: 475px; vertical-align: middle; text-align: center;">
			<a href="https://www.ovh.pt/images/about-us/infra-haute-dispo.jpg" rel="lightbox[gallery]" style="box-sizing: border-box; color: rgb(0, 104, 177); text-decoration-line: none; background-color: transparent; font-size: 14px; line-height: 20px; padding: 0px; margin: 0px; cursor: pointer; outline: 0px; border: 0px;" title=""><img src="https://www.ovh.pt/images/about-us/infra-haute-dispo.jpg" style="box-sizing: border-box; vertical-align: middle; border: 0px; font-size: 12px; color: rgb(60, 60, 60); line-height: 16px; padding: 0px; margin: 0px; max-width: 100%; height: auto; width: 436.219px;" /></a></div>
		<div class="table-cell half padded" style="box-sizing: border-box; line-height: 16px; padding: 19.3906px; margin: 0px; display: table-cell; width: 475px; vertical-align: middle; text-align: center;">
			<ul style="box-sizing: border-box; margin: 0px 0px 5px 10px; line-height: 16px; padding-right: 0px; padding-left: 0px;">
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					Dupla alimenta&ccedil;&atilde;o el&eacute;trica em todas as instala&ccedil;&otilde;es;</li>
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					Onduladores de&nbsp;<span class="fs16 lh18 fw400" qtlid="816450" style="box-sizing: border-box; color: inherit; line-height: 20px; padding: 0px; margin: 0px; text-align: inherit;">250KVA</span>&nbsp;cada;</li>
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					Geradores com autonomia de&nbsp;<span class="fs16 lh18 fw400" qtlid="816489" style="box-sizing: border-box; color: inherit; line-height: 20px; padding: 0px; margin: 0px; text-align: inherit;">48h</span>&nbsp;;</li>
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					<span class="fs16 lh18 fw400" qtlid="816502" style="box-sizing: border-box; color: inherit; line-height: 20px; padding: 0px; margin: 0px; text-align: inherit;">datacenters com 2 liga&ccedil;&otilde;es &agrave; rede el&eacute;trica (no m&iacute;nimo)</span>&nbsp;; no interior,&nbsp;<span class="fs16 lh18 fw400" qtlid="816528" style="box-sizing: border-box; color: inherit; line-height: 20px; padding: 0px; margin: 0px; text-align: inherit;">2 salas &quot;g&eacute;meas&quot; de roteamento&nbsp;</span>para garantir um funcionamento constante</li>
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					Capacidades avan&ccedil;adas de rede: liga&ccedil;&atilde;o superior a 10Gb e 40GB na rede principal.</li>
			</ul>
		</div>
	</div>
</div>
<p>
	<br style="box-sizing: border-box; color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 16px; line-height: 14px; padding: 0px; margin: 0px; clear: both;" />
	<span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 16px;">&nbsp;&nbsp;</span><a name="exigences" style="box-sizing: border-box; color: rgb(0, 104, 177); text-decoration-line: underline; font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 14px; line-height: 20px; padding: 0px; margin: 0px; cursor: pointer; outline: 0px; border-width: 0px; border-style: initial; border-color: initial;"></a></p>
<div class="wrapper" style="box-sizing: border-box; color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 16px; line-height: 16px; padding: 0px; margin: 0px auto; width: 970px; max-width: 100%;">
	<p class="titleLight" style="box-sizing: border-box; margin: 0px; font-size: 24px; color: rgb(255, 255, 255); line-height: 20px; padding: 10px 30px; display: table-cell; background: none 0px 0px repeat scroll rgb(68, 68, 68); width: auto;">
		Em conformidade com a exig&ecirc;ncias internacionais</p>
	<br style="box-sizing: border-box; line-height: 14px; padding: 0px; margin: 0px; clear: both;" />
	<br style="box-sizing: border-box; line-height: 14px; padding: 0px; margin: 0px; clear: both;" />
	<div style="box-sizing: border-box; line-height: 16px; padding: 0px; margin: 0px; display: table; width: 970px;">
		<div class="table-cell half padded" style="box-sizing: border-box; line-height: 16px; padding: 19.3906px; margin: 0px; display: table-cell; width: 475px; vertical-align: middle; text-align: center;">
			<ul style="box-sizing: border-box; margin: 0px 0px 5px 10px; line-height: 16px; padding-right: 0px; padding-left: 0px;">
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					A F&Aacute;CIL10&nbsp;tem a certifica&ccedil;&atilde;o&nbsp;<span class="fs16 lh18 fw400" qtlid="684092" style="box-sizing: border-box; color: inherit; line-height: 20px; padding: 0px; margin: 0px; text-align: inherit;">ISO 27001</span>:<span class="fs16 lh18 fw400" qtlid="816554" style="box-sizing: border-box; color: inherit; line-height: 20px; padding: 0px; margin: 0px; text-align: inherit;">2005</span>&nbsp;para o fornecimento e explora&ccedil;&atilde;o de infraestruturas dedicadas de Cloud Computing;</li>
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					A F&Aacute;CIL10&nbsp;respeita as normas&nbsp;<span class="fs16 lh18 fw400" qtlid="816580" style="box-sizing: border-box; color: inherit; line-height: 20px; padding: 0px; margin: 0px; text-align: inherit;">ISO 27002</span>&nbsp;e&nbsp;<span class="fs16 lh18 fw400" qtlid="816593" style="box-sizing: border-box; color: inherit; line-height: 20px; padding: 0px; margin: 0px; text-align: inherit;">ISO 27005&nbsp;</span>relacionadas com a gest&atilde;o da seguran&ccedil;a, a avalia&ccedil;&atilde;o de riscos e atividades relacionadas.</li>
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					<span class="fs16 lh18 fw400" qtlid="816606" style="box-sizing: border-box; color: inherit; line-height: 20px; padding: 0px; margin: 0px; text-align: inherit;">A F&Aacute;CIL10&nbsp;recebeu as certifica&ccedil;&otilde;es SOC 1 e SOC 2 type II</span>para 3 datacenters em Fran&ccedil;a, e 1 no Canad&aacute;, prova evidente do n&iacute;vel de seguran&ccedil;a da solu&ccedil;&atilde;o Private Cloud.</li>
			</ul>
			<br style="box-sizing: border-box; line-height: 14px; padding: 0px; margin: 0px; clear: both;" />
			&nbsp;</div>
		<div class="table-cell half padded" style="box-sizing: border-box; line-height: 16px; padding: 19.3906px; margin: 0px; display: table-cell; width: 475px; vertical-align: middle; text-align: center;">
			<img src="https://www.ovh.pt/images/about-us/aicpa.png" style="box-sizing: border-box; vertical-align: middle; border: 0px; line-height: 16px; padding: 0px; margin: 0px; max-width: 100%; height: auto; width: 436.219px;" /></div>
	</div>
</div>
<p>
	<br style="box-sizing: border-box; color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 16px; line-height: 14px; padding: 0px; margin: 0px; clear: both;" />
	<span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 16px;">&nbsp;&nbsp;</span><a name="performances-ecologiques" style="box-sizing: border-box; color: rgb(0, 104, 177); text-decoration-line: underline; font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 14px; line-height: 20px; padding: 0px; margin: 0px; cursor: pointer; outline: 0px; border-width: 0px; border-style: initial; border-color: initial;"></a></p>
<div class="wrapper" style="box-sizing: border-box; color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 16px; line-height: 16px; padding: 0px; margin: 0px auto; width: 970px; max-width: 100%;">
	<p class="titleLight" style="box-sizing: border-box; margin: 0px; font-size: 24px; color: rgb(255, 255, 255); line-height: 20px; padding: 10px 30px; display: table-cell; background: none 0px 0px repeat scroll rgb(68, 68, 68); width: auto;">
		Valores record em termos de efici&ecirc;ncia energ&eacute;tica</p>
	<br style="box-sizing: border-box; line-height: 14px; padding: 0px; margin: 0px; clear: both;" />
	<br style="box-sizing: border-box; line-height: 14px; padding: 0px; margin: 0px; clear: both;" />
	<div style="box-sizing: border-box; line-height: 16px; padding: 0px; margin: 0px; display: table; width: 970px;">
		<div class="table-cell half padded" style="box-sizing: border-box; line-height: 16px; padding: 19.3906px; margin: 0px; display: table-cell; width: 475px; vertical-align: middle; text-align: center;">
			<p class="fs16 fw200 lh20 justify" style="box-sizing: border-box; margin: 0px; color: rgb(73, 73, 73); line-height: 20px; padding: 0px; text-align: justify;">
				&nbsp;</p>
			<div class="Fleft half" style="box-sizing: border-box; line-height: 16px; padding: 0px; margin: 0px; float: left; width: 213.734px;">
				<a href="https://www.ovh.pt/images/about-us/watercooling.jpg" rel="lightbox[gallery]" style="box-sizing: border-box; color: rgb(0, 104, 177); text-decoration-line: none; background-color: transparent; font-size: 14px; line-height: 20px; padding: 0px; margin: 0px; cursor: pointer; outline: 0px; border: 0px;"><img alt="" src="https://www.ovh.pt/images/about-us/watercooling.jpg" style="box-sizing: border-box; vertical-align: middle; border: 0px; font-size: 12px; color: rgb(60, 60, 60); line-height: 16px; padding: 0px; margin: 0px; max-width: 100%; height: auto; width: 213.734px;" /></a></div>
			<div class="Fright half" style="box-sizing: border-box; line-height: 16px; padding: 0px; margin: 0px; float: right; width: 213.734px;">
				<a href="https://www.ovh.pt/images/about-us/roubaix-4.jpg" rel="lightbox[gallery]" style="box-sizing: border-box; color: rgb(0, 104, 177); text-decoration-line: none; background-color: transparent; font-size: 14px; line-height: 20px; padding: 0px; margin: 0px; cursor: pointer; outline: 0px; border: 0px;"><img alt="" src="https://www.ovh.pt/images/about-us/roubaix-4.jpg" style="box-sizing: border-box; vertical-align: middle; border: 0px; font-size: 12px; color: rgb(60, 60, 60); line-height: 16px; padding: 0px; margin: 0px; max-width: 100%; height: auto; width: 213.734px;" /></a></div>
			<br style="box-sizing: border-box; line-height: 14px; padding: 0px; margin: 0px; clear: both;" />
			<br style="box-sizing: border-box; line-height: 14px; padding: 0px; margin: 0px; clear: both;" />
			<div style="box-sizing: border-box; line-height: 16px; padding: 0px; margin: 0px; width: 436.219px;" translate="none">
				<a href="https://www.ovh.pt/images/solutions/datacenters/evolution.jpg" rel="lightbox[gallery]" style="box-sizing: border-box; color: rgb(0, 104, 177); text-decoration-line: none; background-color: transparent; font-size: 14px; line-height: 20px; padding: 0px; margin: 0px; cursor: pointer; outline: 0px; border: 0px;"><img src="https://www.ovh.pt/images/solutions/datacenters/evolution.jpg" style="box-sizing: border-box; vertical-align: middle; border: 0px; font-size: 12px; color: rgb(60, 60, 60); line-height: 16px; padding: 0px; margin: 0px; max-width: 100%; height: auto; width: 436.219px;" /></a></div>
			<p style="box-sizing: border-box; margin: 0px; font-size: 14px; color: rgb(73, 73, 73); line-height: 20px; padding: 0px;">
				&nbsp;</p>
		</div>
		<div class="table-cell half padded" style="box-sizing: border-box; line-height: 16px; padding: 19.3906px; margin: 0px; display: table-cell; width: 475px; vertical-align: middle; text-align: center;">
			<ul style="box-sizing: border-box; margin: 0px 0px 5px 10px; line-height: 16px; padding-right: 0px; padding-left: 0px;">
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					<span class="fs16 lh18 fw400" qtlid="690450" style="box-sizing: border-box; color: inherit; line-height: 20px; padding: 0px; margin: 0px; text-align: inherit;">98%</span>&nbsp;das salas de alojamento da F&Aacute;CIL10&nbsp;tem sistemas de ar condicionado;</li>
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					O&nbsp;<span class="fs16 lh18 fw400" qtlid="117153" style="box-sizing: border-box; color: inherit; line-height: 20px; padding: 0px; margin: 0px; text-align: inherit;">Watercooling</span>&nbsp;permite dissipar&nbsp;<span class="fs16 lh18 fw400" qtlid="816697" style="box-sizing: border-box; color: inherit; line-height: 20px; padding: 0px; margin: 0px; text-align: inherit;">70%</span>&nbsp;do calor emitido pelo processadores;</li>
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					O&nbsp;<span class="fs16 lh18 fw400" qtlid="689110" style="box-sizing: border-box; color: inherit; line-height: 20px; padding: 0px; margin: 0px; text-align: inherit;">Aircooling</span>&nbsp;evacua os restantes 30%</li>
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					Custos com energia&nbsp;<span class="fs16 lh18 fw400" qtlid="690515" style="box-sizing: border-box; color: inherit; line-height: 20px; padding: 0px; margin: 0px; text-align: inherit;">divididos por 2</span>&nbsp;;</li>
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					<span class="fs16 lh18 fw400" qtlid="1171418" style="box-sizing: border-box; color: inherit; line-height: 20px; padding: 0px; margin: 0px; text-align: inherit;">Power Usage Efficiency (PUE) inferior a 1,2</span>&nbsp;: redu&ccedil;&atilde;o constante do consumo energ&eacute;tico nos datacenters.</li>
			</ul>
		</div>
	</div>
</div>
<p>
	<br style="box-sizing: border-box; color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 16px; line-height: 14px; padding: 0px; margin: 0px; clear: both;" />
	<span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 16px;">&nbsp;&nbsp;</span><a name="innovation" style="box-sizing: border-box; color: rgb(0, 104, 177); text-decoration-line: underline; font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 14px; line-height: 20px; padding: 0px; margin: 0px; cursor: pointer; outline: 0px; border-width: 0px; border-style: initial; border-color: initial;"></a></p>
<div class="wrapper" style="box-sizing: border-box; color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 16px; line-height: 16px; padding: 0px; margin: 0px auto; width: 970px; max-width: 100%;">
	<p class="titleLight" style="box-sizing: border-box; margin: 0px; font-size: 24px; color: rgb(255, 255, 255); line-height: 20px; padding: 10px 30px; display: table-cell; background: none 0px 0px repeat scroll rgb(68, 68, 68); width: auto;">
		R&amp;D e inova&ccedil;&atilde;o constantes</p>
	<br style="box-sizing: border-box; line-height: 14px; padding: 0px; margin: 0px; clear: both;" />
	<br style="box-sizing: border-box; line-height: 14px; padding: 0px; margin: 0px; clear: both;" />
	<div style="box-sizing: border-box; line-height: 16px; padding: 0px; margin: 0px; display: table; width: 970px;">
		<div class="table-cell half padded" style="box-sizing: border-box; line-height: 16px; padding: 19.3906px; margin: 0px; display: table-cell; width: 475px; vertical-align: middle; text-align: center;">
			<ul style="box-sizing: border-box; margin: 0px 0px 5px 10px; line-height: 16px; padding-right: 0px; padding-left: 0px;">
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					A F&Aacute;CIL10&nbsp;concebe e constr&oacute;i os seus pr&oacute;prios centros de dados;</li>
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					R&amp;D constante para simplificar a instala&ccedil;&atilde;o e a manuten&ccedil;&atilde;o dos servidores, e permitir interven&ccedil;&otilde;es r&aacute;pidas: cabos imediatamente acess&iacute;veis, racks criados &agrave; medida</li>
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					<span class="fs16 lh18 fw400" qtlid="816801" style="box-sizing: border-box; color: inherit; line-height: 20px; padding: 0px; margin: 0px; text-align: inherit;">Inova&ccedil;&atilde;o permanente</span>&nbsp;especialmente no que diz respeito ao melhoramento das t&eacute;cnicas de arrefecimento e ventila&ccedil;&atilde;o: conce&ccedil;&atilde;o de implementa&ccedil;&atilde;o de sistemas de&nbsp;<span class="fs16 lh18 fw400" qtlid="816845" style="box-sizing: border-box; color: inherit; line-height: 20px; padding: 0px; margin: 0px; text-align: inherit;">Watercooling e Aircooling</span>&nbsp;propriet&aacute;rios;</li>
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					Equipamentos topo de gama renovados com regularidade.</li>
			</ul>
		</div>
		<div class="table-cell half padded" style="box-sizing: border-box; line-height: 16px; padding: 19.3906px; margin: 0px; display: table-cell; width: 475px; vertical-align: middle; text-align: center;">
			<a href="https://www.ovh.pt/images/about-us/conception.jpg" rel="lightbox[gallery]" style="box-sizing: border-box; color: rgb(0, 104, 177); text-decoration-line: none; background-color: transparent; font-size: 14px; line-height: 20px; padding: 0px; margin: 0px; cursor: pointer; outline: 0px; border: 0px;" title=""><img src="https://www.sonda.com/media/upload/noticias/fotos/02.jpg.1200x4000_q80.jpg" style="box-sizing: border-box; vertical-align: middle; border: 0px; font-size: 12px; color: rgb(60, 60, 60); line-height: 16px; padding: 0px; margin: 0px; max-width: 100%; width: 1200px; height: 522px;" /></a></div>
	</div>
</div>
<p>
	<br style="box-sizing: border-box; color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 16px; line-height: 14px; padding: 0px; margin: 0px; clear: both;" />
	<span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 16px;">&nbsp;&nbsp;</span><a name="localisation" style="box-sizing: border-box; color: rgb(0, 104, 177); text-decoration-line: underline; font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 14px; line-height: 20px; padding: 0px; margin: 0px; cursor: pointer; outline: 0px; border-width: 0px; border-style: initial; border-color: initial;"></a></p>
<div class="wrapper" style="box-sizing: border-box; color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 16px; line-height: 16px; padding: 0px; margin: 0px auto; width: 970px; max-width: 100%;">
	<p class="titleLight" style="box-sizing: border-box; margin: 0px; font-size: 24px; color: rgb(255, 255, 255); line-height: 20px; padding: 10px 30px; display: table-cell; background: none 0px 0px repeat scroll rgb(68, 68, 68); width: auto;">
		Escolha estrat&eacute;gica da localiza&ccedil;&atilde;o</p>
	<br style="box-sizing: border-box; line-height: 14px; padding: 0px; margin: 0px; clear: both;" />
	<br style="box-sizing: border-box; line-height: 14px; padding: 0px; margin: 0px; clear: both;" />
	<div style="box-sizing: border-box; line-height: 16px; padding: 0px; margin: 0px; display: table; width: 970px;">
		<div style="box-sizing: border-box; line-height: 16px; padding: 0px; margin: 0px; width: 970px;">
			<ul style="box-sizing: border-box; margin: 9.6875px 0px 5px; line-height: 16px; padding-right: 0px; padding-left: 0px;">
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 19.3906px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					A F&Aacute;CIL10 disp&otilde;e de datacenters que com servi&ccedil;os equivalentes, separados por 200km de dist&acirc;ncia com para assegurar a redund&acirc;ncia e continuidade de servi&ccedil;o;</li>
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 19.3906px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					Possibilidade de&nbsp;<span class="fs16 lh18 fw400" qtlid="676447" style="box-sizing: border-box; color: inherit; line-height: 20px; padding: 0px; margin: 0px; text-align: inherit;">PRA / PCA</span></li>
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 19.3906px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					Os datacenters F&Aacute;CIL10&nbsp;fora dos Estados Unidos n&atilde;o est&atilde;o abrangidos pelo Patriot Act.</li>
			</ul>
		</div>
	</div>
</div>
<p>
	&nbsp;</p>
	@stop
