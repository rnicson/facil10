@extends('layout')
 
@section('title', 'QUEM SOMOS')
 
@section('content')
<div class="container">
<h2 style="box-sizing: border-box; border: 0px; font-family: Catamaran, sans-serif; font-size: 20px; font-weight: 500; margin: 10px 0px; outline: 0px; padding: 20px 0px; vertical-align: baseline; clear: both; color: rgb(38, 46, 69);">
	A F&aacute;cil10 &eacute; uma empresa de Tecnologia que&nbsp; ajuda seus clientes&nbsp;a alcan&ccedil;ar a alta performance com facilidade.</h2>
<h2 style="box-sizing: border-box; border: 0px; font-family: Catamaran, sans-serif; font-size: 20px; font-weight: 500; margin: 10px 0px; outline: 0px; padding: 20px 0px; vertical-align: baseline; clear: both; color: rgb(38, 46, 69);">
	&nbsp;</h2>
<div class="company-services" style="box-sizing: border-box; border: 0px; font-family: Catamaran, sans-serif; font-size: 16px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; display: flex; flex-wrap: wrap; justify-content: center; color: rgb(38, 46, 69);">
	<div class="consultoria" style="box-sizing: border-box; border: 0px; font-family: inherit; font-style: inherit; font-weight: inherit; margin: 10px 0px; outline: 0px; padding: 0px 5px; vertical-align: baseline; text-align: center; width: 324px; min-width: 300px;">
		<img alt="consultoria em gestão empresarial" class="alignnone" height="150" src="https://www.euax.com.br/wp-content/themes/novoeuax/imgs/somos1.png" style="box-sizing: border-box; max-width: 100%; border: 0px; height: 150px; border-radius: 50%; width: 150px; transition: all 0.5s ease 0s;" width="150" /><br style="box-sizing: border-box;" />
		<strong style="box-sizing: border-box; border: 0px; font-family: inherit; font-size: 1.8rem; font-style: inherit; margin: 0px; outline: 0px; padding: 10px 0px; vertical-align: baseline; color: rgb(32, 32, 32); display: inline-block;">Consultoria</strong><br style="box-sizing: border-box;" />
		Conduzimos sua equipe &agrave; conquista de resultados!</div>
	<div class="cursos-in-company" style="box-sizing: border-box; border: 0px; font-family: inherit; font-style: inherit; font-weight: inherit; margin: 10px 0px; outline: 0px; padding: 0px 5px; vertical-align: baseline; text-align: center; width: 324px; min-width: 300px;">
		<img alt="outsourcing em gestão empresarial" class="alignnone" height="150" src="https://www.euax.com.br/wp-content/themes/novoeuax/imgs/somos2.png" style="box-sizing: border-box; max-width: 100%; border: 0px; height: 150px; border-radius: 50%; width: 150px; transition: all 0.5s ease 0s;" width="150" /><br style="box-sizing: border-box;" />
		<strong style="box-sizing: border-box; border: 0px; font-family: inherit; font-size: 1.8rem; font-style: inherit; margin: 0px; outline: 0px; padding: 10px 0px; vertical-align: baseline; color: rgb(32, 32, 32); display: inline-block;">Outsourcing</strong><br style="box-sizing: border-box;" />
		Aloca&ccedil;&atilde;o de profissionais especializados e de alta maturidade para fazer a performance acontecer!</div>
	<div class="treinamentos-vivenciais" style="box-sizing: border-box; border: 0px; font-family: inherit; font-style: inherit; font-weight: inherit; margin: 10px 0px; outline: 0px; padding: 0px 5px; vertical-align: baseline; text-align: center; width: 324px; min-width: 300px;">
		<img alt="capacitação corporativa" class="alignnone" height="150" src="https://www.euax.com.br/wp-content/themes/novoeuax/imgs/somos3.png" style="box-sizing: border-box; max-width: 100%; border: 0px; height: 150px; border-radius: 50%; width: 150px; transition: all 0.5s ease 0s;" width="150" /><br style="box-sizing: border-box;" />
		<strong style="box-sizing: border-box; border: 0px; font-family: inherit; font-size: 1.8rem; font-style: inherit; margin: 0px; outline: 0px; padding: 10px 0px; vertical-align: baseline; color: rgb(32, 32, 32); display: inline-block;">Capacita&ccedil;&atilde;o</strong><br style="box-sizing: border-box;" />
		Oferecemos um suporte t&eacute;cnico de acordo com sua necessidade em T.I</div>
</div>
<h3 style="box-sizing: border-box; border: 0px; font-family: Merriweather, sans-serif; font-size: 22px; font-style: italic; margin: 20px 0px; outline: 0px; padding: 40px; vertical-align: baseline; clear: both; text-align: center; background-color: rgb(241, 243, 245); line-height: 3rem; color: rgb(38, 46, 69);">
	Somos obstinados em elevar a Facilidade para<br style="box-sizing: border-box;" />
	nossos clientes!</h3>
<p style="box-sizing: border-box; border: 0px; font-family: Catamaran, sans-serif; font-size: 16px; margin: 0px 0px 1.5em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(38, 46, 69);">
	Nos orgulhamos em possuir um time de profissionais com atitude e conhecimento necess&aacute;rio para garantir resultados de forma &aacute;gil e assertiva.</p>
<p style="box-sizing: border-box; border: 0px; font-family: Catamaran, sans-serif; font-size: 16px; margin: 0px 0px 1.5em; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(38, 46, 69);">
	<a href="" rel="noopener" style="box-sizing: border-box; border: 0px; font-family: inherit; font-style: inherit; font-weight: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(72, 212, 174); background-color: transparent;" target="_blank"><img alt="consultoria empresarial" class="aligncenter wp-image-19360 size-full" height="200" sizes="(max-width: 800px) 100vw, 800px" src="http://www.euax.com.br/wp-content/uploads/2018/04/consultoria-empresarial.jpg" srcset="https://www.euax.com.br/wp-content/uploads/2018/04/consultoria-empresarial.jpg 800w, https://www.euax.com.br/wp-content/uploads/2018/04/consultoria-empresarial-300x75.jpg 300w, https://www.euax.com.br/wp-content/uploads/2018/04/consultoria-empresarial-768x192.jpg 768w" style="box-sizing: border-box; max-width: 100%; border: 0px; height: auto; clear: both; display: block; margin-left: auto; margin-right: auto;" title="consultoria empresarial" width="800" /></a></p>
<div class="company-services" style="box-sizing: border-box; border: 0px; font-family: Catamaran, sans-serif; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; display: flex; flex-wrap: wrap; justify-content: center; color: rgb(38, 46, 69);">
	&nbsp;</div>
<p>
	&nbsp;</p>
	<centeR><H1>Transparência</H1></centeR>
<div class="text-center margin-bottom-60" style="box-sizing: border-box; text-align: center; color: rgb(85, 85, 85); font-family: Arial, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; margin-bottom: 60px !important;">
	<h1 style="box-sizing: border-box; font-size: 32px; margin: 17px 0px 8.5px; font-weight: 300; line-height: 1.1; color: inherit; font-family: &quot;Source Sans Pro&quot;, sans-serif !important;">
		Por que valorizamos a transpar&ecirc;ncia com nossos Clientes?</h1>
</div>
<div class="row list text-center" style="box-sizing: border-box; text-align: center; margin-left: -10px; margin-right: -10px; color: rgb(85, 85, 85); font-family: Arial, &quot;Helvetica Neue&quot;, Helvetica, sans-serif;">
	<div class="col-md-4 item" style="box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 389.984px;">
		<h2 style="box-sizing: border-box; font-weight: 300; line-height: 1.1; color: inherit; font-family: &quot;Source Sans Pro&quot;, sans-serif !important; margin-top: 40px; margin-bottom: 28px; font-size: 24px;">
			Para termos um atendimento cada&nbsp;<br style="box-sizing: border-box;" />
			vez mais pr&oacute;ximo</h2>
		<span class="txt-main" style="box-sizing: border-box; font-size: 15px !important; line-height: 24px;">Acreditamos que um relacionamento transparente e de confian&ccedil;a &eacute; muito mais valioso que qualquer jogadinha de Marketing.</span></div>
	<div class="col-md-4 item" style="box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 389.984px;">
		<h2 style="box-sizing: border-box; font-weight: 300; line-height: 1.1; color: inherit; font-family: &quot;Source Sans Pro&quot;, sans-serif !important; margin-top: 40px; margin-bottom: 28px; font-size: 24px;">
			Pois queremos trazer o melhor&nbsp;<br style="box-sizing: border-box;" />
			servi&ccedil;o pelo pre&ccedil;o mais justo</h2>
		<span class="txt-main" style="box-sizing: border-box; font-size: 15px !important; line-height: 24px;">Nos movemos constantemente para te oferecer produtos atualizados e de alto n&iacute;vel. Sem perder o foco em ter pre&ccedil;os justos e competitivos.</span></div>
	<div class="col-md-4 item" style="box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 389.984px;">
		<h2 style="box-sizing: border-box; font-weight: 300; line-height: 1.1; color: inherit; font-family: &quot;Source Sans Pro&quot;, sans-serif !important; margin-top: 40px; margin-bottom: 28px; font-size: 24px;">
			Pois incentivamos o desenvolvimento<br style="box-sizing: border-box;" />
			da internet brasileira</h2>
		<span class="txt-main" style="box-sizing: border-box; font-size: 15px !important; line-height: 24px;">Sonhamos com as possibilidades de cada neg&oacute;cio online que surge. E para que nossa internet fique cada dia mais forte focamos em cuidar de cada cliente.</span></div>
</div>
<p>
	&nbsp;</p>

</div>
@stop