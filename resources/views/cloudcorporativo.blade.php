@extends('layout')
 
@section('title', 'INFRAESTRUTURA PESSOAL')
 
@section('content')
<div class="container">
<center><h3>Chegou a hora de migrar o ambiente de TI da sua Empresa para a Nuvem?</h3></center>
<div class="row">
   
    </div>
    </div>
    <hr>
    <div class="container">
    <div class="row">
    <div class="col" style="max-width: 48% !important;margin-top:10%">
    Mais do que processamento,podemos levar seu sistema para a nuvem! 
    <center><a href="contato"><button type="button" class="btn btn-dark">Quero um Orçamento</button></a>
    <a href="https://api.whatsapp.com/send?phone=5511945040347&text=Encontrei%20o%20site%20da%20F%C3%A1cil10%2Ce%20tenho%20interesse%20em%20seus%20servi%C3%A7os." target="_Blank"><button type="button" class="btn btn-success">Tirar Dúvidas pelo Whatsapp</button></a></center>
    </div>
    <div class="col" style="max-width: 48% !important;">
    <img src="img/infra-nuvem.jpg" style="max-width: 100% !important;">
    </div>

  </div>
</div>
<hr>
<div class="row">
<div class="col-md-3">
<center><b>MAPEAMENTO</b></center>
<hr>
Mapeamos os equipamentos, aplicações e infraestrutura (física e lógica) de um modo geral. Assim, compreendemos a capacidade e a criticidade de cada componente de seu sistema.
</div>
<div class="col-md-3">
<center><b>AVALIAÇÃO</b></center>
<hr>
Avaliamos qual melhor formato de nuvem para a empresa, com os custos do projeto dentro do orçamento disponível, orientando o planejamento desde o começo até os resultados.
</div>
<div class="col-md-3">
<center><b>ESTRATÉGIAS</b></center>
<hr>
Entendemos o impacto que a nuvem pode causar nas estratégias de gestão e segurança da empresa, pois alguns métodos que se aplicam em sistemas locais nem sempre funcionam na nuvem.
</div>
<div class="col-md-3">
<center><b>HOMOLOGAÇÃO</b></center>
<hr>
Configuramos um banco de dados para testar as funções e opções específicas da implantação. Dessa maneira, saberemos se a estratégia se adequa de acordo com o perfil e a demanda da infraestrutura.
</div>
</div>
<br>
<hr>
<br>
<div class="row" style="background-color:#0000FF;color:#ffff;">
<div class="col-md-12">
<center><h5>QUAIS AS VANTAGENS QUE AS EMPRESAS GANHAM AO</h5>
<h3>MIGRAREM PARA A NUVEM DA PENSO?</h3></center>
</div>
<br><br><br><br><br>
<div class="col-md-4"><center><b>Redução de Custos</b><br>Corte nos investimentos relacionados a infraestrutura e armazenamento, além de economia com energia elétrica e gerenciamento da estrutura.</center></div>
<div class="col-md-4"><center><b>Segurança</b><br>Nuvem hospedada em data center com rigorosos padrões de segurança física, além de soluções para segurança completa da informação, como UTM e criptografia.</center></div>
<div class="col-md-4"><center><b>Mobilidade</b><br>Acesso aos sistemas e arquivos da sua empresa a qualquer hora e lugar, sem investimento em servidores físicos ou outros equipamentos.</center></div>
</div>
<a href="contato"><button type="button" class="btn btn-warning btn-block">Quero colocar minha TI na Nuvem!</button></a>
@stop