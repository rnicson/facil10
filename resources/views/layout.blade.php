<!DOCTYPE HTML>
	<html>
	<head>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	    @if(Request::segment(1) == 'noticia')
           <title>@yield('title')</title>
           <meta name="description" content="@yield('descricao')" />
           <meta property="og:image" content="@yield('imagem')">
           <meta name="keywords" content="fácil10,Infraestrutura,desenvolvimento,web,criação,artes Gráficas,hospedagem,cloud computing,servidor dedicado,servidor de hospedagem,desenvolvimento de sistemas,desenvolvimento de site,manutenção de computadores,notebooks,tecnologia,empresa de tecnologia">
<meta name="robots" content="index, follow">
<meta name="revisit-after" content="1 day">
<meta name="language" content="Portuguese">
<meta name="generator" content="N/A">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        @else
	    	<title>Fácil10 - @yield('title')</title>
	    	<meta name="description" content="Cansado de sofrer com problemas na internet, e-mail, softwares de escritório, lentidões ou perda de informações?

Deixe a Fácil10 cuidar da sua TI e tenha a segurança para o seu negócio crescer ainda mais." />
            <meta name="keywords" content="fácil10,Infraestrutura,desenvolvimento,web,criação,artes Gráficas,hospedagem,cloud computing,servidor dedicado,servidor de hospedagem,desenvolvimento de sistemas,desenvolvimento de site,manutenção de computadores,notebooks,tecnologia,empresa de tecnologia">
<meta name="robots" content="index, follow">
<meta name="revisit-after" content="1 day">
<meta name="language" content="Portuguese">
<meta name="generator" content="N/A">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <meta property="og:image" content="https://facil10.com.br/img/logo.png">
	    @endif
		<link rel="shortcut icon" href="<?php echo asset('/img/favicon.png')?>" type="image/x-icon">
		<link rel="stylesheet" href="<?php echo asset('lib/bootstrap/css/bootstrap.min.css')?>" type="text/css">
		<link rel="stylesheet" href="<?php echo asset('lib/owl/dist/assets/owl.carousel.min.css')?>" type="text/css">
		<link rel="stylesheet" href="<?php echo asset('lib/owl/dist/assets/owl.theme.default.min.css')?>" type="text/css">
		<link rel="stylesheet" href="<?php echo asset('lib/owl/dist/assets/owl.carousel.min.css')?>" type="text/css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo asset('css/estilo.css')?>" type="text/css">
		<link rel="stylesheet" href="<?php echo asset('css/estilo-mobile.css')?>" type="text/css">
		<link rel="stylesheet" href="<?php echo asset('css/component.css')?>" type="text/css">
<script>
function funcao1()
{
alert("Você precisa contratar um serviço!");
}
</script>

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-7558467240309417",
    enable_page_level_ads: true
  });
</script>
	</head>
    	<body>
	  @guest
		<header class="clearfix">
		            <!--Menu-->
		    
		    	<div class="column">
					<div id="dl-menu" class="dl-menuwrapper">
						<button class="dl-trigger">Menu</button>
						<ul class="dl-menu">
							<li>
								<a href="https://facil10.com.br">Home</a></li>
							
							
							<li>
								<a href="#">Empresa</a>
								<ul class="dl-submenu">
									<li><a href="https://facil10.com.br/quemsomos">Quem Somos</a></li>
                                    <li><a href="https://facil10.com.br/infraestrutura">Infraestrutura</a></li>
								</ul>
							</li>
							
							<li>
								<a href="#">Desenvolvimento</a>
								<ul class="dl-submenu">
								<li><a href="https://facil10.com.br/sitemobi">Site</a></li>
								<li><a href="https://facil10.com.br/eccomerce">Loja Virtual</a></li>
                                <li><a href="https://facil10.com.br/artemobi">Arte Gráfica</a></li>
                                <li><a href="https://facil10.com.br/sistemas">Sistemas</a></li>
								</ul>
							</li>
							<li>
								<a href="#">Servidores</a>
								<ul class="dl-submenu">
									<li><a href="https://facil10.com.br/dedicadom">Dedicado</a></li>
									<li><a href="https://facil10.com.br/cloudm">Cloud</a></li>
									<li><a href="https://facil10.com.br/revendam">Revenda</a></li>
									<li><a href="https://facil10.com.br/hospedagemm">Hospedagem</a></li>
								</ul>
							</li>
							<li>
								<a href="#">Marketing</a>
								<ul class="dl-submenu">
									<li><a href="https://facil10.com.br/seo">SEO</a></li>
									<li><a href="https://facil10.com.br/emailm">Email Marketing</a></li>
									<li><a href="https://facil10.com.br/smsm">SMS Marketing</a></li>
								</ul>
							</li>
							<li>
								<a href="#">Infraestrutura</a>
								<ul class="dl-submenu">
                                    <li><a href="https://facil10.com.br/suporteti">Suporte de TI</a></li>
                                    <li><a href="https://facil10.com.br/cloudcorporativo">Cloud Corporativo</a></li>
									<li><a href="https://facil10.com.br/segurancainformacao">Segurança da Informação</a></li>
								</ul>
							</li>
								<li><a href="#">Restrito</a><ul class="dl-submenu">
                                <li><a href="http://facil10.com.br/login">Entrar na Conta</a></li>
                                <li><a href="http://facil10.com.br/register">Registrar</a></li>
								</ul></li>
						</ul>
					</div><!-- /dl-menuwrapper -->
				</div>
		   


		
		<!--Menu-->

			<div class="container container-logo">
				<a href="https://facil10.com.br"><img id="logotipo" src="/img/logo.png" alt="Logotipo"></a>
			</div>

			<div class="header-roxo">
				
				<div class="container">
				   
		    
				<nav id="menuHeader">
					<ul class="menu clearfix">
						<li>EMPRESA<!-- submenu -->
            <ul class="sub-menu clearfix">
                <li><a href="https://facil10.com.br/quemsomos">Quem Somos</a></li>
                <li><a href="https://facil10.com.br/infra-desk">Infraestrutura</a></li>
            </ul><!-- submenu --></li>
						<li>DESENVOLVIMENTO<!-- submenu -->
            <ul class="sub-menu clearfix">
                <li><a href="https://facil10.com.br/site">Site</a></li><br>
                <li><a href="https://facil10.com.br/eccomerce">Loja Virtual</a></li><br>
                <li><a href="https://facil10.com.br/artegrafica">Arte Gráfica</a></li><br>
                <li><a href="https://facil10.com.br/sistemas">Sistemas</a></li>
            </ul><!-- submenu --></li>
						<li>SERVIDORES<!-- submenu -->
            <ul class="sub-menu clearfix">
                <li><a href="https://facil10.com.br/dedicado">Dedicado</a></li><br>
                <li><a href="https://facil10.com.br/cloud">Cloud</a></li><br>
                <li><a href="https://facil10.com.br/revenda">Revenda</a></li><br>
                <li><a href="https://facil10.com.br/hospedagem">Hospedagem</a></li>
            </ul><!-- submenu --></li>
						<li>MARKETING<!-- submenu -->
            <ul class="sub-menu clearfix">
                <li><a href="https://facil10.com.br/seo">Seo</a></li><br>
                <li><a href="https://facil10.com.br/emailmarketing">Email Marketing</a></li>
				<li><a href="https://facil10.com.br/smsmarketing">SMS Marketing</a></li>
            </ul><!-- submenu --></li>
						<li>INFRAESTRUTURA<!-- submenu -->
            <ul class="sub-menu clearfix">
			<li><a href="https://facil10.com.br/suporteti">Suporte de TI</a></li>
                                    <li><a href="https://facil10.com.br/cloudcorporativo">Cloud Corporativo</a></li>
									<li><a href="https://facil10.com.br/segurancainformacao">Segurança da Informação</a></li>
            </ul><!-- submenu --></li>
            <li><a href="https://facil10.com.br/contato">CONTATO</a></li>
						<li><a href="#">RESTRITO</a><!-- submenu -->
            <ul class="sub-menu clearfix" style="z-index:10;">
                <li><a href="https://facil10.com.br/login">Login</a></li><br>
                <li><a href="https://facil10.com.br/register">Registrar</a></li>
            </ul><!-- submenu --></li>
					</ul>
</nav>
				</div>

			</div>	

			<div class="social">
			<div class="container">
				<ul class="float-right">
					<li><a href="https://www.facebook.com/facil10.com.br/" target="_Blank"><img src="/img/facebook.png"></a></li>
					<li><a href="https://api.whatsapp.com/send?phone=5511945040347&text=Encontrei%20o%20site%20da%20F%C3%A1cil10%2Ce%20tenho%20interesse%20em%20seus%20servi%C3%A7os." target="_Blank"><img src="/img/whatsapp.png"></a></li>
					<li><a href="https://www.linkedin.com/company/facil10/" target="_Blank"><img src="/img/linkedin.png"></a></li>
					<li><a href="https://www.instagram.com/facil10.com.br/" target="_Blank"><img src="/img/instagram.png"></a></li>
				</ul>
			</div>
		</div>
<hr>

		</header>
@else
		<header class="clearfix">
		            <!--Menu-->
		    
		    	<div class="column">
					<div id="dl-menu" class="dl-menuwrapper">
						<button class="dl-trigger">Menu</button>
						<ul class="dl-menu">
							<li>
								<a href="https://facil10.com.br">Home</a></li>
													<li>
								<a href="#">Desenvolvimento</a>
								<ul class="dl-submenu">
								<li><a href="https://facil10.com.br/sitemobi">Site</a></li>
								<li><a href="https://facil10.com.br/eccomerce">Loja Virtual</a></li>
                                <li><a href="https://facil10.com.br/artemobi">Arte Gráfica</a></li>
                                <li><a href="https://facil10.com.br/sistemas">Sistemas</a></li>
								</ul>
							</li>
							<li>
								<a href="#">Servidores</a>
								<ul class="dl-submenu">
									<li><a href="https://facil10.com.br/dedicadom">Dedicado</a></li>
									<li><a href="https://facil10.com.br/cloudm">Loud</a></li>
									<li><a href="https://facil10.com.br/revendam">Revenda</a></li>
									<li><a href="https://facil10.com.br/hospedagemm">Hospedagem</a></li>
								</ul>
							</li>
							<li>
								<a href="#">Marketing</a>
								<ul class="dl-submenu">
									<li><a href="https://facil10.com.br/seo">SEO</a></li>
									<li><a href="https://facil10.com.br/emailm">Email Marketing</a></li>
									<li><a href="https://facil10.com.br/smsm">SMS Marketing</a></li>
								</ul>
							</li>
							<li>
								<a href="#">Infraestrutura</a>
								<ul class="dl-submenu">
								<li><a href="https://facil10.com.br/suporteti">Suporte de TI</a></li>
                                    <li><a href="https://facil10.com.br/cloudcorporativo">Cloud Corporativo</a></li>
									<li><a href="https://facil10.com.br/segurancainformacao">Segurança da Informação</a></li>
								
								</ul>
							</li>
								
							<li>
								<a href="/dados">Meus Serviços</a>
									<ul class="dl-submenu">
									    <li><a href="https://facil10.com.br/servicos">Serviços</a></li>
                <li><a href="https://facil10.com.br/faturas">Faturas</a></li>
                <li><a href="https://facil10.com.br/creditos-conta">Créditos da Conta</a></li>
									</ul>
							</li>
							<li>
								<a href="/pagamentos">Suporte</a>
									<ul class="dl-submenu">
									    <li><a href="https://facil10.com.br/criar-chamados">Criar Chamado</a></li>
                <li><a href="https://facil10.com.br/chamados-abertos">Chamados Abertos</a></li>
                <li><a href="https://facil10.com.br/chamados-fechados">Chamados Fechados</a></li>
									</ul>
							</li>
							@if(Auth::id() == 1 ||Auth::id() == 3 ) 
							
							<li>
								<a href="/pagamentos">Admin</a>
									<ul class="dl-submenu">
									    <li><a href="News">Noticias</a></li>
                
									</ul>
							</li>
							@endif
							
							
							
														<li>
								<a href="/contato">Contato</a>
								
							</li>
					               <li><a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Sair
                                        </a> 

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form></li>
						</ul>
					</div><!-- /dl-menuwrapper -->
				</div>
		   


		
		<!--Menu-->

			<div class="container container-logo">
				<a href="https://facil10.com.br"><img id="logotipo" src="/img/logo.png" alt="Logotipo"></a>
			</div>

			<div class="header-roxo">
				
				<div class="container">
				   
		    
				<nav id="menuHeader">
					<ul class="menu clearfix">
						<li>Desenvolvimento<!-- submenu -->
            <ul class="sub-menu clearfix">
                <li><a href="https://facil10.com.br/site">Site</a></li><br>
                <li><a href="https://facil10.com.br/eccomerce">Loja Virtual</a></li><br>
                <li><a href="https://facil10.com.br/artegrafica">Arte Gráfica</a></li><br>
                <li><a href="https://facil10.com.br/sistemas">Sistemas</a></li>
            </ul><!-- submenu --></li>
						<li>Servidores<!-- submenu -->
            <ul class="sub-menu clearfix">
                <li><a href="https://facil10.com.br/dedicado">Dedicado</a></li><br>
                <li><a href="https://facil10.com.br/cloud">Cloud</a></li><br>
                <li><a href="https://facil10.com.br/revenda">Revenda</a></li><br>
                <li><a href="https://facil10.com.br/hospedagem">Hospedagem</a></li>
            </ul><!-- submenu --></li>
						<li>Marketing<!-- submenu -->
            <ul class="sub-menu clearfix">
                <li><a href="https://facil10.com.br/seo">Seo</a></li><br>
                <li><a href="https://facil10.com.br/emailmarketing">Email Marketing</a></li>
				<li><a href="https://facil10.com.br/smsmarketing">SMS Marketing</a></li>
            </ul><!-- submenu --></li>
						<li>Infraestrutura<!-- submenu -->
            <ul class="sub-menu clearfix">
			<li><a href="https://facil10.com.br/suporteti">Suporte de TI</a></li>
                                    <li><a href="https://facil10.com.br/cloudcorporativo">Cloud Corporativo</a></li>
									<li><a href="https://facil10.com.br/segurancainformacao">Segurança da Informação</a></li>
            </ul><!-- submenu --></li>
				        <li><a href="#">Meus Serviços</a>
				        <ul class="sub-menu clearfix">
                <li><a href="https://facil10.com.br/servicos">Serviços</a></li>
                <li><a href="https://facil10.com.br/faturas">Faturas</a></li>
                <li><a href="https://facil10.com.br/creditos-conta">Créditos</a></li>
            </ul>
				        </li>
						<li><a href="#">Suporte</a>
						<ul class="sub-menu clearfix">
                <li><a href="https://facil10.com.br/criar-chamados">Criar Chamado</a></li>
                <li><a href="https://facil10.com.br/chamados-abertos">Chamados Abertos</a></li>
                <li><a href="https://facil10.com.br/chamados-fechados">Chamados Fechados</a></li>
            </ul>
						</li>
						@if(Auth::id() == 1 ||Auth::id() == 3 ) 
						
						<li><a href="#">Admin</a>
						<ul class="sub-menu clearfix">
                <li><a href="News">Noticias</a></li>
				
                
            </ul>
						</li>
						
				@endif
						<li><a href="https://facil10.com.br/contato">Contato</a></li>
					               <li><a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Sair
                                        </a> 

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form></li>
					</ul>
</nav>
				</div>

			</div>	

			<div class="social">
			<div class="container">
				<ul class="float-right">
					<li><a href="https://www.facebook.com/facil10.com.br/"><img src="/img/facebook.png"></a></li>
					<li><a href="https://api.whatsapp.com/send?phone=5511945040347&text=Encontrei%20o%20site%20da%20F%C3%A1cil10%2Ce%20tenho%20interesse%20em%20seus%20servi%C3%A7os." target="_Blank"><img src="/img/whatsapp.png"></a></li>
					<li><a href="https://www.linkedin.com/company/facil10/" target="_Blank"><img src="/img/linkedin.png"></a></li>
					<li><a href="https://www.instagram.com/facil10.com.br/" target="_Blank"><img src="/img/instagram.png"></a></li>
				</ul>
			</div>
		</div>
<hr>

		</header>
@endguest
		<section>
        @yield('content')
        </section>
    		<footer>
			
			<div class="row row-cinza-escuro">
				
				<div class="container copyright-mobile">
					
					<p>Copyright © Fácil10 2018.</p><p> Todos os Direitos Reservados.</p>

				</div>

				<div class="partner container">

				<ul>

					<li><img src="img/centos.jpg"></li>
					<li><img src="img/cisco.png"></li>
					<li><img src="img/cpanel.png"></li>
					<li><img src="img/vmware.jpg"></li>
					<li><img src="img/microsoft.png"></li>
					<li><img src="img/seguro.png"></li>

				</ul>

				</div>
			</div>

		</footer>
	<script type="text/javascript" src="<?php echo asset('lib/jquery/jquery.min.js')?>"></script>
		<script type="text/javascript" src="<?php echo asset('lib/bootstrap/js/bootstrap.min.js')?>"></script>
	<script type="text/javascript" src="<?php echo asset('lib/owl/dist/owl.carousel.min.js')?>"></script>
		<script>
        		    $(".thumbnails").owlCarousel({
            		    loop: true,
            		    margin: 10,
            		    responsive:{
            		        0 :{
            		            items:1
            		        },
            		        480 :{
            		            items:3
            		        },
            		        768 :{
            		            items:4
            		        },
            		        1200 :{
            		            items:6
            		        }
        		        }
        		    });
		        </script>
		<script src="<?php echo asset('js/modernizr.custom.js')?>"></script>
				<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script src="<?php echo asset('js/jquery.dlmenu.js')?>"></script>
				<script>
			$(function() {
				$( '#dl-menu' ).dlmenu();
			});
			</script>
<script src="//code.jivosite.com/widget/1WgrLtCERP" async></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91190701-10"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91190701-10');
</script>
 
	</body>
	</html>
</DOCTYPE>