@extends('layout')
 
@section('title', 'INFRAESTRUTURA EMPRESARIAL')
 
@section('content')
<div class="container">
<center><h1>Precisa de Suporte Técnico para Pessoa Física?</h1></center>
<div class="row">
    <div class="col-md-12">
        Conheça os serviços da Fácil10,trabalhamos diariamente solucionando os problemas de nossos clientes seja Pessoa Física ou Jurídica.
    </div>
    </div>
    </div>
    <hr>
    <div class="container">
  <div class="row">
    <div class="col">
      <img src="https://facil10.com.br/img/foto.jpg" width="200px">
    </div>
    <div class="col" style="margin-top:30px;">
       Em curto prazo de tempo após confirmação de pagamento do chamado um de nossos técnicos entrarão em contato para lhe atender.
    </div>
  </div>
</div>
<hr>
<center>
    <h2>Planos Remotos</h2>
<div class"container">
<div class="row" style="background-color:#aaa;">
  <div class="col-md-3"><b>1 Chamado</b></div>
  <div class="col-md-3"><b>5 Chamados</b></div>
  <div class="col-md-3"><b>10 Chamados</b></div>
  <div class="col-md-3"><b>30 Chamados</b></div>
</div>
<div class="row" style="background-color:#fff;">
  <div class="col-md-3">Email</div>
  <div class="col-md-3">Email</div>
  <div class="col-md-3">Email</div>
  <div class="col-md-3">Email</div>
</div>
<div class="row" style="background-color:#fff;">
  <div class="col-md-3">Office</div>
  <div class="col-md-3">Office</div>
  <div class="col-md-3">Office</div>
  <div class="col-md-3">Office</div>
</div>
<div class="row" style="background-color:#fff;">
  <div class="col-md-3">Programas</div>
  <div class="col-md-3">Programas</div>
  <div class="col-md-3">Programas</div>
  <div class="col-md-3">Programas</div>
</div>
<div class="row" style="background-color:#fff;">
  <div class="col-md-3">Configurações</div>
  <div class="col-md-3">Configurações</div>
  <div class="col-md-3">Configurações</div>
  <div class="col-md-3">Configurações</div>
</div>
<div class="row" style="background-color:#fff;">
  <div class="col-md-3">Vírus</div>
  <div class="col-md-3">Vírus</div>
  <div class="col-md-3">Vírus</div>
  <div class="col-md-3">Vírus</div>
</div>
<hr>
<div class="row" style="background-color:#fff; color:#ff0000;">
  <div class="col-md-3">R$25,00</div>
  <div class="col-md-3">R$110,00</div>
  <div class="col-md-3">R$200,00</div>
  <div class="col-md-3">R$550,00</div>
</div>
<hr>
@guest
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://facil10.com.br/login"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-3"><a href="https://facil10.com.br/login"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-3"><a href="https://facil10.com.br/login"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-3"><a href="https://facil10.com.br/login"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@else
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://www.moip.com.br/PagamentoMoIP.do?id_carteira={{auth()->user()->id_carteira}}&valor=2500&nome=1 Chamado Remoto Empresarial&pagador_email={{auth()->user()->email}}&pagador_nome={{auth()->user()->name}}"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-3"><a href="https://www.moip.com.br/PagamentoMoIP.do?id_carteira={{auth()->user()->id_carteira}}&valor=11000&nome=5 Chamado Remoto Empresarial&pagador_email={{auth()->user()->email}}&pagador_nome={{auth()->user()->name}}"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-3"><a href="https://www.moip.com.br/PagamentoMoIP.do?id_carteira={{auth()->user()->id_carteira}}&valor=20000&nome=10 Chamado Remoto Empresarial&pagador_email={{auth()->user()->email}}&pagador_nome={{auth()->user()->name}}"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-3"><a href="https://www.moip.com.br/PagamentoMoIP.do?id_carteira={{auth()->user()->id_carteira}}&valor=55000&nome=30 Chamado Remoto Empresarial&pagador_email={{auth()->user()->email}}&pagador_nome={{auth()->user()->name}}"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@endguest
</div>
</center>
<hr>
<center>
    <h2>Planos Presenciais</h2>
<div class"container">
<div class="row" style="background-color:#aaa;">
  <div class="col-md-3"><b>1 CHAMADO</b></div>
  <div class="col-md-3"><b>5 CHAMADOS</b></div>
  <div class="col-md-3"><b>10 CHAMADOS</b></div>
  <div class="col-md-3"><b>30 CHAMADOS</b></div>
</div>
<div class="row" style="background-color:#fff;">
  <div class="col-md-3">Sistema Operacional</div>
  <div class="col-md-3">Sistema Operacional</div>
  <div class="col-md-3">Sistema Operacional</div>
  <div class="col-md-3">Sistema Operacional</div>
</div>
<div class="row" style="background-color:#fff;">
  <div class="col-md-3">Hardware</div>
  <div class="col-md-3">Hardware</div>
  <div class="col-md-3">Hardware</div>
  <div class="col-md-3">Hardware</div>
</div>
<div class="row" style="background-color:#fff;">
  <div class="col-md-3">Software</div>
  <div class="col-md-3">Software</div>
  <div class="col-md-3">Software</div>
  <div class="col-md-3">Software</div>
</div>
<div class="row" style="background-color:#fff;">
  <div class="col-md-3">Periféricos</div>
  <div class="col-md-3">Periféricos</div>
  <div class="col-md-3">Periféricos</div>
  <div class="col-md-3">Periféricos</div>
</div>
<hr>
<div class="row" style="background-color:#fff; color:#ff0000;">
  <div class="col-md-3">R$290,00</div>
  <div class="col-md-3">R$1.300,00</div>
  <div class="col-md-3">R$2.000,00</div>
  <div class="col-md-3">R$5.000,00</div>
</div>
<hr>
@guest
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://facil10.com.br/login"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-3"><a href="https://facil10.com.br/login"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-3"><a href="https://facil10.com.br/login"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-3"><a href="https://facil10.com.br/login"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@else
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://www.moip.com.br/PagamentoMoIP.do?id_carteira={{auth()->user()->id_carteira}}&valor=29000&nome=1 Chamado Presencial Empresarial&pagador_email={{auth()->user()->email}}&pagador_nome={{auth()->user()->name}}"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-3"><a href="https://www.moip.com.br/PagamentoMoIP.do?id_carteira={{auth()->user()->id_carteira}}&valor=130000&nome=5 Chamado Presencial Empresarial&pagador_email={{auth()->user()->email}}&pagador_nome={{auth()->user()->name}}"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-3"><a href="https://www.moip.com.br/PagamentoMoIP.do?id_carteira={{auth()->user()->id_carteira}}&valor=200000&nome=10 Chamado Presencial Empresarial&pagador_email={{auth()->user()->email}}&pagador_nome={{auth()->user()->name}}"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-3"><a href="https://www.moip.com.br/PagamentoMoIP.do?id_carteira={{auth()->user()->id_carteira}}&valor=500000&nome=30 Chamado Presencial Empresarial&pagador_email={{auth()->user()->email}}&pagador_nome={{auth()->user()->name}}"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@endguest
</div>
</center>
@stop