@extends('layout')
 
@section('title', 'SERVIDOR DEDICADO')
 
@section('content')
<div class="container">
<center><h1>Servidor Dedicado</h1></center>
    </div>
<center>
<div class"container">
<div class="row" style="background-color:#aaa;">
  <div class="col-md-4"><b>Dedicado Simples</b></div>
</div>
<div class="row" style="background-color:#fff;">
  <div class="col-md-4">CPU:Intel Xeon E3-1230 V5</br>
RAM: 8GB DDR4 ECC</br>
Hard Drive:1TB SATA</br>
Bandwidth: 100TB</br>
Uplink Port Speed: 1000mbps</br>
Operating System: CentOS 7 - 64 Bit</br>
IP Addresses: 4 IPs (1 Usable)</br>
Network Attached Storage: 20gb NAS</br>
KVM Over IP: IPMI 2.0 with integrated KVM over IP</br>
Support Level: Unmanaged Support</div>
</div>
<hr>
<div class="row" style="background-color:#fff; color:#ff0000;">
  <div class="col-md-4">R$690,00/Mês</div>
</div>
<hr>
@guest
<div class="row" style="background-color:#fff;">
  <div class="col-md-4"><a href="https://link.pagar.me/lryewRmd6U"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@else
<div class="row" style="background-color:#fff;">
  <div class="col-md-4"><a href="https://link.pagar.me/lryewRmd6U"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@endguest

</br>

<div class="row" style="background-color:#aaa;">
  <div class="col-md-4"><b>Dedicado intermediario</b></div>
</div>
<div class="row" style="background-color:#fff;">
<div class="col-md-4">CPU: Quad Intel Xeon E5-4603</br>
RAM: 16GB DDR3 ECC</br>
Hard Drive: 500GB Enterprise SATA</br>
Bandwidth: 100TB</br>
Uplink Port Speed: 1000mbps</br>
Operating System: CentOS 7 - 64 Bit</br>
IP Addresses: 4 IPs (1 Usable)</br>
Network Attached Storage: 20gb NAS</br>
KVM Over IP: IPMI 2.0 with integrated KVM over IP</br>
Support Level: Unmanaged Support</div>
</div>
<hr>
<div class="row" style="background-color:#fff; color:#ff0000;">
  <div class="col-md-4">R$1.421,00/Mês</div>
</div>
<hr>
@guest
<div class="row" style="background-color:#fff;">
   <div class="col-md-4"><a href="https://link.pagar.me/lSJTt07OaU"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@else
<div class="row" style="background-color:#fff;">
  <div class="col-md-4"><a href="https://link.pagar.me/lSJTt07OaU"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@endguest

</br>

<div class="row" style="background-color:#aaa;">
  <div class="col-md-4"><b>Dedicado Avançado</b></div>
</div>
<div class="row" style="background-color:#fff;">
  <div class="col-md-4">CPU: Dual Intel Xeon E5-2620 V3</br>
RAM: 32GB DDR4 ECC</br>
Hard Drive: 1TB Enterprise SATA</br>
Bandwidth: 100TB</br>
Uplink Port Speed: 1000mbps</br>
Operating System: CentOS 7 - 64 Bit</br>
IP Addresses: 4 IPs (1 Usable)</br>
Network Attached Storage: 20gb NAS</br>
KVM Over IP: IPMI 2.0 with integrated KVM over IP</br>
Support Level: Unmanaged Support</div>
</div>
<hr>
<div class="row" style="background-color:#fff; color:#ff0000;">
    <div class="col-md-4">R$3.712,00/Mês</div>
</div>
<hr>
@guest
<div class="row" style="background-color:#fff;">
   <div class="col-md-4"><a href="https://link.pagar.me/lryshAXOpL"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@else
<div class="row" style="background-color:#fff;">
  <div class="col-md-4"><a href="https://link.pagar.me/lryshAXOpL"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@endguest

</div>
</center>
@stop