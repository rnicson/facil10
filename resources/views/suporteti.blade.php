@extends('layout')
 
@section('title', 'INFRAESTRUTURA PESSOAL')
 
@section('content')
<div class="container">
<center><h1>Conheça o Suporte de TI para empresas da Fácil10!</h1></center>
<div class="row">
    </div>
    </div>
    <hr>
    <div class="container">
  <div class="row">
    <div class="col" style="max-width: 48% !important;margin-top:10%">
    Pare de ter problemas com sua TI, contando com um time de técnicos remotos durante o seu home office ou alocados na sua empresa. 
    <center><a href="contato"><button type="button" class="btn btn-dark">Quero um Orçamento</button></a>
    <a href="https://api.whatsapp.com/send?phone=5511945040347&text=Encontrei%20o%20site%20da%20F%C3%A1cil10%2Ce%20tenho%20interesse%20em%20seus%20servi%C3%A7os." target="_Blank"><button type="button" class="btn btn-success">Tirar Dúvidas pelo Whatsapp</button></a></center>
    </div>
    <div class="col" style="max-width: 48% !important;">
    <img src="img/suporteti.jpg" style="max-width: 100% !important;">
    </div>

  </div>
</div>
<hr>
<div class="container">
<h5><center>NÃO REALIZAREMOS SOMENTE O SUPORTE DE TI, MAS</center></h5> 
<h2><center>GARANTIREMOS O FUNCIONAMENTO 100% DA SUA TI</center><h2>
<br>
<div class="row">
<div class="col-md-4" style="background-color: #d3d3d3;">
<b><h5>Equipe especialista</h5></b>
<h6>Um time avançado, com certificações em servidores de bancos de dados, virtualização e outras plataformas corporativas</h6>
</div>
<div class="col-md-5">
<b><h5>Documentação do ambiente de TI</h5></b>
<h6>Material que contém a descrição completa dos equipamentos, sistemas, acessos, senhas e telefones de fornecedores que compõem a estrutura de sua TI</h6>
</div>
<div class="col-md-3" style="background-color: #d3d3d3;">
<b><h5>Planejamento anual de TI</h5></b>
<h6>Apresentação de fácil leitura que assimila os pontos de risco e melhorias necessárias ao ambiente</h6>
</div>
</div>

<a href="contato"><button type="button" class="btn btn-warning btn-block">Quero conhecer o Suporte de TI da Fácil10</button></a>
</div>
@stop