@extends('layout')
 
@section('title')
 
@section('content')
<center><h1>Criar Chamado</h1></center>
<div class="form-group">
    <label for="exampleFormControlInput1">Departamento</label>
    <select class="form-control form-control-lg">
  <option>Selecione o Departamento</option>
  <option>Suporte Técnico</option>
  <option>Financeiro</option>
  <option>Vendas</option>
    </select>
  </div>
<div class="form-group">
    <label for="exampleFormControlInput1">Nível</label>
    <select class="form-control form-control-lg">
  <option>Selecione seu Nível de Conhecimento</option>
  <option>Eu não sou técnico. Por favor faça isso por mim.</option>
  <option>Estou aprendendo. Por favor,explique tudo com cuidado.</option>
  <option>Eu tenho um bom entendimento técnico</option>
  <option>Eu provavelmente sei mais do que vocês</option>
</select>
  </div>
<div class="form-group">
    <label for="exampleFormControlInput1">Serviço</label>
    <select class="form-control form-control-lg">
  <option>Selecione o Serviço</option>
</select>
  </div>
<div class="form-group">
    <label for="exampleFormControlInput1">Assunto</label>
    <input type="text" class="form-control form-control-lg" id="exampleFormControlInput1">
  </div>
<div class="form-group">
    <label for="exampleFormControlTextarea1">Descrição detalhada do problema</label>
    <textarea class="form-control form-control-lg" id="exampleFormControlTextarea1" rows="3"></textarea>
  </div>

<div class="form-group">
    <label for="exampleFormControlTextarea1">Passos para reproduzir o problema. Por favor, seja o mais descritivo possível
Qualquer usuários/senhas necessários para reproduzir o problema (endereço de e-mail, MySQL, aplicações de terceiros, etc)</label>
    <textarea class="form-control form-control-lg" id="exampleFormControlTextarea1" rows="3"></textarea>
  </div>
<div class="form-group">
    <label for="exampleFormControlTextarea1">Qualquer usuários/senhas necessários para reproduzir o problema (endereço de e-mail, MySQL, aplicações de terceiros, etc)</label>
    <textarea class="form-control form-control-lg" id="exampleFormControlTextarea1" rows="3"></textarea>
  </div>
  <div class="form-group">
    <label for="exampleFormControlFile1">Anexar um Arquivo</label>
    <input type="file" class="form-control-file form-control-lg" id="exampleFormControlFile1">
  </div>
  
  <button type="button" onclick="funcao1()" class="btn btn-primary btn-lg btn-block">Criar Chamado</button>
  
@stop