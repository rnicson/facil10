@extends('layout')
 
@section('title', 'EMAIL MARKETING')
 
@section('content')
<div class="container">
<center><h1>Email Marketing</h1></center>
    </div>
<center>
<div class"container">
<div class="row" style="background-color:#aaa;">
  <div class="col-md-3"><b>Envio 1</b></div>
  <div class="col-md-3"><b>Envio 2</b></div>
  <div class="col-md-3"><b>Envio 3</b></div>
  <div class="col-md-3"><b>Envio 4</b></div>
</div>
<div class="row" style="background-color:#fff;">
  <div class="col-md-3">Envios: 1 Mil</div>
  <div class="col-md-3">Envios: 5 Mil</div>
  <div class="col-md-3">Envios: 10 Mil</div>
  <div class="col-md-3">Envios: 30 Mil</div>
</div>
<hr>
<div class="row" style="background-color:#fff; color:#ff0000;">
  <div class="col-md-3">R$10,00/Única</div>
  <div class="col-md-3">R$19,99/Única</div>
  <div class="col-md-3">R$35,00/Única</div>
  <div class="col-md-3">R$69,99/Única</div>
</div>
<hr>
@guest
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://link.pagar.me/lB1UAWNdaL"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-3"><a href="https://link.pagar.me/lSyeMgfVOpI"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-3"><a href="https://link.pagar.me/lB1hWzNda8"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-3"><a href="https://link.pagar.me/lSklBXzNO6L"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@else
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://link.pagar.me/lB1UAWNdaL"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-3"><a href="https://link.pagar.me/lSyeMgfVOpI"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-3"><a href="https://link.pagar.me/lB1hWzNda8"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-3"><a href="https://link.pagar.me/lSklBXzNO6L"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@endguest
</div>
</center>
<hr>
<center>
<div class"container">
<div class="row" style="background-color:#aaa;">
  <div class="col-md-3"><b>Envio 5</b></div>
  <div class="col-md-3"><b>Envio 6</b></div>
</div>
<div class="row" style="background-color:#fff;">
  <div class="col-md-3">Envios: 60 Mil</div>
  <div class="col-md-3">Envios: 100 Mil</div>
</div>
<hr>
<div class="row" style="background-color:#fff; color:#ff0000;">
  <div class="col-md-3">R$119,99/Única</div>
  <div class="col-md-3">R$200,00/Única</div>
</div>
<hr>
@guest
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://link.pagar.me/lB1nHf4O6U"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-3"><a href="https://link.pagar.me/lBJlJOfVu6I"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@else
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://link.pagar.me/lB1nHf4O6U"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
  <div class="col-md-3"><a href="https://link.pagar.me/lBJlJOfVu6I"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@endguest
</div>
</center>
@stop