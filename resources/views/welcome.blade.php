@extends('layout')
 
@section('title', 'TECNOLOGIA GERANDO FACILIDADE')
 
@section('content')
<section>

			<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  			<div class="carousel-inner">
  			    	<div class="carousel-item active">
      			<a href="https://rendemais.net" target="_blank"><img class="d-block w-100" height="30%" src="img/trabalhe.png" alt="First slide"></a>
    		</div>
    		<div class="carousel-item">
      			<a href="suporteti"><img class="d-block w-100" height="30%" src="img/infra.png"></a>
    		</div>
    		<div class="carousel-item">
      			<a href="site"><img class="d-block w-100" height="30%" src="img/dev.png"></a>
    		</div>
    		<div class="carousel-item">
			<a href="suporteti"><img class="d-block w-100" height="30%" src="img/banner.png"></a>
    		</div>
  			</div>
  				<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
    			<span class="sr-only">Anterior</span>
  				</a>
  				<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    			<span class="carousel-control-next-icon" aria-hidden="true"></span>
    			<span class="sr-only">Próximo</span>
  				</a>
			</div>

			<div id="call-to-action">
				
				<div class="container">

					<div class="row text-center">
						<h2>A Fácil10 desenvolve soluções para corrigir problemas Tecnológicos</h2>
					</div>

					<div class="container">
						
						<p><b>A Fácil10 dispõe de modelos de plano que enquadram de acordo com o perfil do cliente, sendo eles: Pessoal ou Empresarial</b></p>

					</div>
					<div class="container">
						<p> Suporte Remoto e Presencial | Desenvolvimento de Sites | Servidores Dedicados | Hospedagem de Site | Servidor de Email | Email Marketing</p>
					</div>
					<div class="text-center">
						<div class="container row-max-400">
							
							<div class="col-xs-6">
								<a href="/quemsomos" class="btn btn-roxo">Conhecer</a>
							</div>
							<div class="col-xs-6">
								<a href="https://facil10.com.br/register" class="btn btn-amarelo">Cadastrar</a>
							</div>

						</div>
					</div>
				</div>

			</div>

			<div id="news" class="container">
				
				<div class="row-cinza-escuro">
					<hr>
					<h2>Notícias sobre Tecnologia</h2>
					<hr>
				</div>	
				<div class="row thumbnails owl-carousel owl-theme">
				    @foreach($noticia as $noticia)
				    <a href="https://facil10.com.br/noticia/{{$noticia->id}}/{{$noticia->slug}}">
					<div class="item">
						<div class="item-inner">
						<img src="/img/news/{{$noticia->imagem}}" alt="Noticia">
						<h3>{{$noticia->titulo}}</h3>
						<time>{{$noticia->data}}</time>
                        </div>
					</div>
					</a>
					@endforeach
				</div>

			</div>

		</section>
@stop