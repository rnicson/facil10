@extends('layout')
 
@section('title', 'CURSO - DESENVOLVEDOR WEB')
 
@section('content')
<br>
@guest
<a href="https://www.facil10.com.br/login" class="btn btn-outline-success btn-block">Quero Contratar</a>
@else
<a href="https://www.moip.com.br/PagamentoMoIP.do?id_carteira={{auth()->user()->id_carteira}}&valor=15999&nome=Curso Desenvolvedor Web&pagador_email={{auth()->user()->email}}&pagador_nome={{auth()->user()->name}}" class="btn btn-outline-success btn-block">Quero Contratar</a>
@endguest
<br>

<div class="box-cursos-interno como-funciona-o-curso-programador-web first" style="background-color: #ffffff;" data-url="como-funciona-o-curso-programador-web">
            <div class="container">
    <h2 class="titulo">
        COMO FUNCIONA<br class="hidden-xs" />
        <b>O CURSO</b>
    </h2>

    <p style="text-align: justify;">
        Garantir a interatividade e o dinamismo dos sites &eacute; essencial para fidelizar o p&uacute;blico on-line. Dominar algumas ferramentas &eacute; necess&aacute;rio para o profissional que deseja entrar neste mercado que s&oacute; cresce no Brasil. Diante deste cen&aacute;rio, a Microlins desenvolveu o curso de programador web para possibilitar a voc&ecirc; o conhecimento e o pleno dom&iacute;nio sobre o Flash, uma das principais ferramentas de intera&ccedil;&atilde;o multim&iacute;dia para web.
    </p>

    <p style="text-align: justify;">
        Com este curso, voc&ecirc; aprender&aacute; a desenvolver projetos com recursos multim&iacute;dia para diversas plataformas web. Voc&ecirc; receber&aacute; orienta&ccedil;&otilde;es sobre novas tend&ecirc;ncias em rela&ccedil;&atilde;o a estrutura dos sites, o que ir&aacute; estimular sua criatividade e o seu racioc&iacute;nio para criar projetos din&acirc;micos e interativos. Al&eacute;m disso, voc&ecirc; ter&aacute; o conhecimento necess&aacute;rio para programar utilizando linguagem Actionscript, que &eacute; a base do Flash. &nbsp;
    </p>

    <p style="text-align: justify;">
        O curso tem dura&ccedil;&atilde;o de dez meses e est&aacute; divido em quatro m&oacute;dulos:&nbsp;
    </p>

    <p>
        <span style="color:#003366;"><span style="font-size:14px;"><strong>L&oacute;gica e Algoritmo | AS &ndash; Flash Introdu&ccedil;&atilde;o | AS &ndash; Flash Aplica&ccedil;&atilde;o | AS &ndash; Flash Acesso a Banco de Dados</strong></span></span>
    </p>

    <p style="text-align: justify;">
        Ao final do curso, voc&ecirc; ter&aacute; as compet&ecirc;ncias necess&aacute;rias para desenvolver projetos em flash, tornando suas cria&ccedil;&otilde;es din&acirc;micas e atraentes ao p&uacute;blico, seja qual for a plataforma utilizada.
    </p>
</div>

        </div>
        <div class="box-cursos-interno mercado-de-trabalho-programador-web " style="background-color: #ffffff;" data-url="mercado-de-trabalho-programador-web">
            <div class="container">
    <h2 class="titulo">
        MERCADO DE<br class="hidden-xs" />
        <b>TRABALHO</b>
    </h2>

    <p style="text-align: justify;">
        A &aacute;rea de <strong>web </strong>&eacute; uma das que mais oferece oportunidades em termos de mercado de trabalho. Com este curso, voc&ecirc; estar&aacute; apto a trabalhar em empresas dos mais variados segmentos, atuar em ag&ecirc;ncias de comunica&ccedil;&atilde;o, etc. Al&eacute;m disso, voc&ecirc; poder&aacute; trabalhar como aut&ocirc;nomo, gerenciando seus projetos e seu pr&oacute;prio neg&oacute;cio.&nbsp;
    </p>

    <p style="text-align: justify;">
        Atualmente, um<strong> Programador Web </strong>possui uma renda m&eacute;dia de <strong>R$2.300,00</strong>. Estes ganhos aumentam na medida em que voc&ecirc; conquista novas qualifica&ccedil;&otilde;es e, consequentemente, novos cargos. Em pouco tempo de carreira, voc&ecirc; poder&aacute; se tornar um <strong>Analista Programador</strong>, com um ganho m&eacute;dio de <strong>R$3.500,00</strong>&nbsp;ou at&eacute; mesmo um <strong>Analista de Sistemas</strong>, com sal&aacute;rio m&eacute;dio de <strong>R$3.900,00</strong>.
    </p>

    <p style="text-align: justify;">
        A web oferece muitas oportunidades de cargos e ganhos elevados. Para conquistar o seu lugar de destaque no mercado, voc&ecirc; ter&aacute; que se dedicar e estar sempre atualizado com as novas tend&ecirc;ncias e ferramentas utilizadas na &aacute;rea. Com a <strong>Microlins </strong>voc&ecirc; ter&aacute; tudo isso, com a garantia de um ensino de qualidade e voltado para tornar voc&ecirc; um profissional qualificado.&nbsp;
    </p>

    <h2 class="titulo">
        EVOLU&Ccedil;&Atilde;O NA<br class="hidden-xs" />
        <b>CARREIRA</b>
    </h2>

    <div class="media">
        <div class="media-left media-middle">
            <img alt="" class="media-object" src="/galeria/repositorio/images/mercado_de_trabalho/estagio_design.jpg" title="" />
        </div>

        <div class="media-right media-body media-middle">
            <p>
                <strong>Programador Web</strong>
            </p>

            <p>
                R$2.300,00
            </p>
        </div>
    </div>

    <div class="media">
        <div class="media-left media-middle">
            <img alt="" class="media-object" src="/galeria/repositorio/images/mercado_de_trabalho/funcionario_design.jpg" title="" />
        </div>

        <div class="media-right media-body media-middle">
            <p>
                <strong>Analista Programador</strong>
            </p>

            <p>
                R$3.500,00
            </p>
        </div>
    </div>

    <div class="media">
        <div class="media-left media-middle">
            <img alt="" class="media-object" src="/galeria/repositorio/images/mercado_de_trabalho/coordenadora_design.jpg" title="" />
        </div>

        <div class="media-right media-body media-middle">
            <p>
                <strong>Analista Sistema</strong>
            </p>

            <p>
                R$3.900,00
            </p>
        </div>
    </div>

    <p style="margin-top: 20px;">
        <strong>FONTE:</strong> CATHO - M&Eacute;DIA SALARIAL BRASIL
    </p>
</div>

        </div>

@guest
<a href="https://www.facil10.com.br/login" class="btn btn-outline-success btn-block">Quero Contratar</a>
@else
<a href="https://www.moip.com.br/PagamentoMoIP.do?id_carteira={{auth()->user()->id_carteira}}&valor=15999&nome=Curso Desenvolvedor Web&pagador_email={{auth()->user()->email}}&pagador_nome={{auth()->user()->name}}" class="btn btn-outline-success btn-block">Quero Contratar</a>
@endguest

@stop