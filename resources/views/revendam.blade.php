@extends('layout')
 
@section('title', 'REVENDA DE HOSPEDAGEM')
 
@section('content')
<div class="container">
<center><h1>Revenda de Hospedagem</h1></center>
    </div>
<center>
<div class"container">
<div class="row" style="background-color:#aaa;">
  <div class="col-md-3"><b>Revenda 1</b></div>
</div>
<div class="row" style="background-color:#fff;">
   <div class="col-md-3">Armazenamento: 60 GB</br>
Tráfego:Ilimitado
</div>
</div>
<hr>
<div class="row" style="background-color:#fff; color:#ff0000;">
  <div class="col-md-3">R$60,00/Mês</div>
</div>
<hr>
@guest
<div class="row" style="background-color:#fff;">
  <div class="col-md-4"><a href="https://link.pagar.me/lBJkllVuTI"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@else
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://link.pagar.me/lBJkllVuTI"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@endguest

</br>

<div class="row" style="background-color:#aaa;">
  <div class="col-md-3"><b>Revenda 2</b></div>
</div>
<div class="row" style="background-color:#fff;">
<div class="col-md-3">Armazenamento: 70 GB</br>
Tráfego:Ilimitado</div>
</div>
<hr>
<div class="row" style="background-color:#fff; color:#ff0000;">
  <div class="col-md-3">R$70,00/Mês</div>
</div>
<hr>
@guest
<div class="row" style="background-color:#fff;">
   <div class="col-md-4"><a href="https://link.pagar.me/lHylfeEda8"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@else
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://link.pagar.me/lHylfeEda8"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@endguest

</br>

<div class="row" style="background-color:#aaa;">
  <div class="col-md-3"><b>Revenda 3</b></div>
</div>
<div class="row" style="background-color:#fff;">
  <div class="col-md-3">Armazenamento: 110 GB</br>
Tráfego:Ilimitado</div>
</div>
<hr>
<div class="row" style="background-color:#fff; color:#ff0000;">
    <div class="col-md-3">R$100,00/Mês</div>
</div>
<hr>
@guest
<div class="row" style="background-color:#fff;">
   <div class="col-md-4"><a href="https://link.pagar.me/lBJZquxNu6U"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@else
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://link.pagar.me/lBJZquxNu6U"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@endguest

</br>

<div class="row" style="background-color:#aaa;">
  <div class="col-md-3"><b>Revenda 4</b></div>
</div>
<div class="row" style="background-color:#fff;">
  <div class="col-md-3">Armazenamento: 190 GB</br>
Tráfego:Ilimitado</div>
</div>
<hr>
<div class="row" style="background-color:#fff; color:#ff0000;">
    <div class="col-md-3">R$150,00/Mês</div>
</div>
<hr>
@guest
<div class="row" style="background-color:#fff;">
   <div class="col-md-4"><a href="https://link.pagar.me/lryR5xEuaL"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@else
<div class="row" style="background-color:#fff;">
  <div class="col-md-3"><a href="https://link.pagar.me/lryR5xEuaL"><button type="button" class="btn btn-outline-success">Adquirir</button></a></div>
</div>
@endguest

</div>
</center>
@stop