@extends('layout')
 
@section('title', 'ARTE GRÁFICA')
 
@section('content')
	<h2 >
		A um pre&ccedil;o acess&iacute;vel &eacute; poss&iacute;vel desenvolver sua arte com os designers da Fácil10, especializados em cria&ccedil;&atilde;o de materias gr&aacute;ficos.</h2>
	<p>
		Se voc&ecirc; ainda n&atilde;o tem o layout para imprimir seus materiais gr&aacute;ficos, poder&aacute; contar com a equipe de cria&ccedil;&atilde;o da Fácil10, composta por designers dedicados &agrave; cria&ccedil;&atilde;o e ajustes de layouts para impress&atilde;o de produtos gr&aacute;ficos.</p>
			<h2>
			REGRAS PARA&nbsp;<span style="box-sizing: border-box; font-weight: 700;">CRIA&Ccedil;&Atilde;O DE ARTE</span></h2>
	</div>
	<p>
		&nbsp;<span>Os prazos para cria&ccedil;&atilde;o da arte</span>&nbsp;come&ccedil;am a contar a partir da aprova&ccedil;&atilde;o do briefing, enviado por voc&ecirc;, contendo todas as informa&ccedil;&otilde;es e materiais (como logotipo, fotos e conte&uacute;do) necess&aacute;rios para a cria&ccedil;&atilde;o.</p>
	<p>
		&nbsp;<span >O prazo de produ&ccedil;&atilde;o do material</span>&nbsp;criado pelos designers da Fácil10 come&ccedil;a a contar somente ap&oacute;s sua aprova&ccedil;&atilde;o da arte.</p>
	<p>
		&nbsp;<span>O pre&ccedil;o inclui 3 altera&ccedil;&otilde;es gratuitas na arte.</span>&nbsp;Caso voc&ecirc; deseje mais altera&ccedil;&otilde;es, ser&aacute; cobrado o valor de R$ 45,00 por altera&ccedil;&atilde;o solicitada.</p>
	<p>
		&nbsp;<span>Cria&ccedil;&atilde;o de Logotipo n&atilde;o incluso.</span>&nbsp;Os valores para cria&ccedil;&atilde;o das artes n&atilde;o est&atilde;o inclusos a cria&ccedil;&atilde;o de logo marcas, caso queira adquirir esse servi&ccedil;o envie um e-mail para nossa equipe solicitando a cria&ccedil;&atilde;o. O valor para desenvolvimento de Logotipo &eacute; de a partir de R$500,00.</p>
	<p>
		&nbsp;<span>O envio do briefing dever&aacute; ser feito atrav&eacute;s de sua conta no site.</span>&nbsp;Ap&oacute;s finalizarmos a cria&ccedil;&atilde;o da sua arte, enviaremos um e-mail informando que est&aacute; pronto, ent&atilde;o &eacute; necess&aacute;rio acessar sua conta no site e aprovar. Se necess&aacute;rio enviaremos um e-mail solicitando mais informa&ccedil;&otilde;es para a cria&ccedil;&atilde;o, por&eacute;m &eacute; necess&aacute;rio enviar os detalhes atrav&eacute;s de sua conta no site, n&atilde;o aceitaremos briefings enviados por e-mail.</p>
	<p>
		&nbsp;<span>Por gentileza, conferir a arte como um todo.</span>&nbsp;Textos contidos no material, conferir se n&atilde;o h&aacute; nenhum erro ortogr&aacute;fico, averiguar se est&aacute; tudo disposto conforme o solicitado. O material produzido ser&aacute; fiel ao arquivo aprovado pelo cliente. Ap&oacute;s aprova&ccedil;&atilde;o, o mesmo &eacute; enviado para produ&ccedil;&atilde;o. N&atilde;o nos responsabilizaremos por altera&ccedil;&otilde;es ou reparos ap&oacute;s aprova&ccedil;&atilde;o, por favor analise com cuidado.</p>
		<p>
		Conhe&ccedil;a nossos produtos e fa&ccedil;a agora mesmo seu pedido!</p>
@stop