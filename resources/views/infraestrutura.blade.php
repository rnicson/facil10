@extends('layout')
 
@section('title', 'INFRAESTRUTURA')
 
@section('content')
		<span class="fs30 lh32 fw200" style="box-sizing: border-box; font-size: 30px; color: inherit; line-height: 34px; padding: 0px; margin: 0px; text-align: inherit;">F&aacute;cil 10: especialistas na conce&ccedil;&atilde;o e gest&atilde;o de datacenters</span>
<p>
	<a name="datacentre" style="box-sizing: border-box; color: rgb(0, 104, 177); text-decoration-line: underline; font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 14px; line-height: 20px; padding: 0px; margin: 0px; cursor: pointer; outline: 0px; border-width: 0px; border-style: initial; border-color: initial;"></a></p>
	<p class="titleLight" style="box-sizing: border-box; margin: 0px; font-size: 24px; color: rgb(255, 255, 255); line-height: 20px; padding: 10px 30px; display: table-cell; background: none 0px 0px repeat scroll rgb(68, 68, 68); width: auto;">
		Os nossos datacenters</p>
	<p style="box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem;">
		&nbsp;</p>
	<p style="box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem;">
		<span style="box-sizing: border-box;">Trabalhamos com datacenters no Brasil e Estados Unidos com estrutura de ponta nos sistemas operacionais Linux e Windows</span></p>
	<p style="box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem;">
		<span style="box-sizing: border-box; font-weight: bolder; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;">E tamb&eacute;m solu&ccedil;&otilde;es em Datacenters como:&nbsp;</span><br style="box-sizing: border-box;" />
		<span style="box-sizing: border-box; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;">&bull;&nbsp;Colocation,</span><br style="box-sizing: border-box; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;" />
		<span style="box-sizing: border-box; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;">&bull;&nbsp;Colocation Gerenciado,&nbsp;</span><br style="box-sizing: border-box; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;" />
		<span style="box-sizing: border-box; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;">&bull;&nbsp;Servidores Dedicados&nbsp;</span><br style="box-sizing: border-box; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;" />
		<span style="box-sizing: border-box; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;">&bull;&nbsp;Cloud Computing.</span><br style="box-sizing: border-box; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;" />
		<br style="box-sizing: border-box; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;" />
		<span style="box-sizing: border-box; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;">Com experi&ecirc;ncia profissional e cont&iacute;nuo aprimoramento t&eacute;cnico, o corpo de analistas esta a disposi&ccedil;&atilde;o para implementar solu&ccedil;&otilde;es e resolver problemas com alta efici&ecirc;ncia e qualidade.</span><br style="box-sizing: border-box; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;" />
		<br style="box-sizing: border-box; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;" />
		<span style="box-sizing: border-box; color: rgb(102, 102, 102); font-family: Arial, Helvetica, sans-serif;">Conhe&ccedil;a nossa &aacute;rea de clientes e nossos depoimentos recebidos que foram resultados de um esfor&ccedil;o cont&iacute;nuo de toda nossa equipe!</span><br style="box-sizing: border-box; line-height: 14px; padding: 0px; margin: 0px; clear: both;" />
		&nbsp;</p>
		<p class="legend italic center lgrey" qtlid="1171366" style="box-sizing: border-box; margin: 9.6875px 0px 0px; font-size: 14px; color: rgb(153, 153, 153); line-height: 20px; padding: 0px; text-align: center; font-style: italic;">
			Rede Mundial de Datacenters</p>

	<p class="titleLight" style="box-sizing: border-box; margin: 0px; font-size: 24px; color: rgb(255, 255, 255); line-height: 20px; padding: 10px 30px; display: table-cell; background: none 0px 0px repeat scroll rgb(68, 68, 68); width: auto;">
		M&aacute;xima seguran&ccedil;a</p>
	<br style="box-sizing: border-box; line-height: 14px; padding: 0px; margin: 0px; clear: both;" />
	<br style="box-sizing: border-box; line-height: 14px; padding: 0px; margin: 0px; clear: both;" />
			<ul style="box-sizing: border-box; margin: 0px 0px 5px 10px; line-height: 16px; padding-right: 0px; padding-left: 0px;">
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					A F&aacute;cil 10 &eacute; a &uacute;nica entidade respons&aacute;vel pelos seus datacenters. S&oacute; os trabalhadores acreditados podem aceder &agrave;s instala&ccedil;&otilde;es e aos servidores;</li>
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					Acesso controlado por cart&otilde;es de identifica&ccedil;&atilde;o RFID, v&iacute;deo-vigil&acirc;ncia e equipas de seguran&ccedil;a onsite 24x7;</li>
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					Salas equipadas com sistemas de dete&ccedil;&atilde;o de fumos;</li>
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					Pessoal t&eacute;cnico presente 24x7<a class="btn fw600" href="https://www.ovh.pt/aproposito/seguranca.xml" qtlid_title="816385" style="box-sizing: border-box; color: rgb(255, 255, 255); text-decoration-line: none; background-color: transparent; display: inline-block; text-align: inherit; white-space: nowrap; vertical-align: middle; user-select: none; border: 0px; padding: 10px; font-size: 14px; line-height: 30px; border-radius: 7px; transition: box-shadow 0.35s ease-in-out 0s, all 0s ease-in-out 0s, all 0s ease-in-out 0s, all 0s ease-in-out 0s; cursor: pointer; margin: 0px; outline: 0px; height: 30px; box-shadow: rgba(0, 0, 0, 0) 0px 0px 0px 0px inset;" title="Saber mais sobre a segurança OVH"><span class="bold white" qtlid="816385" style="box-sizing: border-box; line-height: 30px; padding: 0px; margin: 0px; text-align: inherit; display: inline !important;">&ccedil;a&nbsp;</span></a></li>
			</ul>
<p>
	<br style="box-sizing: border-box; color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 16px; line-height: 14px; padding: 0px; margin: 0px; clear: both;" />
	<span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 16px;">&nbsp;&nbsp;</span><a name="infra" style="box-sizing: border-box; color: rgb(0, 104, 177); text-decoration-line: underline; font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 14px; line-height: 20px; padding: 0px; margin: 0px; cursor: pointer; outline: 0px; border-width: 0px; border-style: initial; border-color: initial;"></a></p>
	<p class="titleLight" style="box-sizing: border-box; margin: 0px; font-size: 24px; color: rgb(255, 255, 255); line-height: 20px; padding: 10px 30px; display: table-cell; background: none 0px 0px repeat scroll rgb(68, 68, 68); width: auto;">
		Infraestruturas de alta disponibilidade</p>
	<br style="box-sizing: border-box; line-height: 14px; padding: 0px; margin: 0px; clear: both;" />
	<br style="box-sizing: border-box; line-height: 14px; padding: 0px; margin: 0px; clear: both;" />
			<a href="https://www.ovh.pt/images/about-us/infra-haute-dispo.jpg" rel="lightbox[gallery]" style="box-sizing: border-box; color: rgb(0, 104, 177); text-decoration-line: none; background-color: transparent; font-size: 14px; line-height: 20px; padding: 0px; margin: 0px; cursor: pointer; outline: 0px; border: 0px;" title=""><img src="https://www.ovh.pt/images/about-us/infra-haute-dispo.jpg" style="box-sizing: border-box; vertical-align: middle; border: 0px; font-size: 12px; color: rgb(60, 60, 60); line-height: 16px; padding: 0px; margin: 0px; max-width: 100%; height: auto; width: 436.219px;" /></a></div>
			<ul style="box-sizing: border-box; margin: 0px 0px 5px 10px; line-height: 16px; padding-right: 0px; padding-left: 0px;">
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					Dupla alimenta&ccedil;&atilde;o el&eacute;trica em todas as instala&ccedil;&otilde;es;</li>
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					Onduladores de&nbsp;<span class="fs16 lh18 fw400" qtlid="816450" style="box-sizing: border-box; color: inherit; line-height: 20px; padding: 0px; margin: 0px; text-align: inherit;">250KVA</span>&nbsp;cada;</li>
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					Geradores com autonomia de&nbsp;<span class="fs16 lh18 fw400" qtlid="816489" style="box-sizing: border-box; color: inherit; line-height: 20px; padding: 0px; margin: 0px; text-align: inherit;">48h</span>&nbsp;;</li>
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					<span class="fs16 lh18 fw400" qtlid="816502" style="box-sizing: border-box; color: inherit; line-height: 20px; padding: 0px; margin: 0px; text-align: inherit;">datacenters com 2 liga&ccedil;&otilde;es &agrave; rede el&eacute;trica (no m&iacute;nimo)</span>&nbsp;; no interior,&nbsp;<span class="fs16 lh18 fw400" qtlid="816528" style="box-sizing: border-box; color: inherit; line-height: 20px; padding: 0px; margin: 0px; text-align: inherit;">2 salas &quot;g&eacute;meas&quot; de roteamento&nbsp;</span>para garantir um funcionamento constante</li>
				<li style="box-sizing: border-box; color: rgb(76, 76, 76); line-height: 20px; padding: 0px 0px 8.51563px; margin: 0px 0px 0px 20px; list-style-image: none; list-style-type: square; text-align: left;">
					Capacidades avan&ccedil;adas de rede: liga&ccedil;&atilde;o superior a 10Gb e 40GB na rede principal.</li>
			</ul>
@stop