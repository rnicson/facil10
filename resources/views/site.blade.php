@extends('layout')
 
@section('title', 'DESENVOLVIMENTO SITES')
 
@section('content')
<section class="tamanhoTelaPaginas topo-criacao-de-site" style="width: 1200px; margin: 0px auto; color: rgb(0, 0, 0); font-family: Ubuntu, sans-serif; font-size: 10px;">
	<div class="criacao-de-sites-esquerda" style="margin: 20px 0px 0px; padding: 0px; float: left; width: 600px;">
		<h2 style="margin: 0px 0px 20px; padding: 0px; font-size: 2.8em; color: rgb(45, 62, 80); line-height: 1.1em;">
			Converta&nbsp;<strong>muito mais</strong>&nbsp;Leads para seu neg&oacute;cio!</h2>
		<h3 style="margin: 0px 0px 10px; padding: 0px; font-weight: 300; font-size: 2em; color: rgb(45, 62, 80); line-height: 1.4em;">
			A Fácil10 sabe da import&acirc;ncia de desenvolver sites de qualidade, projetados e pensados para gerar resultados.</h3>
		<p style="margin: 0px; padding: 0px; font-size: 1.6em; line-height: 1.8em; color: rgb(45, 62, 80);">
			A decis&atilde;o de uma pessoa de ficar em um site ou n&atilde;o &eacute; tomada em menos de 6 segundos. Se o site da sua empresa n&atilde;o entregar o mais r&aacute;pido poss&iacute;vel a informa&ccedil;&atilde;o certa, seu visitante ir&aacute; acabar indo para o site do seu concorrente.<br />
			<br />
			<strong><b>Nossa miss&atilde;o &eacute; gerar mais oportunidades de neg&oacute;cio</b></strong>&nbsp;para sua empresa, por isso trabalhamos na cria&ccedil;&atilde;o de sites modernos, personalizados, de alta performance e com as maiores tend&ecirc;ncias de design e usabilidade.</p>
		<ul style="margin: 20px 0px 0px; padding-right: 50px; padding-left: 0px; width: 300px; float: left; box-sizing: border-box;">
			<li style="margin: 0px 0px 10px; padding: 0px; color: rgb(45, 62, 80); list-style: inside; font-size: 1.6em; line-height: 1.4em;">
				Sua empresa facilmente encontrada no Google</li>
			<li style="margin: 0px 0px 10px; padding: 0px; color: rgb(45, 62, 80); list-style: inside; font-size: 1.6em; line-height: 1.4em;">
				Fique na frente de seus concorrentes</li>
		</ul>
		<ul style="margin: 20px 0px 0px; padding-right: 50px; padding-left: 0px; width: 300px; float: left; box-sizing: border-box;">
			<li style="margin: 0px 0px 10px; padding: 0px; color: rgb(45, 62, 80); list-style: inside; font-size: 1.6em; line-height: 1.4em;">
				Gere mais oportunidades de vendas</li>
			<li style="margin: 0px 0px 10px; padding: 0px; color: rgb(45, 62, 80); list-style: inside; font-size: 1.6em; line-height: 1.4em;">
				Alcance mais clientes na internet</li>
		</ul>
		<div class="limpa" style="margin: 0px; padding: 0px; clear: both;">
			&nbsp;</div>
	</div>
	<div class="criacao-de-sites-direita" style="margin: 0px; padding: 0px; float: right; width: 570px;">
		<img alt="Criação de Sites Modernos e Responsivos - São Paulo" class="imagem-topo-criacao" src="https://www.agenciamaya.com.br/templates/img/criacao-de-sites/criacao-de-sites-profissionais-e-responsivos.jpg" style="border: none; max-width: 80%; display: block; margin: 0px auto 10px;" title="Criação de Sites Modernos e Responsivos - Tecnogate" />
		<div class="limpa" style="margin: 0px; padding: 0px; clear: both;">
			&nbsp;</div>
		<div class="limpa" style="margin: 0px; padding: 0px; clear: both;">
			&nbsp;</div>
	</div>
	<div class="quebraPaginaCriacaoSites" style="margin: 0px; padding: 0px; clear: both; height: 30px;">
		&nbsp;</div>

</section>
<div class="limpa" style="margin: 0px; padding: 0px; clear: both; color: rgb(0, 0, 0); font-family: Ubuntu, sans-serif; font-size: 10px;">
	&nbsp;</div>
<section class="bg-cinza depoimentos-criacao-de-sites depoimentosHome" style="background-color: rgb(244, 247, 249); padding-top: 40px; padding-bottom: 40px; color: rgb(0, 0, 0); font-family: Ubuntu, sans-serif; font-size: 10px;">
	<div class="tamanhoTelaPaginas" style="margin: 0px auto; padding: 0px; width: 1200px;">
		<div class="alinha-depoimentos-interna" style="margin: 0px 40px; padding: 0px;">
			<div class="bx-wrapper" style="margin: 0px auto; padding: 0px; max-width: 100%;">
				<div class="bx-viewport" style="margin: 0px; padding: 0px; float: left; width: 1000px; overflow: hidden; position: relative; height: 177px;">
					<ul id="slideDepoimentos" style="margin: 0px; padding-right: 0px; padding-left: 0px; width: 7150px; position: relative; transition-duration: 0s; transform: translate3d(-1010px, 0px, 0px);">
						<li class="bx-clone" style="margin: 0px 10px 0px 0px; padding: 0px; float: left; list-style: none; position: relative; width: 1000px;">
							<div class="depoimentoIndividual" style="margin: 0px; padding: 0px;">
								<div class="textoCentralizado depoimentoIndividualEsquerda" style="margin: 0px; padding: 0px; text-align: center; float: left; width: 150px;">
									<img alt="Criação de Site paraAdestramento Cão Meu Amigo" src="https://www.agenciamaya.com.br/admin/imagens/noticias/criacao-de-site-adestramento-de-caes.jpg" style="border: none; max-width: 100%; display: block; margin: 0px auto 30px;" title="Depoimento deAndré Fröhlich" /></div>
								<div class="depoimentoIndividualDireita" style="margin: 0px; padding: 0px; float: right; width: 760px;">
									<p style="margin: 0px; padding: 0px; color: rgb(45, 62, 80); font-size: 1.5em; line-height: 1.6em; text-align: justify;">
										Quando decidi promover a minha marca com o uso de um site, procurei alguns profissionais da &aacute;rea. Ao chegar na Fácil10, tive certeza que minha procura havia acabado. Encontrei um atendimento de excel&ecirc;ncia e profissionais qualificados, trabalho nota 10.</p>
									<h4 style="margin: 0px; padding: 0px; color: rgb(45, 62, 80); font-size: 1.7em; line-height: 1.6em;">
										Andr&eacute; Fr&ouml;hlich</h4>
									<h3 style="margin: 0px; padding: 0px; font-weight: 300; text-transform: uppercase; color: rgb(45, 62, 80); font-size: 1.7em; line-height: 1.6em;">
										ADESTRAMENTO C&Atilde;O MEU AMIGO</h3>
								</div>
							</div>
						</li>
						<li style="margin: 0px 10px 0px 0px; padding: 0px; float: left; list-style: none; position: relative; width: 1000px;">
							<div class="depoimentoIndividual" style="margin: 0px; padding: 0px;">
								<div class="textoCentralizado depoimentoIndividualEsquerda" style="margin: 0px; padding: 0px; text-align: center; float: left; width: 150px;">
									<img alt="Criação de Site paraLDC Laboratórios" src="http://leecadeirante.com.br/wp-content/uploads/2018/04/cropped-logo.png" style="border: none; max-width: 100%; display: block; margin: 0px auto 30px;" title="Depoimento Lee Cadeirante" /></div>
								<div class="depoimentoIndividualDireita" style="margin: 0px; padding: 0px; float: right; width: 760px;">
									<p style="margin: 0px; padding: 0px; color: rgb(45, 62, 80); font-size: 1.5em; line-height: 1.6em; text-align: justify;">
										Trabalho com a Fácil10 h&aacute; mais ou menos uns quatro anos. Uma empresa s&eacute;ria que cumpre o que promete, os trabalhos que solicitamos foram realizados com muita qualidade e criatividade, nossa parceria com voc&ecirc;s &eacute; e ser&aacute; duradoura pois prestam servi&ccedil;os com excel&ecirc;ncia e qualidade. Parab&eacute;ns para todos da equipe.</p>
									<h4 style="margin: 0px; padding: 0px; color: rgb(45, 62, 80); font-size: 1.7em; line-height: 1.6em;">
										<strong>Lee Cadeirante</strong></h4>
									<h3 style="margin: 0px; padding: 0px; font-weight: 300; text-transform: uppercase; color: rgb(45, 62, 80); font-size: 1.7em; line-height: 1.6em;">
										Presidente Nacional do Pais(Partido Pela Acessibilidade e Inclusão Social</h3>
								</div>
							</div>
						</li>
						<li style="margin: 0px 10px 0px 0px; padding: 0px; float: left; list-style: none; position: relative; width: 1000px;">
							<div class="depoimentoIndividual" style="margin: 0px; padding: 0px;">
								<div class="textoCentralizado depoimentoIndividualEsquerda" style="margin: 0px; padding: 0px; text-align: center; float: left; width: 150px;">
									<img alt="Criação de Site paraFENAC" src="https://www.agenciamaya.com.br/admin/imagens/noticias/criacao-de-site-pneumatica-sao-leopoldo.jpg" style="border: none; max-width: 100%; display: block; margin: 0px auto 30px;" title="Depoimento deArthur Reis" /></div>
								<div class="depoimentoIndividualDireita" style="margin: 0px; padding: 0px; float: right; width: 760px;">
									<p style="margin: 0px; padding: 0px; color: rgb(45, 62, 80); font-size: 1.5em; line-height: 1.6em; text-align: justify;">
										Tivemos problemas com o site antigo da Empresa, na ocasi&atilde;o tivemos ataques as vulnerabilidades de tecnologia obsoleta. Ap&oacute;s avalia&ccedil;&atilde;o de fornecedor de servi&ccedil;o (utilizando crit&eacute;rios do PMBOK como avalia&ccedil;&atilde;o de portf&oacute;lio, prazo de entrega, avalia&ccedil;&atilde;o de outros clientes, pre&ccedil;o e tecnologia, e escolhemos a Fácil10. Ap&oacute;s colocarmos o novo site no ar, nunca mais tivemos problemas com rela&ccedil;&atilde;o ao desenvolvimento dos nossos sites. J&aacute; fizemos 6 sites com a Fácil10.</p>
									<h4 style="margin: 0px; padding: 0px; color: rgb(45, 62, 80); font-size: 1.7em; line-height: 1.6em;">
										Arthur Reis</h4>
									<h3 style="margin: 0px; padding: 0px; font-weight: 300; text-transform: uppercase; color: rgb(45, 62, 80); font-size: 1.7em; line-height: 1.6em;">
										FENAC</h3>
								</div>
							</div>
						</li>
						<li style="margin: 0px 10px 0px 0px; padding: 0px; float: left; list-style: none; position: relative; width: 1000px;">
							<div class="depoimentoIndividual" style="margin: 0px; padding: 0px;">
								<div class="textoCentralizado depoimentoIndividualEsquerda" style="margin: 0px; padding: 0px; text-align: center; float: left; width: 150px;">
									<img alt="Criação de Site paraImobanco" src="https://www.agenciamaya.com.br/admin/imagens/noticias/criacao-de-site-natal-rio-grande-do-norte.jpg" style="border: none; max-width: 100%; display: block; margin: 0px auto 30px;" title="Depoimento deFernando Colares" /></div>
								<div class="depoimentoIndividualDireita" style="margin: 0px; padding: 0px; float: right; width: 760px;">
									<p style="margin: 0px; padding: 0px; color: rgb(45, 62, 80); font-size: 1.5em; line-height: 1.6em; text-align: justify;">
										A Fácil10 tem uma equipe maravilhosa! O atendimento &eacute; sempre eficiente e extremamente profissional, contudo, sem deixar de ser acolhedor. Pessoal, obrigado pela compet&ecirc;ncia e criatividade!</p>
									<h4 style="margin: 0px; padding: 0px; color: rgb(45, 62, 80); font-size: 1.7em; line-height: 1.6em;">
										Fernando Colares</h4>
									<h3 style="margin: 0px; padding: 0px; font-weight: 300; text-transform: uppercase; color: rgb(45, 62, 80); font-size: 1.7em; line-height: 1.6em;">
										IMOBANCO</h3>
								</div>
							</div>
						</li>
						<li style="margin: 0px 10px 0px 0px; padding: 0px; float: left; list-style: none; position: relative; width: 1000px;">
							<div class="depoimentoIndividual" style="margin: 0px; padding: 0px;">
								<div class="textoCentralizado depoimentoIndividualEsquerda" style="margin: 0px; padding: 0px; text-align: center; float: left; width: 150px;">
									<img alt="Criação de Site paraPousada Água da Fonte" src="https://www.agenciamaya.com.br/admin/imagens/noticias/criacao-de-site-pousada-florianopolis.jpg" style="border: none; max-width: 100%; display: block; margin: 0px auto 30px;" title="Depoimento deAndré Krüger" /></div>
								<div class="depoimentoIndividualDireita" style="margin: 0px; padding: 0px; float: right; width: 760px;">
									<p style="margin: 0px; padding: 0px; color: rgb(45, 62, 80); font-size: 1.5em; line-height: 1.6em; text-align: justify;">
										O primeiro site que fizemos com a Fácil10 nem conhec&iacute;amos a equipe pessoalmente, pois estamos em outro estado. Ocorreu tudo bem quanto ao desenvolvimento, cria&ccedil;&atilde;o e atendimento de nossas solicita&ccedil;&otilde;es. Depois fizemos mais um site e tamb&eacute;m tivemos a oportunidade de conhecer o pessoal. Destaque para o p&oacute;s-vendas que sempre que precisamos somos atendidos! Parab&eacute;ns &agrave; toda Equipe!</p>
									<h4 style="margin: 0px; padding: 0px; color: rgb(45, 62, 80); font-size: 1.7em; line-height: 1.6em;">
										Andr&eacute; Kr&uuml;ger</h4>
									<h3 style="margin: 0px; padding: 0px; font-weight: 300; text-transform: uppercase; color: rgb(45, 62, 80); font-size: 1.7em; line-height: 1.6em;">
										POUSADA &Aacute;GUA DA FONTE</h3>
								</div>
							</div>
						</li>
						<li style="margin: 0px 10px 0px 0px; padding: 0px; float: left; list-style: none; position: relative; width: 1000px;">
							<div class="depoimentoIndividual" style="margin: 0px; padding: 0px;">
								<div class="textoCentralizado depoimentoIndividualEsquerda" style="margin: 0px; padding: 0px; text-align: center; float: left; width: 150px;">
									<img alt="Criação de Site paraAdestramento Cão Meu Amigo" src="https://www.agenciamaya.com.br/admin/imagens/noticias/criacao-de-site-adestramento-de-caes.jpg" style="border: none; max-width: 100%; display: block; margin: 0px auto 30px;" title="Depoimento deAndré Fröhlich" /></div>
								<div class="depoimentoIndividualDireita" style="margin: 0px; padding: 0px; float: right; width: 760px;">
									<p style="margin: 0px; padding: 0px; color: rgb(45, 62, 80); font-size: 1.5em; line-height: 1.6em; text-align: justify;">
										Quando decidi promover a minha marca com o uso de um site, procurei alguns profissionais da &aacute;rea. Ao chegar na Fácil10, tive certeza que minha procura havia acabado. Encontrei um atendimento de excel&ecirc;ncia e profissionais qualificados, trabalho nota 10.</p>
									<h4 style="margin: 0px; padding: 0px; color: rgb(45, 62, 80); font-size: 1.7em; line-height: 1.6em;">
										Andr&eacute; Fr&ouml;hlich</h4>
									<h3 style="margin: 0px; padding: 0px; font-weight: 300; text-transform: uppercase; color: rgb(45, 62, 80); font-size: 1.7em; line-height: 1.6em;">
										ADESTRAMENTO C&Atilde;O MEU AMIGO</h3>
								</div>
							</div>
						</li>
						<li class="bx-clone" style="margin: 0px 10px 0px 0px; padding: 0px; float: left; list-style: none; position: relative; width: 1000px;">
							<div class="depoimentoIndividual" style="margin: 0px; padding: 0px;">
								<div class="textoCentralizado depoimentoIndividualEsquerda" style="margin: 0px; padding: 0px; text-align: center; float: left; width: 150px;">
									<img alt="Criação de Site paraLDC Laboratórios" src="https://www.agenciamaya.com.br/admin/imagens/noticias/criacao-de-site-laboratorio-de-analises.jpg" style="border: none; max-width: 100%; display: block; margin: 0px auto 30px;" title="Depoimento deAntonio Marcos" /></div>
								<div class="depoimentoIndividualDireita" style="margin: 0px; padding: 0px; float: right; width: 760px;">
									<p style="margin: 0px; padding: 0px; color: rgb(45, 62, 80); font-size: 1.5em; line-height: 1.6em; text-align: justify;">
										Trabalho com a Fácil10 h&aacute; mais ou menos uns quatro anos. Uma empresa s&eacute;ria que cumpre o que promete, os trabalhos que solicitamos foram realizados com muita qualidade e criatividade, nossa parceria com voc&ecirc;s &eacute; e ser&aacute; duradoura pois prestam servi&ccedil;os com excel&ecirc;ncia e qualidade. Parab&eacute;ns para todos da equipe.</p>
									<h4 style="margin: 0px; padding: 0px; color: rgb(45, 62, 80); font-size: 1.7em; line-height: 1.6em;">
										Antonio Marcos</h4>
									<h3 style="margin: 0px; padding: 0px; font-weight: 300; text-transform: uppercase; color: rgb(45, 62, 80); font-size: 1.7em; line-height: 1.6em;">
										LDC LABORAT&Oacute;RIOS</h3>
								</div>
							</div>
						</li>
					</ul>
				</div>
				<div class="bx-controls bx-has-controls-direction" style="margin: 0px; padding: 0px; float: right; width: 56px;">
					<div class="bx-controls-direction" style="margin: 0px; padding: 0px;">
						&nbsp;</div>
				</div>
			</div>
		</div>
		<div class="limpa" style="margin: 0px; padding: 0px; clear: both;">
			&nbsp;</div>
	</div>
</section>
<section style="color: rgb(0, 0, 0); font-family: Ubuntu, sans-serif; font-size: 10px;">
	<div class="tamanhoTelaPaginas" style="margin: 0px auto; padding: 0px; width: 1200px;">
		<div class="quebraPagina" style="margin: 0px; padding: 0px; clear: both; height: 120px;">
			&nbsp;</div>
		<div class="processo-criacao-esquerda" style="margin: 50px 0px 0px; padding: 0px; float: left; width: 600px;">
			<h2 style="margin: 0px 0px 20px; padding: 0px; color: rgb(37, 61, 4); font-size: 2.8em;">
				Como &eacute; o processo de cria&ccedil;&atilde;o de um site de alta performance?</h2>
			<p class="textoSimples" style="margin: 0px; padding: 0px; color: rgb(45, 62, 80); font-size: 1.8em; line-height: 1.6em;">
				Possu&iacute;mos um conjunto de profissionais especializados em planejar passo a passo como seu site de sucesso ser&aacute; desenvolvido. A cada etapa, voc&ecirc; participa enviando suas considera&ccedil;&otilde;es.</p>
		</div>
		<div class="processo-criacao-direita" style="margin: 0px; padding: 0px; float: right; width: 565px;">
			<img alt="Processo de Criação de Sites Modernos" src="https://www.agenciamaya.com.br/templates/img/criacao-de-sites/processos-criacao-de-sites-modernos.jpg" style="border: none; max-width: 100%;" title="Processo de Criação de Sites Modernos" />
			<div class="limpa" style="margin: 0px; padding: 0px; clear: both;">
				&nbsp;</div>
		</div>
		<div class="quebraPagina" style="margin: 0px; padding: 0px; clear: both; height: 120px;">
			&nbsp;</div>
		<div class="icones-criacao" style="margin: 0px; padding: 30px 0px 80px;">
			<div class="icone-criacao-individual icone-criacao-cima" style="margin: 0px 20px 0px 0px; padding: 0px; position: relative; float: left; width: 310px;">
				<img alt="Criação de Sites - Pesquisa de Mercado" src="https://www.agenciamaya.com.br/templates/img/criacao-de-sites/icone-pesquisa-de-mercado.png" style="border: none; max-width: 100%; float: left;" title="Criação de Sites - Pesquisa de Mercado" />
				<h3 style="margin: 0px 10px 0px 0px; padding: 0px; font-weight: 300; float: right; width: 200px; font-size: 1.4em; color: rgb(45, 62, 80); line-height: 1.3em;">
					An&aacute;lise dos objetivos estrat&eacute;gicos da empresa e escolha das tecnologias mais adequadas.</h3>
			</div>
			<div class="icone-criacao-individual icone-criacao-baixo" style="margin: 0px 20px 0px 0px; padding: 0px; position: relative; float: left; width: 310px;">
				<img alt="Criação de Sites - Estrutura do Site" src="https://www.agenciamaya.com.br/templates/img/criacao-de-sites/icone-design.png" style="border: none; max-width: 100%; float: left;" title="Criação de Sites - Estrutura do Site" />
				<h3 style="margin: 0px 10px 0px 0px; padding: 0px; font-weight: 300; float: right; width: 200px; font-size: 1.4em; color: rgb(45, 62, 80); line-height: 1.3em;">
					O Layout do site pensado, planejado e desenvolvido com foco na experi&ecirc;ncia do usu&aacute;rio (UX).</h3>
			</div>
			<div class="icone-criacao-individual icone-criacao-menor icone-criacao-cima-dois" style="margin: 0px 20px 0px 0px; padding: 0px; position: relative; float: left; width: 260px;">
				<img alt="Criação de Sites - Planejamento da Interface" src="https://www.agenciamaya.com.br/templates/img/criacao-de-sites/icone-planejamento.png" style="border: none; max-width: 100%; float: left;" title="Criação de Sites - Planejamento da Interface" />
				<h3 style="margin: 0px 10px 0px 0px; padding: 0px; font-weight: 300; float: right; width: 150px; font-size: 1.4em; color: rgb(45, 62, 80); line-height: 1.3em;">
					Planejamento de arquitetura e interface.</h3>
			</div>
			<div class="icone-criacao-individual icone-criacao-menor icone-criacao-ultimo" style="margin: 0px; padding: 0px; position: relative; float: left; width: 260px;">
				<img alt="Criação de Sites - Teste de Desempenho" src="https://www.agenciamaya.com.br/templates/img/criacao-de-sites/icone-testes.png" style="border: none; max-width: 100%; float: left;" title="Criação de Sites - Teste de Desempenho" />
				<h3 style="margin: 0px 10px 0px 0px; padding: 0px; font-weight: 300; float: right; width: 150px; font-size: 1.4em; color: rgb(45, 62, 80); line-height: 1.3em;">
					Testes para avaliar o desempenho do site ou landing page.</h3>
			</div>
		</div>

	</div>
	<hr>
<div class="row" style="background-color: #CECEF6;box-sizing: border-box; display: flex; flex-wrap: wrap; margin-right: -15px; margin-left: -15px; color: rgb(0, 0, 0); font-family: font-mdartes-regular; font-size: 14px;">
	<div class="col-lg-4" style="box-sizing: border-box; position: relative; width: 379.984px; min-height: 1px; padding-right: 15px; padding-left: 15px; flex: 0 0 33.3333%; max-width: 33.3333%;">
		<div class="single-planos" style="box-sizing: border-box;">
			<div class="top-sec d-flex justify-content-between" style="box-sizing: border-box; display: flex !important; justify-content: space-between !important; border-bottom: 1px solid rgb(238, 238, 238); padding: 20px 10px 5px; text-align: center; transition: all 0.3s ease 0s;">
			    <div class="top-left" style="box-sizing: border-box;">
					<h4 style="box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-family: inherit; line-height: 1.5em; color: rgb(109, 111, 112); font-size: 18px;">
						Básico</h4>
				</div>
				<div class="top-right" style="box-sizing: border-box;">
					<h1 style="box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-family: inherit; line-height: 1.5em; color: rgb(109, 111, 112); font-size: 36px;">
						R$ 550</h1>
				</div>
			</div>
			<div class="bottom-sec" style="box-sizing: border-box; border-bottom: 1px solid rgb(238, 238, 238); padding: 20px 10px 5px; text-align: center; transition: all 0.3s ease 0s;">
				<p style="box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem;">
					Hospedagem 1ª Mês Gr&aacute;tis&sup1;</p>
			</div>
			<div class="end-sec" style="box-sizing: border-box; background-color: rgb(249, 249, 255); transition: all 0.3s ease 0s; padding: 40px 40px 22px;">
				<ul style="box-sizing: border-box; margin: 0px 0px 20px; padding-right: 0px; padding-left: 0px; list-style: none;">
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						At&eacute; 4 p&aacute;ginas</li>
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						2 contas de e-mails 2Gb cada</li>
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						Banner principal fixo</li>
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						Formul&aacute;rio de contato</li>
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						Gerenciador de textos e imagens&sup3;</li>
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						Cadastro em todos buscadores</li>
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						SEO B&aacute;sico</li>
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						&nbsp;</li>
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						&nbsp;</li>
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						&nbsp;</li>
				</ul>
				@guest
				<a href="https://link.pagar.me/lB1eOzqMd6U" style="box-sizing: border-box; color: rgb(0, 180, 181); text-decoration-line: none; background-color: transparent; transition: all 0.3s ease 0s;" target="_blank"><button class="primary-btn planos-btn mt-20" style="border-radius: 50px; margin: 20px 0px 0px; font-family: inherit; font-size: inherit; line-height: 42px; overflow: visible; text-transform: uppercase; outline: none; background: rgb(0, 180, 181); padding-left: 40px; padding-right: 40px; border-width: 1px; border-style: solid; border-color: transparent; color: rgb(255, 255, 255); transition: all 0.3s ease 0s; cursor: pointer; position: relative; box-shadow: none; width: 269.984px;">CONTRATAR</button></a>
				@else
				<a href="https://link.pagar.me/lB1eOzqMd6U" style="box-sizing: border-box; color: rgb(0, 180, 181); text-decoration-line: none; background-color: transparent; transition: all 0.3s ease 0s;" target="_blank"><button class="primary-btn planos-btn mt-20" style="border-radius: 50px; margin: 20px 0px 0px; font-family: inherit; font-size: inherit; line-height: 42px; overflow: visible; text-transform: uppercase; outline: none; background: rgb(0, 180, 181); padding-left: 40px; padding-right: 40px; border-width: 1px; border-style: solid; border-color: transparent; color: rgb(255, 255, 255); transition: all 0.3s ease 0s; cursor: pointer; position: relative; box-shadow: none; width: 269.984px;">CONTRATAR</button></a>
				@endguest</div>
		</div>
	</div>
	<div class="col-lg-4" style="box-sizing: border-box; position: relative; width: 379.984px; min-height: 1px; padding-right: 15px; padding-left: 15px; flex: 0 0 33.3333%; max-width: 33.3333%;">
		<div class="single-planos" style="box-sizing: border-box;">
			<div class="top-sec d-flex justify-content-between" style="box-sizing: border-box; display: flex !important; justify-content: space-between !important; border-bottom: 1px solid rgb(238, 238, 238); padding: 20px 10px 5px; text-align: center; transition: all 0.3s ease 0s;">
				<div class="top-left" style="box-sizing: border-box;">
					<h4 style="box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-family: inherit; line-height: 1.5em; color: rgb(109, 111, 112); font-size: 18px;">
						Intermediário</h4>
				</div>
				<div class="top-right" style="box-sizing: border-box;">
					<h1 style="box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-family: inherit; line-height: 1.5em; color: rgb(109, 111, 112); font-size: 36px;">
						R$ 750</h1>
				</div>
			</div>
			<div class="bottom-sec" style="box-sizing: border-box; border-bottom: 1px solid rgb(238, 238, 238); padding: 20px 10px 5px; text-align: center; transition: all 0.3s ease 0s;">
				<p style="box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem;">
					Hospedagem 1ªMês Gr&aacute;tis&sup1;</p>
			</div>
			<div class="end-sec" style="box-sizing: border-box; background-color: rgb(249, 249, 255); transition: all 0.3s ease 0s; padding: 40px 40px 22px;">
				<ul style="box-sizing: border-box; margin: 0px 0px 20px; padding-right: 0px; padding-left: 0px; list-style: none;">
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						At&eacute; 6 p&aacute;ginas</li>
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						4 contas de e-mails 2Gb cada</li>
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						Banner principal din&acirc;mico 2 imagens</li>
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						Formul&aacute;rio de contato</li>
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						Gerenciador de textos e imagens&sup3;</li>
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						Cadastro em todos buscadores</li>
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						Google Maps</li>
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						Google Analytics</li>
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						SEO B&aacute;sico</li>
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						&nbsp;</li>
				</ul>
				@guest
				<a href="https://link.pagar.me/lBkV3u5zdTU" style="box-sizing: border-box; color: rgb(0, 180, 181); text-decoration-line: none; background-color: transparent; transition: all 0.3s ease 0s;" target="_blank"><button class="primary-btn planos-btn mt-20" style="border-radius: 50px; margin: 20px 0px 0px; font-family: inherit; font-size: inherit; line-height: 42px; overflow: visible; text-transform: uppercase; outline: none; background: rgb(0, 180, 181); padding-left: 40px; padding-right: 40px; border-width: 1px; border-style: solid; border-color: transparent; color: rgb(255, 255, 255); transition: all 0.3s ease 0s; cursor: pointer; position: relative; box-shadow: none; width: 269.984px;">CONTRATAR</button></a>
				@else
				<a href="https://link.pagar.me/lBkV3u5zdTU" style="box-sizing: border-box; color: rgb(0, 180, 181); text-decoration-line: none; background-color: transparent; transition: all 0.3s ease 0s;" target="_blank"><button class="primary-btn planos-btn mt-20" style="border-radius: 50px; margin: 20px 0px 0px; font-family: inherit; font-size: inherit; line-height: 42px; overflow: visible; text-transform: uppercase; outline: none; background: rgb(0, 180, 181); padding-left: 40px; padding-right: 40px; border-width: 1px; border-style: solid; border-color: transparent; color: rgb(255, 255, 255); transition: all 0.3s ease 0s; cursor: pointer; position: relative; box-shadow: none; width: 269.984px;">CONTRATAR</button></a>
				@endguest
				</div>
		</div>
	</div>
	<div class="col-lg-4" style="box-sizing: border-box; position: relative; width: 379.984px; min-height: 1px; padding-right: 15px; padding-left: 15px; flex: 0 0 33.3333%; max-width: 33.3333%;">
		<div class="single-planos" style="box-sizing: border-box;">
			<div class="top-sec d-flex justify-content-between" style="box-sizing: border-box; display: flex !important; justify-content: space-between !important; border-bottom: 1px solid rgb(238, 238, 238); padding: 20px 10px 5px; text-align: center; transition: all 0.3s ease 0s;">
				<div class="top-left" style="box-sizing: border-box;">
					<h4 style="box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-family: inherit; line-height: 1.5em; color: rgb(109, 111, 112); font-size: 18px;">
						Profissional</h4>
				</div>
				<div class="top-right" style="box-sizing: border-box;">
					<h1 style="box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-family: inherit; line-height: 1.5em; color: rgb(109, 111, 112); font-size: 36px;">
						R$ 950</h1>
				</div>
			</div>
			<div class="bottom-sec" style="box-sizing: border-box; border-bottom: 1px solid rgb(238, 238, 238); padding: 20px 10px 5px; text-align: center; transition: all 0.3s ease 0s;">
				<p style="box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem;">
					Hospedagem 1ªMês Gr&aacute;tis&sup1; e Dom&iacute;nio 1ªAno Gr&aacute;tis&sup2;</p>
			</div>
			<div class="end-sec" style="box-sizing: border-box; background-color: rgb(249, 249, 255); transition: all 0.3s ease 0s; padding: 40px 40px 22px;">
				<ul style="box-sizing: border-box; margin: 0px 0px 20px; padding-right: 0px; padding-left: 0px; list-style: none;">
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						At&eacute; 8 p&aacute;ginas</li>
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						6 contas de e-mails 2Gb cada</li>
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						Banner principal din&acirc;mico 4 imagens</li>
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						Formul&aacute;rio de contato</li>
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						Gerenciador de textos e imagens&sup3;</li>
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						Cadastro em todos buscadores</li>
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						Google Maps</li>
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						Google Analytics</li>
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						SEO B&aacute;sico</li>
					<li style="box-sizing: border-box; margin-bottom: 10px; height: 20px;">
						Atendimento via chat</li>
				</ul>
				@guest
				<a href="https://link.pagar.me/lr1bgjzdTU" style="box-sizing: border-box; color: rgb(0, 180, 181); text-decoration-line: none; background-color: transparent; transition: all 0.3s ease 0s;" target="_blank"><button class="primary-btn planos-btn mt-20" style="border-radius: 50px; margin: 20px 0px 0px; font-family: inherit; font-size: inherit; line-height: 42px; overflow: visible; text-transform: uppercase; outline: none; background: rgb(0, 180, 181); padding-left: 40px; padding-right: 40px; border-width: 1px; border-style: solid; border-color: transparent; color: rgb(255, 255, 255); transition: all 0.3s ease 0s; cursor: pointer; position: relative; box-shadow: none; width: 269.984px;">CONTRATAR</button></a>
				@else
				<a href="https://link.pagar.me/lr1bgjzdTU" style="box-sizing: border-box; color: rgb(0, 180, 181); text-decoration-line: none; background-color: transparent; transition: all 0.3s ease 0s;" target="_blank"><button class="primary-btn planos-btn mt-20" style="border-radius: 50px; margin: 20px 0px 0px; font-family: inherit; font-size: inherit; line-height: 42px; overflow: visible; text-transform: uppercase; outline: none; background: rgb(0, 180, 181); padding-left: 40px; padding-right: 40px; border-width: 1px; border-style: solid; border-color: transparent; color: rgb(255, 255, 255); transition: all 0.3s ease 0s; cursor: pointer; position: relative; box-shadow: none; width: 269.984px;">CONTRATAR</button></a>
				@endguest
				</div>
		</div>
	</div>
</div>
<div class="row d-flex justify-content-center" style="box-sizing: border-box; flex-wrap: wrap; margin-right: -15px; margin-left: -15px; color: rgb(119, 119, 119); font-family: font-mdartes-regular; font-size: 14px; display: flex !important; justify-content: center !important;">
	<div class="menu-content mt-30 col-lg-8" style="box-sizing: border-box; position: relative; width: 760px; min-height: 1px; padding-right: 15px; padding-left: 15px; flex: 0 0 66.6667%; max-width: 66.6667%; margin-top: 30px;">
		<div class="title text-center" style="box-sizing: border-box; text-align: center;">
			<h2 class="mb-10" style="box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; font-family: inherit; line-height: 1.5em; color: rgb(109, 111, 112); font-size: 30px;">
				Os planos n&atilde;o atendem ao seu projeto?</h2>
			<p style="box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem;">
				Caso deseje mais recursos ou outras funcionalidades que n&atilde;o estejam descritas aqui no site, ou n&atilde;o fa&ccedil;am parte do pacote do seu interesse, solicite um or&ccedil;amento descrevendo as necessidades do seu projeto.</p>
		</div>
	</div>
</div>
<center>
<div class="container">

	<p>
		<a class="botaoHome abreFormulario botaoCriacaoSites" data-formulario="Página Criação de Sites" href="https://api.whatsapp.com/send?phone=5511945040347&text=Encontrei%20o%20site%20da%20F%C3%A1cil10%2Ce%20tenho%20interesse%20em%20seus%20servi%C3%A7os." style="margin: 0px; padding: 0px 25px; font-weight: 700; transition: all 600ms ease 0s; text-decoration-line: none; color: rgb(255, 255, 255); border-radius: 25px; line-height: 50px; border: 1px solid rgb(255, 51, 51); background: rgb(255, 97, 34); display: inline-block; font-size: 1.5em; text-transform: uppercase; letter-spacing: 1px; white-space: nowrap; font-family: Ubuntu, sans-serif; text-align: center;">SOLICITE UM OR&Ccedil;AMENTO!</a></p>
		</div>
		</center>
</section>

@stop