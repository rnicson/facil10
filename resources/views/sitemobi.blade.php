@extends('layout')
 
@section('title', 'DESENVOLVIMENTO SITES')
 
@section('content')
	<h2 style="margin: 0px 0px 20px; padding: 0px; font-size: 2.8em; color: rgb(45, 62, 80); line-height: 1.1em;">
			Converta&nbsp;<strong>muito mais</strong>&nbsp;Leads para seu neg&oacute;cio!</h2>
		<h3 style="margin: 0px 0px 10px; padding: 0px; font-weight: 300; font-size: 2em; color: rgb(45, 62, 80); line-height: 1.4em;">
			A Fácil10 sabe da import&acirc;ncia de desenvolver sites de qualidade, projetados e pensados para gerar resultados.</h3>
		<p style="margin: 0px; padding: 0px; font-size: 1.6em; line-height: 1.8em; color: rgb(45, 62, 80);">
			A decis&atilde;o de uma pessoa de ficar em um site ou n&atilde;o &eacute; tomada em menos de 6 segundos. Se o site da sua empresa n&atilde;o entregar o mais r&aacute;pido poss&iacute;vel a informa&ccedil;&atilde;o certa, seu visitante ir&aacute; acabar indo para o site do seu concorrente.<br />
			<br />
			<strong><b>Nossa miss&atilde;o &eacute; gerar mais oportunidades de neg&oacute;cio</b></strong>&nbsp;para sua empresa, por isso trabalhamos na cria&ccedil;&atilde;o de sites modernos, personalizados, de alta performance e com as maiores tend&ecirc;ncias de design e usabilidade.</p>
		<ul style="margin: 20px 0px 0px; padding-right: 50px; padding-left: 0px; width: 300px; float: left; box-sizing: border-box;">
			<li style="margin: 0px 0px 10px; padding: 0px; color: rgb(45, 62, 80); list-style: inside; font-size: 1.6em; line-height: 1.4em;">
				Sua empresa facilmente encontrada no Google</li>
			<li style="margin: 0px 0px 10px; padding: 0px; color: rgb(45, 62, 80); list-style: inside; font-size: 1.6em; line-height: 1.4em;">
				Fique na frente de seus concorrentes</li>
		</ul>
		<ul style="margin: 20px 0px 0px; padding-right: 50px; padding-left: 0px; width: 300px; float: left; box-sizing: border-box;">
			<li style="margin: 0px 0px 10px; padding: 0px; color: rgb(45, 62, 80); list-style: inside; font-size: 1.6em; line-height: 1.4em;">
				Gere mais oportunidades de vendas</li>
			<li style="margin: 0px 0px 10px; padding: 0px; color: rgb(45, 62, 80); list-style: inside; font-size: 1.6em; line-height: 1.4em;">
				Alcance mais clientes na internet</li>
		</ul>
		<div class="container">

	<p>
		<a class="botaoHome abreFormulario botaoCriacaoSites" data-formulario="Página Criação de Sites" href="https://api.whatsapp.com/send?phone=5511945040347&text=Encontrei%20o%20site%20da%20F%C3%A1cil10%2Ce%20tenho%20interesse%20em%20seus%20servi%C3%A7os." style="margin: 0px; padding: 0px 25px; font-weight: 450; transition: all 600ms ease 0s; text-decoration-line: none; color: rgb(255, 255, 255); border-radius: 25px; line-height: 50px; border: 1px solid rgb(255, 51, 51); background: #7F00FF; width:100%; display: inline-block; font-size: 1.2em; text-transform: uppercase; letter-spacing: 1px; white-space: nowrap; font-family: Ubuntu, sans-serif; text-align: center;">SOLICITE UM OR&Ccedil;AMENTO!</a></p>
		</div>
@stop